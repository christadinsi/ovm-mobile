// ignore_for_file: implementation_imports

import 'package:agenda/agenda.dart';
import 'package:budget/budget.dart';
import 'package:comite/comite.dart';
import 'package:flutter/material.dart';
import 'package:ovm/authentification/connexion/interface_connexion.dart';
import 'package:ovm/authentification/inscription/inscription.dart';
import 'package:ovm/authentification/login/inscription_comodite.dart';
import 'package:ovm/authentification/login/inscription_marie.dart';
import 'package:ovm/authentification/login/inscription_mariee.dart';
import 'package:ovm/authentification/login/splash_screen_inscription_comite.dart';
import 'package:ovm/authentification/login/splash_screen_inscription_mariee.dart';
import 'package:ovm/views/home.dart';

import 'package:ovm/views/mariee/accueil_mariee.dart';
import 'package:ovm/views/membre_comite/formulaire_integration_membre_comite.dart';
import 'package:prestataires/prestataires.dart';
import 'package:profile/profile.dart';
// import 'package:profile/src/views/profile_mariee/forfait/forfait.dart';
// import 'package:profile/src/views/profile_mariee/progression/button_progression.dart';

class AppRouter {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => const Home());
      case '/connexion':
        return MaterialPageRoute(builder: (context) => const Connexion());
      case '/inscription':
        return MaterialPageRoute(builder: (context) => const Inscription());
      case '/FormulaireInscriptionMariee':
        return MaterialPageRoute(
          builder: (context) => const FormulaireInscriptionMariee(),
        );
      case '/FormulaireInscriptionMarie':
        return MaterialPageRoute(
          builder: (context) => const FormulaireInscriptionMarie(),
        );
      case '/FormulaireInscriptionComodite':
        return MaterialPageRoute(
          builder: (context) => const FormulaireInscriptionComodite(),
        );
      case '/FormulaireDemandeIntegrationComite':
        return MaterialPageRoute(
          builder: (context) => const FormulaireDemandeIntegrationComite(),
        );
      case '/SplashScreenInscriptionMarie':
        return MaterialPageRoute(
          builder: (context) => const SplashScreenInscriptionMariee(),
        );
      case '/SplashScreenInscriptionMariee':
        return MaterialPageRoute(
          builder: (context) => const SplashScreenInscriptionMariee(),
        );
      case '/SplashScreenInscriptionComite':
        return MaterialPageRoute(
          builder: (context) => const SplashScreenInscriptionComite(),
        );
      case '/AccueilMariee':
        return MaterialPageRoute(
          builder: (context) => const AccueilMariee(),
        );
      case '/ProfileMariee':
        return MaterialPageRoute(
          builder: (context) => const ProfileMariee(),
        );
      case '/PrestatairesMariee':
        return MaterialPageRoute(
          builder: (context) => const PrestatairesMariee(),
        );
      case '/agendaMariee':
        return MaterialPageRoute(
          builder: (context) => const AgendaAccueil(),
        );
      // case '/ProgressionMariee':
      //   return MaterialPageRoute(
      //       builder: (context) => const ProgressionMariee());

      case '/BudgetMariee':
        return MaterialPageRoute(
          builder: (context) => const BudgetMariee(),
        );
      case '/AccueilCommite':
        return MaterialPageRoute(
          builder: (context) => const AccueilCommite(),
        );

      default:
        // Gérer les routes inconnues en renvoyant une page par défaut ou une erreur 404, par exemple :
        return null;
    }
  }
}
