import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class AppWidgetsBoutons {

  static Center boutonRetour({
    required Color colorbouton,
    required double width,
    required double height,
  }) {
    return  Center(

      child: SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor:
              MaterialStateProperty.all(colorbouton)),
          onPressed: () {
           
          },
          child: const Icon(
            Icons.home_filled,
            color: Colors.white,
            size: 40,
          ),
        ),
      ),
    );

  }

  static SizedBox  ButtonCreer({
    required String text,
    required void Function()? onPressed,
    required Color color,
  })
  {
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child: Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }

  static ButtonInscription(
      {
        required String text,
        required void Function()? onPressed,
        required Color color,
      })
{
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child: Text(
          text,style: const TextStyle(color: Design.colorSecondTexte,fontSize: 24),
        ),
      ),
    );
  }

  static ZoomTapAnimation ButtonGlobal({

    required String text,
    required Function onTap,
    required Color color,
    required double largeur,
  }){
    return ZoomTapAnimation(
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: color,
        ),
        width: largeur,
        height: 60,
        child: Center(
          child: Text(
            text,
            style:
            const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
          ),
        ),
        // child: ElevatedButton(
        //   // onPressed: () => onPressed(),
        //   style: ElevatedButton.styleFrom(
        //     backgroundColor: color,
        //     shape: RoundedRectangleBorder(
        //       borderRadius: BorderRadius.circular(100),
        //     ),
        //   ),

        // ),
      ),
    );
  }

}