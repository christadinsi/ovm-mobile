import 'package:flutter/material.dart';
import 'package:ovm/App/view/app.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

Future<void> main() async {

  await Future.delayed(const Duration(seconds: 10));
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(const App());
  FlutterNativeSplash.remove();
}
