import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ovm/views/mariee/accueil_mariee.dart';
import 'package:page_transition/page_transition.dart';

class Connexion extends StatelessWidget {
  const Connexion({super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final double myh = MediaQuery.of(context).size.height;
    final double myw = MediaQuery.of(context).size.width;


    return Scaffold(
      body: Column(children: [
        Stack(
          children: [
            BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.5, sigmaY: 10.5),
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      const Color.fromARGB(216, 0, 0, 0),
                      const Color(0xFF000000).withOpacity(0.7),
                    ],
                  ),
                ),
                width: myw*1.25,
                height: myh*.53,
                child: Image.asset("assets/images/conn.jpg",
                    filterQuality: FilterQuality.high,
                    fit: BoxFit.fitHeight,
                    alignment: Alignment.bottomLeft),
              ),
            ),
            // icon qui doit etre sur l'image
            Positioned(
              top: 30,
              left: 10,
              child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const InkWell(
                    child: Icon(
                      Icons.arrow_back,
                      color: Color(0xFFD47FA6),
                    ),
                  )),
            ),
            Positioned(
              bottom: -2,
              left: 0,
              right: 0,
              child: Stack(
                children: [
                  Container(
                    width: myw*1.17,
                    height: myh*.10,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            stops: const [
                          0.2,
                          0.2,
                          0.6,
                          .6
                        ],
                            colors: [
                          const Color(0XFFFFFFFF).withOpacity(.1),
                          const Color(0XFFFFFFFF).withOpacity(.1),
                          const Color(0XFFFFFFFF).withOpacity(.7),
                          const Color(0xFFFFFFFF).withOpacity(.7)
                        ])
                        // color: Colors.white.withOpacity(.7),

                        //  color: Colors.white.withOpacity(.4),

                        ),
                  ),
                  Positioned(
                    bottom: -15,
                    child: Container(
                      margin: EdgeInsets.only(left: myw*.16,right: myw*.16),
                      decoration: const BoxDecoration(
                          // color: Colors.white.withOpacity(.2)
                          ),
                      // padding:  EdgeInsets.only(left: myw*.21, right: myw*.21),
                      child: const Text(
                        "Connexion",
                        style: TextStyle(
                            fontSize: 50,
                            color: Color(0xFFD47FA6),
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),

            // bloc connexion
          ],
        ),
        const SizedBox(
          height: 50,
        ),
        Expanded(
          child: Form(
              // la cle pour que le formulaire soit valider
              // if(_formKey.currentState!.validator()){return ok}
              key: formKey,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding:
                          const EdgeInsets.only(top: 25, left: 5, bottom: 2),
                      width: myw*.9,
                      height: myh*.08,
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Color(0xFF0000001C),
                              offset: Offset(0, 20),
                              blurRadius: 40)
                        ],
                        color: const Color(0xFF9599B3),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Veuillez entrer un numéro de téléphone ";
                          }
                        },
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(30)),
                          hintText: 'Telephone',
                          hintStyle: const TextStyle(
                              fontSize: 25,
                              color: Colors.white,
                              overflow: TextOverflow.ellipsis),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 27,
                    ),
                    // pwd
                    Container(
                      padding:
                          const EdgeInsets.only(top: 25, left: 5, bottom: 2),
                      width: myw*.9,
                      height: myh*.08,
                      decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Color(0xFF0000001C),
                              offset: Offset(0, 20),
                              blurRadius: 40)
                        ],
                        color: const Color(0xFF9599B3),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Veuillez entrer un mot de passe ";
                          }
                        },
                        obscureText: true,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(30),
                          ),
                          hintText: 'Mot de passe',
                          hintStyle: const TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 27,
                    ),
                    // button
                    Container(
                        width: myw*.9,
                      height: myh*.08,
                        decoration: BoxDecoration(
                          color: const Color(0xFFD47FA6),
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: const [
                            BoxShadow(
                                color: Color.fromARGB(0, 236, 236, 240),
                                offset: Offset(0, 20),
                                blurRadius: 40)
                          ],
                        ),
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0xFFD47FA6)),
                            onPressed: () {
                              // Navigator.of(context).pushReplacementNamed( '/AccueilMariee');
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const AccueilMariee(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: const Text(
                              "Se connecter",
                              style:
                                  TextStyle(fontSize: 25, color: Colors.white),
                            ))),
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          "Mot de passe oublié?",
                          style:
                              TextStyle(fontSize: 18, color: Color(0xFF000000)),
                        )),
                  ],
                ),
              )),
        ),
      ]),
    );
  }
}
