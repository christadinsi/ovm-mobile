import 'package:flutter/material.dart';
import 'package:ovm/authentification/login/inscription_mariee.dart';
import 'package:ovm/components/widgets-boutons.dart';
import 'package:ovm/design/design.dart';
import 'package:page_transition/page_transition.dart';

class SplashScreenInscriptionMariee extends StatefulWidget {
  const SplashScreenInscriptionMariee({super.key});

  @override
  State<SplashScreenInscriptionMariee> createState() =>
      _SplashScreenInscriptionMarieeState();
}

class _SplashScreenInscriptionMarieeState
    extends State<SplashScreenInscriptionMariee> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/lamariee/marieSplashScreen14.jpg",
            fit: BoxFit.cover,
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 440, top: 30),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true)
                        .pushNamed('/Inscription');
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  "Vous etes ",
                  style: TextStyle(
                      fontSize: Design.firstText,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 0),
                child: Text(
                  "le future marié",
                  style: TextStyle(
                      fontSize: Design.firstText,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Padding(
                padding: EdgeInsets.only(top: 90.0),
                child: Text(
                  "une nouvelle page",
                  style: TextStyle(
                      color: Design.colorSecondTexte,
                      fontSize: Design.secondText),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 1.0),
                child: Text(
                  "de votre histoire commence",
                  style: TextStyle(
                      color: Design.colorSecondTexte,
                      fontSize: Design.secondText),
                ),
              ),
              const SizedBox(
                height: 160,
              ),
              AppWidgetsBoutons.ButtonInscription(
                text: "Inscription",
                onPressed: () {
                  Navigator.push(
                    context,
                    PageTransition(
                      child: const FormulaireInscriptionMariee(),
                      type: PageTransitionType.rightToLeft,
                      childCurrent: widget,
                      duration: const Duration(milliseconds: 200),
                    ),
                  );
                },
                color: Design.colorMariee,
              )
            ],
          ),
        ],
      ),
    );
  }
}
