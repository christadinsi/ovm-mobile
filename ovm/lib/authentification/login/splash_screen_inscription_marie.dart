import 'package:flutter/material.dart';
import 'package:ovm/authentification/login/inscription_marie.dart';
import 'package:ovm/components/widgets-boutons.dart';
import 'package:ovm/design/design.dart';
import 'package:page_transition/page_transition.dart';

class SplashScreenInscriptionMarie extends StatefulWidget {
  const SplashScreenInscriptionMarie({super.key});

  @override
  State<SplashScreenInscriptionMarie> createState() =>
      _SplashScreenInscriptionMarieState();
}

class _SplashScreenInscriptionMarieState
    extends State<SplashScreenInscriptionMarie> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/lamariee/marieeProfile13.jpg",
            fit: BoxFit.cover,
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 440, top: 30),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true)
                        .pushNamed('/Inscription');
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 80),
                child: Text(
                  "Vous etes ",
                  style: TextStyle(
                      fontSize: Design.firstText,
                      color: Design.ColorMarie,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 0),
                child: Text(
                  "le future marié",
                  style: TextStyle(
                      fontSize: Design.firstText,
                      color: Design.ColorMarie,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Padding(
                padding: EdgeInsets.only(top: 60.0),
                child: Text(
                  "une nouvelle page",
                  style: TextStyle(
                      color: Design.colorSecondTexte,
                      fontSize: Design.secondText),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 1.0),
                child: Text(
                  "de votre histoire commence",
                  style: TextStyle(
                      color: Design.colorSecondTexte,
                      fontSize: Design.secondText),
                ),
              ),
              const SizedBox(
                height: 160,
              ),
              AppWidgetsBoutons.ButtonInscription(
                text: "Inscription",
                onPressed: () {
                  Navigator.push(
                    context,
                    PageTransition(
                      child: const FormulaireInscriptionMarie(),
                      type: PageTransitionType.rightToLeft,
                      childCurrent: widget,
                      duration: Duration(milliseconds: 200),
                    ),
                  );
                },
                color: Design.ColorMarie,
              )
            ],
          ),
        ],
      ),
    );
  }
}
