import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ovm/authentification/login/inscription_comodite.dart';
import 'package:ovm/components/widgets-boutons.dart';
import 'package:ovm/design/design.dart';
import 'package:page_transition/page_transition.dart';

class SplashScreenInscriptionComite extends StatefulWidget {
  const SplashScreenInscriptionComite({super.key});

  @override
  State<SplashScreenInscriptionComite> createState() =>
      _SplashScreenInscriptionComiteState();
}

class _SplashScreenInscriptionComiteState
    extends State<SplashScreenInscriptionComite> {
  @override
  Widget build(BuildContext context) {
    // final double _width = MediaQuery.of(context).size.width;
    // final double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/lamariee/marieSplashScreen15.jpg",
            fit: BoxFit.cover,
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 440, top: 30),
                child: IconButton(
                  onPressed: () {
                    Navigator
                        .pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 90),
                child: Text(
                  "Vous etes ",
                  style: TextStyle(
                      fontSize: Design.firstText,
                      color: Design.colorMembreComite,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 0),
                child: Text(
                  "le future marié",
                  style: TextStyle(
                      fontSize: Design.firstText,
                      color: Design.colorMembreComite,
                      fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Padding(
                padding: EdgeInsets.only(top: 80.0),
                child: Text(
                  "Votre est precieuse......",
                  style: TextStyle(
                      color: Design.colorSecondTexte,
                      fontSize: Design.secondText),
                ),
              ),
              // const Padding(
              //   padding: EdgeInsets.only(top: 1.0),
              //   child: Text(
              //     "de votre histoire commence",
              //     style: TextStyle(
              //         color: Design.colorSecondTexte,
              //         fontSize: Design.secondText),
              //   ),
              // ),
              const SizedBox(
                height: 160,
              ),
              AppWidgetsBoutons.ButtonInscription(
                text: "Inscription",
                onPressed: () {
                  Navigator.push(
                    context,
                    PageTransition(
                      child: const FormulaireInscriptionComodite(),
                      type: PageTransitionType.rightToLeft,
                      childCurrent: widget,
                      duration: Duration(milliseconds: 200),
                    ),
                  );
                },
                color: Design.colorMembreComite,
              )
            ],
          ),
        ],
      ),
    );
  }
}
