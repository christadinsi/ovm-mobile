import 'package:flutter/material.dart';

class Inscription extends StatelessWidget {
  const Inscription({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back,
                color: Color.fromARGB(255, 149, 153, 179)),
            onPressed: () {
              Navigator.pop(context);
            },
          )),

      body: Center(
        child: LayoutBuilder(
          
          builder: (BuildContext context, BoxConstraints constraints) {
             double fontSize = constraints.maxWidth * 0.09999;
                    double fontSize2 = constraints.maxWidth * 0.072;
                    double fontSize3 = constraints.maxWidth * 0.07;
            return  Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(
                  width: 40,
                ),
                 Text(
                  "Inscription",
                  style: TextStyle(
                    // fontSize: 40,
                    fontSize: fontSize,
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(212, 127, 166, 1.0),
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
                 Text(
                  "Sélectionner votre profile",
                  style: TextStyle(
                    // fontSize: 30,
                    fontSize: fontSize2,
                    fontWeight: FontWeight.normal,
                    color: Colors.black87,
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
                // appel formulaire d'inscription
                GestureDetector(
                  
                  onTap: () {
                    //formulaire d'inscription mariee
                    Navigator.pushNamed(context, '/SplashScreenInscriptionMariee');
                  },
                  child: Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(212, 127, 166, 1.0),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(20),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(0,
                                    3), // décalage de l'ombre horizontalement et verticalement
                              ),
                            ],
                          ),
                          child:  Row(children: [
                            Container(
                              width: 100,
                              height: 100,
                              child: const Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Image(
                                  image: AssetImage('assets/images/welcome_4_mariee.png'),
                                  width: 100,
                                  height: 100,
                                ),
                              ),
                            ),
                          ]),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Future Mariée',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(212, 127, 166, 1.0),
                              ),
                            ),
                            Text(
                              'Créer votre profile',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 46,
                ),
                // formulaire inscription du marié
                  GestureDetector(
                  
                  onTap: () {
                    //formulaire d'inscription mariee
                    Navigator.pushNamed(context, '/SplashScreenInscriptionMarie');
                  },
                  child: Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 124, 107, 215),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(20),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(0,
                                    3), // décalage de l'ombre horizontalement et verticalement
                              ),
                            ],
                          ),
                          child:  Row(children: [
                            Container(
                              width: 100,
                              height: 100,
                              child: const Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Image(
                                  image: AssetImage('assets/images/welcome_4_marie.png'),
                                  width: 100,
                                  height: 100,
                                ),
                              ),
                            ),
                          ]),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Future Marié',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 124, 107, 215),
                              ),
                            ),
                            Text(
                              'Créer votre profile',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 46,
                ),
                // inscription membre comité
                  GestureDetector(
                  
                  onTap: () {
                    //formulaire d'inscription mariee
                    Navigator.pushNamed(context, '/SplashScreenInscriptionComite');
                  },
                  child: Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: const  Color.fromARGB(255, 82, 145, 46),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(20),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(0,
                                    3), // décalage de l'ombre horizontalement et verticalement
                              ),
                            ],
                          ),
                          child: const Row(children: [
                            Image(
                              image: AssetImage('assets/images/Plan de travail 26.png'),
                              width: 100,
                              height: 100,
                            ),
                          ]),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Membre du comité',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color:  Color.fromARGB(255, 82, 145, 46),
                              ),
                            ),
                            Text(
                              'Créer votre profile',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                        
              ],
                        ),
            );
          }
          
        ),
      ),
    );
  }
}
