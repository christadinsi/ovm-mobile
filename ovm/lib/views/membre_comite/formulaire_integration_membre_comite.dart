import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';

class FormulaireDemandeIntegrationComite extends StatelessWidget {
  const FormulaireDemandeIntegrationComite({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Design.colorMembreComite,
                      size: 30,
                    ),
                  ),
                  const SizedBox(
                    width: 40,
                  ),
                  const Text(
                    "Membre du comité",
                    style: TextStyle(fontSize: 24, color: Colors.grey),
                  )
                ],
              ),
            ),
            Image.asset('assets/images/imageIntegration.png',width: 150,height: 100,),
            const Text(
              "Demande d'integration",
              style: TextStyle(fontSize: 28, color: Design.colorMembreComite),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: TextFormField(
                  autofocus: false,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Nom :",
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: TextFormField(
                  autofocus: false,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Prenom :",
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: TextFormField(
                  autofocus: false,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Poste :",
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: TextFormField(
                  autofocus: false,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Code Mariage ****** :",
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: TextFormField(
                  autofocus: false,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Nom du future marié :",
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: TextFormField(
                  autofocus: false,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Nom de la future mariée :",
                ),
              ),
            ),
            const SizedBox(height:40,),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.green),
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white
              ),
              width: 200,
              height: 60,
              child: ElevatedButton( 
                onPressed: () {},
                child: const Center(
                  child: Text(
                    "Envoyer",
                    style: TextStyle(fontSize: 24, color: Design.colorMembreComite),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
