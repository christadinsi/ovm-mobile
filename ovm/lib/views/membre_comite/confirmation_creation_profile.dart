import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';

class ConfirmationCreationProfileComite extends StatelessWidget {
  const ConfirmationCreationProfileComite({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Color.fromARGB(153, 143, 162, 162)
          ),
          Image.asset(
            'assets/images/comite_integration.png',
            fit: BoxFit.cover,
          ),
         
          BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 5,
              sigmaY: 5,
            ),
            child: Container(
              color: Colors.black.withOpacity(0.1),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 170.0, left: 16),
            child: Container(
              height: 436,
              width: 329,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 15.0),
                    child: Text(
                      "Votre profile a été créé",
                      style: TextStyle(
                          fontSize: 28,
                          color: Design.colorMembreComite,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  const SizedBox(height: 20,),
                  Image.asset('assets/images/confirmationCreationProfile.png')
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
