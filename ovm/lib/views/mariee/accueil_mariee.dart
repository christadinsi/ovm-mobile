import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';
import 'package:ovm/views/mariee/modules/agenda/agenda.dart';
import 'package:ovm/views/mariee/modules/budget/budget.dart';
import 'package:ovm/views/mariee/modules/buffet/buffet.dart';
import 'package:ovm/views/mariee/modules/comite/comite.dart';
import 'package:ovm/views/mariee/modules/invites/invite.dart';
import 'package:ovm/views/mariee/modules/tables/table.dart';
import 'package:profile/components/widgets-boutons.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class AccueilMariee extends StatelessWidget {
  const AccueilMariee({super.key});

  @override
  Widget build(BuildContext context) {
    // var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    double fontSizeText1 =
        width * 0.08; // Taille de police adaptative pour text1
    double fontSizeText2 = width * 0.045;
    return Scaffold(
      // extendBodyBehindAppBar: true,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'assets/images/marieProfil.jpg',
            fit: BoxFit.cover,
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Column(children: [
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                 Container
                   (
                     padding: EdgeInsets.only(left: 20,),

                   width: width,
                   height: height/5,
                   color: Colors.transparent,
                   child: Column(
                     mainAxisSize: MainAxisSize.max,
                     mainAxisAlignment: MainAxisAlignment.center,
                   crossAxisAlignment: CrossAxisAlignment.start,
                     children: [
                        Text(
                    'Bonjour, Alida',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: fontSizeText1,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 5,),

                  Text(
                    'Ton mariage sera une réussite !',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: fontSizeText2,
                    ),
                  ),
                     ],
                   )
                 )
                ],
              ),
              const SizedBox(height: 150),
              ZoomTapAnimation(
                onTap: () {
                  Navigator.pushNamed(context, '/ProfileMariee');
                },
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Design.colorMariee,
                  ),
                  child: Stack(
                    children: [
                      Center(
                        child: Container(
                          width: 80,
                          height: 80,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                            child: Image.asset(
                              "assets/icones/PROFIL/menu_68_profil.png",
                              color: Colors.white,
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 5,
                        right: 5,
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Design.ColorMarie,
                          ),
                          child: const Center(
                            child: Text(
                              '5',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 2),
              const SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                  gradient: const LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: Design.gradient),
                  boxShadow: [
                    BoxShadow(
                      color: const Color.fromARGB(255, 255, 255, 254)
                          .withOpacity(0.6),
                      // color: const Color.fromARGB(255, 255, 255, 255)
                      //     .withOpacity(0.3),
                      spreadRadius: 6,
                      blurRadius: 10,
                      offset: const Offset(0, 3),
                    ),
                  ],
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      const SizedBox(height: 20),
                      ZoomTapAnimation(
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed('/PrestatairesMariee');
                        },
                        child: AppWidgetsBoutons.ButtonGlobal(
                          color: Design.ColorMarie,
                          text: 'Prestataires',
                          largeur: width / 1.2,
                        ),
                      ),
                      const SizedBox(height: 20),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Agenda(),
                          Budget(),
                          Comite(),
                        ],
                      ),
                      const SizedBox(
                        height: 1,
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Invite(),
                          Tables(),
                          Buffet(),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}