import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class Buffet extends StatelessWidget {
  const Buffet({super.key});

  @override
   Widget build(BuildContext context) {
         var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return  ZoomTapAnimation(
      onTap: () {
        Navigator.pushNamed(context, '/buffet');
      },
      child: Card(
        color: Design.colorMariee,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(0), bottomRight: Radius.circular(40)),
        ),
        child: SizedBox(
          width: width/3.4,
          height: 110,
          child:Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 50,
                  height: 50,
                  child: Image.asset(
                              "assets/icones/AGENDA/noun-caterer-455764.png",
                            ),
                ),
                const Text(
                  'Buffet',
                  style: TextStyle(fontSize: 16,
                  color: Colors.white),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}