import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class Budget extends StatelessWidget {
  const Budget({super.key});

  @override
   Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return  ZoomTapAnimation(
      onTap: () {
        Navigator.pushNamed(context, '/BudgetMariee');
      },
      child: Card(
        color: Design.colorModuleGrey,
      shape:  const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      child:  SizedBox(
        width: width/3.4,
        height: 110,
        child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: width/3,
                  height: 50,
                  child: Image.asset(
                              "assets/icones/BUDGET/profile_19_budget.png",
                            ),
                ),
                const Text(
                  'Budget',
                  style: TextStyle(fontSize: 16,
                  color: Colors.white),
                  ),
              ],
            ),
          ),
      ),
        ),
    );
  }
}