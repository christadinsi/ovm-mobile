import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class Comite extends StatelessWidget {
  const Comite({super.key});

  @override
   Widget build(BuildContext context) {
        var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return   ZoomTapAnimation(
      onTap: () {
        Navigator.pushNamed(context, '/AccueilCommite');
      },
      child: Card(
        color: Design.colorMariee,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40), bottomRight: Radius.circular(0)),
        ),
        child: SizedBox(
          width: width/3.4,
          height: 110,
          child:Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 50,
                  height: 50,
                  child: Image.asset(
                              "assets/icones/AGENDA/noun-meeting-4291788.png",
                            ),
                ),
                const Text(
                  'Comité',
                  style: TextStyle(fontSize: 16,
                  color: Colors.white),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}