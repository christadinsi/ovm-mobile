import 'package:flutter/material.dart';
import 'package:ovm/design/design.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class Agenda extends StatelessWidget {
  const Agenda({super.key});

  @override
   Widget build(BuildContext context) {
        var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return  ZoomTapAnimation(
      onTap: () {
        Navigator.pushNamed(context, '/agendaMariee');
      },

      child: Card(
        color: Design.colorMariee,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(40), bottomLeft: Radius.circular(0)),
        ),
        child: SizedBox(
          width: width/3.4,
          height: 110,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 50,
                  height: 50,
                  child: Image.asset(
                              "assets/icones/AGENDA/profile_19_agenda.png",
                            ),
                ),
                const Text(
                  'Agenda',
                  style: TextStyle(fontSize: 16,
                  color: Colors.white),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}