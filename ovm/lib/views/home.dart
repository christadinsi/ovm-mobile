import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    double fontSizeText1 =
        screenWidth * 0.06; // Taille de police adaptative pour text1
    double fontSizeText2 =
        screenWidth * 0.039; // Taille de police adaptative pour text2

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'assets/images/welcom.jpg',
            fit: BoxFit.cover,
          ),
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              // image: DecorationImage(
              //   image: AssetImage('assets/images/welcom.jpg'),
              //   fit: BoxFit.cover,
              // ),
              // color: Colors.black,
              color: Colors.black.withOpacity(0.5),
            ),
            child: Center(
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                    double fontSize = constraints.maxWidth * 0.09999;
                    double fontSize2 = constraints.maxWidth * 0.08;
                    double fontSize3 = constraints.maxWidth * 0.07;

                  return Column(
                  children: [
                    const SizedBox(
                      height: 80,
                    ),
                     Text(
                      "Bienvenue ",
                      style: TextStyle(
                        // fontSize: 60,
                        fontSize: fontSize,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                     Text(
                      "dans l'application OVM",
                      style: TextStyle(
                          // fontSize: 35,
                          fontSize: fontSize2,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    const SizedBox(
                      height: 120,
                    ),
                     Text(
                      "Votre Wedding Planner",
                      style: TextStyle(
                          // fontSize: 30,
                         fontSize: fontSize3,

                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    const SizedBox(
                      height: 190,
                    ),
                    OutlinedButton(
                      onPressed: () {
                        // Navigator.pushNamed(context, '/connexion');
                        Navigator.of(context, rootNavigator: true)
                            .pushNamed('/connexion');
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          const Color.fromRGBO(149, 153, 179, 1.0),
                        ),
                        minimumSize:
                            MaterialStateProperty.all<Size>(const Size(200, 60)),
                      ),
                      child: const Text(
                        "Connexion",
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    const SizedBox(
                      height: 55,
                    ),
                    OutlinedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          const Color.fromRGBO(212, 127, 166, 1.0),
                        ),
                        minimumSize:
                            MaterialStateProperty.all<Size>(const Size(200, 60)),
                      ),
                      onPressed: () {
                        // Navigator.pushNamed(context, '/inscription');
                        Navigator.of(context, rootNavigator: true)
                            .pushNamed('/inscription');
                      },
                      child: const Text(
                        "Inscription",
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ],
                );
                }
               
              ),
            ),
          ),
        ],
      ),
    );
  }
}
