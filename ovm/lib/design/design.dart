import 'package:flutter/material.dart';

class Design {
  //couleur  du marie
  static const Color ColorMarie = Color(0xFF7C6BD7);
  //couleur  de la mariee
  static const Color colorMariee = Color(0xFFD47FA6);
  //couleur du membre du comite
  static const Color colorMembreComite = Color(0xFF52912E);
  //couleur verte borber box de l'appli Mariée
  static const Color colorvert = Color(0xFF52912E);
  //couleur bleu borber box de l'appli Mariée
  static const Color colorBleu = Color(0xFF4666E5);
  // taille du premier texte du splash_screen des interfaces inscription marie et mariee
  static const double firstText = 32.0;
  // taille du second texte du splash_screen des interfaces inscription marie et mariee
  static const double secondText = 24.0;
  // taille du texte du bouton inscription, splash_screen des interfaces inscription marie et mariee
  static const tailleTexteBoutonInscription = 24;
  //couleur du second texte du splash_screen des interfaces inscription du marie et mariee
  static const Color colorSecondTexte = Colors.white;
  static const Color colorModuleGrey = Color(0xFF9599B3);
  static const List<Color>gradient = [
    Colors.white,
    Colors.white,
      Color.fromARGB(176, 255, 255, 255),
      Color.fromARGB(92, 255, 255, 255),
      Color.fromARGB(0, 255, 255, 255)


  ];
  
}
