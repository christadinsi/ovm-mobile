// ignore_for_file: implementation_imports



import 'package:flutter/material.dart';
import 'package:profile/profile.dart';
import 'package:profile/src/views/profile_mariee/forfait/forfait.dart';
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/demande_a_valider.dart';
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/formulaire_a_valider.dart';
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/invalider._demande.dart';

import 'package:profile/src/views/profile_mariee/infos/interface_information_mariee.dart';
import 'package:profile/src/views/profile_mariee/notification/notification.dart';
import 'package:profile/src/views/profile_mariee/synchronisation/synchronisation.dart';

class AppRouterProfile {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/ProfileMariee':
        return MaterialPageRoute(builder: (context) => const ProfileMariee());
      case '/Notifications':
        return MaterialPageRoute(builder: (context) => const Notifications());
      case '/InfoMariee':
        return MaterialPageRoute(builder: (context) => const InfoMariee());
      case '/ForfaitMariee':
        return MaterialPageRoute(builder: (context) => const ForfaitMariee());

      // case '/ProgressionMariee':
      //   return MaterialPageRoute(
      //       builder: (context) => const ProgressionMariee());
      case '/Synchronisation':
        return MaterialPageRoute(builder: (context) => const Synchronisation());
      //  case '/AccueilMariee':
      //   return MaterialPageRoute(
      //     builder: (context) => const AccueilMariee(),

      //   );
      case 'FormulaireAValiderMariee':
        return MaterialPageRoute(builder: (context) => PageValidation() );
      case 'FormulaireInvalideMariee':
        return MaterialPageRoute(builder: (context) => PageInvalide() );
      case '/DemandeAValider':
        return MaterialPageRoute(builder: (context) => const DemandeAValider());
      default:
        // Gérer les routes inconnues en renvoyant une page par défaut ou une erreur 404, par exemple :
        return null;
    }
  }
}
