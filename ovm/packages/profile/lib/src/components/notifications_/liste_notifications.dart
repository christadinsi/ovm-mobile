import 'package:flutter/material.dart';
import 'package:profile/design/design.dart';

class ListNotification extends StatelessWidget {
  ListNotification({super.key});

  final List<Map<String, dynamic>> iconsWithText = [
    {
      'icon': 'assets/icones/AGENDA/profile_19_agenda.png',
      'text1': 'Agenda',
      'text2': 'Consulter les notifications '
    },
    {
      'icon': 'assets/icones/BUDGET/profile_19_budget.png',
      'text1': 'Budget',
      'text2': 'Consulter les notifications'
    },
    {
      'icon': 'assets/icones/AGENDA/noun-meeting-4291788.png',
      'text1': 'Comité',
      'text2': 'Consulter les notifications'
    },
    {
      'icon': 'assets/icones/AGENDA/iconInviteAgenda.png',
      'text1': 'Invités',
      'text2': 'Consulter les notifications'
    },
    {
      'icon': 'assets/icones/AGENDA/noun-dining-table-35827.png',
      'text1': 'Tables',
      'text2': 'Consulter les notifications'
    },
    {
      'icon': 'assets/icones/AGENDA/noun-caterer-455764.png',
      'text1': 'Buffet',
      'text2': 'Consulter les notifications'
    },
  ];

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return ListView.builder(
      itemCount: iconsWithText.length,
      itemBuilder: (BuildContext context, int index) {
        double fontSizeText1 =
            screenWidth * 0.06; // Taille de police adaptative pour text1
        double fontSizeText2 =
            screenWidth * 0.039; // Taille de police adaptative pour text2

        return GestureDetector(
          onTap: () {
            // Ajoutez ici la logique à exécuter lors du clic sur la rangée
            // print('Ligne $index cliquée');
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28),
            child: Container(
              height: screenHeight / 10,
              width: screenWidth,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                    100), // Appliquer une bordure arrondie uniquement au premier conteneur
                border: Border.all(
                  color: Design.ColorMarie, // Couleur de la bordure
                  width: 2, // Épaisseur de la bordure
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        width: 55,
                        height: 55,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Design.ColorMarie, // Couleur de la bordure
                            width: 2, // Épaisseur de la bordure
                          ),
                          shape: BoxShape.circle,
                          color: Colors.transparent,
                        ),
                        child: Image.asset(iconsWithText[index]['icon'],
                            width: 40, height: 40)),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    flex: 4,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 9),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            iconsWithText[index]['text1'],
                            style:  TextStyle(
                                color: Design.ColorMarie,
                                // fontSize: 25,
                                fontSize: fontSizeText1,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            iconsWithText[index]['text2'],
                            style:  TextStyle(
                                color: Design.colorModuleGrey,
                                //  fontSize: 18
                                 fontSize: fontSizeText2,
                                 ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  // IconData getIconData(String iconName) {
  //   switch (iconName) {
  //     case 'agenda':
  //       return Icons.calendar_today;
  //     case 'budget':
  //       return Icons.attach_money;
  //     // ... Ajoutez les autres icônes ici avec leurs noms respectifs
  //     default:
  //       return Icons.error;
  //   }
  // }
}
