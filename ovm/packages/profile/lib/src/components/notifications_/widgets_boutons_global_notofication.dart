import 'package:flutter/material.dart';
import 'package:profile/design/design.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class ButtonGlobalNotification extends StatelessWidget {
  const ButtonGlobalNotification(
      {super.key,
      required this.text,
      // required this.onTap,
      required this.color,
      required this.largeur,
      required this.hauteur,
      required this.image,
      required this.textNbre});
  final String text;
  // final Function onTap;
  final Color color;
  final double largeur;
  final double hauteur;
  final String image;
  final String textNbre;
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
     double fontSizeText =
        screenWidth * 0.054;
         double fontSizeText1 =
        screenWidth * 0.06;
    return SizedBox(
      height: screenHeight / 10,
    width: screenWidth,
      child: Container(
        // Premier conteneur
        padding: const EdgeInsets.symmetric(horizontal: 10),
        margin: const EdgeInsets.symmetric(
            horizontal:
                5), // Ajoutez une marge horizontale de 20 points de chaque côté
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: color,
        ),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment:
                MainAxisAlignment.center, // Centrer les éléments de la ligne
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    // margin: const EdgeInsets.only(right: 10),
                    padding: const EdgeInsets.all(10),
                    width: 150,
                    height: 150,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Design.colorSecondTexte,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(2),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image.asset(
                          image,
                          height:
                              60, // Nouvelle taille de l'image adaptée à la taille du conteneur
                          width:
                              60, // Nouvelle taille de l'image adaptée à la taille du conteneur
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  ),
              const SizedBox(width: 10),
              //  const  SizedBox(width: 10), // Ajoutez un espace de 10 points entre l'image et le texte
              Expanded(
                flex: 3,
                child: Text(
                  text,
                  style:  TextStyle(
                      color: Design.colorSecondTexte, 
                      // fontSize: 19
                      fontSize: fontSizeText,
                      ),
                textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(width: 10),
              // Ajoutez un espace de 10 points entre les textes
              Expanded(
                  flex: 1,
                  child: Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Design.colorSecondTexte,
                    ),
                    width: 25,
                    height: 25,
                    child: Center(
                      // Centrer le texte dans le cercle
                      child: Text(
                        textNbre,
                        style:  TextStyle(
                          color: const Color.fromARGB(255, 0, 0, 0),
                          // fontSize: 19, // Réduire la taille du texte pour tenir dans le cercle
                        fontSize: fontSizeText1,
                        ),
                      ),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
