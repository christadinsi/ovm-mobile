import 'package:flutter/material.dart';
import 'package:profile/components/widgets-title.dart';

import '../../../../../../components/widgets-boutons.dart';
import '../../../../../../design/design.dart';
import 'package:auto_size_text/auto_size_text.dart';

//interface information de la mariee
class InfoMariee extends StatefulWidget {
  const InfoMariee({super.key});

  @override
  State<InfoMariee> createState() => _InfoMarieeState();
}

class _InfoMarieeState extends State<InfoMariee> {
//basculer entre l'interface Moi et Mariage
  bool basculer = true;

  _editerInformationMoi() {
    showDialog(
        context: context,
        builder: (context) {
          final double myWidth = MediaQuery.of(context).size.width;
          final double myHeigth = MediaQuery.of(context).size.height;
          return AlertDialog(
            backgroundColor: Colors.white,
            title: const Text(
              "Modifier cette information ",
              style: TextStyle(color: Design.ColorMarie, fontSize: 30),
            ),
            icon: Image.asset(
              "assets/icones/PROFIL/onboarding_24_informations.png",
              width: myWidth / 10,
              height: myHeigth / 10,
            ),
            content: TextFormField(
              initialValue: "BOUMBA",
            ),
            actions: [
              Center(
                  child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Design.ColorMarie)),
                      onPressed: () {
                        //modification dans la bdd
                        Navigator.pop(context);
                      },
                      child: const Text(
                        "Modifier",
                        style: TextStyle(
                          color: Design.colorSecondTexte,
                          fontSize: 25,
                        ),
                      )))
            ],
          );
        });
  }

  _editerInformationMariage() {
    showDialog(
        context: context,
        builder: (context) {
          final double myWidth = MediaQuery.of(context).size.width;
          final double myHeigth = MediaQuery.of(context).size.height;
          return AlertDialog(
            backgroundColor: Colors.white,
            title: const Text(
              "Modifier cette information ",
              style: TextStyle(color: Design.colorMariee, fontSize: 30),
            ),
            icon: Image.asset(
              "assets/icones/PROFIL/onboarding_24_informations.png",
              width: myWidth / 10,
              height: myHeigth / 10,
            ),
            content: TextFormField(
              initialValue: "200",
            ),
            actions: [
              Center(
                  child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Design.colorMariee)),
                      onPressed: () {
                        //modification dans la bdd
                        Navigator.pop(context);
                      },
                      child: const Text(
                        "Modifier",
                        style: TextStyle(
                          color: Design.colorSecondTexte,
                          fontSize: 25,
                        ),
                      )))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Colors.white,
            title: const AutoSizeText(
              "Profil",
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.grey,
                  fontWeight: FontWeight.bold),
              maxLines: 1,
              minFontSize: 12,
            ),
            centerTitle: true,
            leading: const BackButton(
              color: Colors.grey,
            )),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(
                left: myWidth / 20,
              ),
              child: AppWidgetsTitles.bar(),
            ),
            const AutoSizeText(
              "Informations",
              style: TextStyle(
                  color: Design.ColorMarie,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2),
              maxLines: 1,
              minFontSize: 12,
            ),
            Image.asset(
              "assets/icones/PROFIL/onboarding_24_informations.png",
              width: myWidth / 2,
              height: myHeigth / 7,
            ),
            const AutoSizeText(
              "Des éléments essentiels",
              style: TextStyle(
                fontSize: 25,
                color: Color.fromRGBO(139, 139, 139, 1),
              ),
              maxLines: 1,
              minFontSize: 12,
            ),
            SizedBox(
              width: myWidth / 1.4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      const AutoSizeText(
                        "Moi",
                        style: TextStyle(
                            fontSize: 20,
                            color: Design.ColorMarie,
                            fontWeight: FontWeight.bold),
                        maxLines: 1,
                        minFontSize: 12,
                      ),
                      Builder(builder: (context) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              //si bascule est false, affiche l'interface Moi
                              basculer = false;
                              Scaffold.of(context).openDrawer();
                            });
                          },
                          child: Card(
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            elevation: 6,
                            child: Container(
                              width: myWidth / 4,
                              height: myHeigth / 6,
                              decoration: const BoxDecoration(
                                border: Border(
                                  top: BorderSide(
                                      color: Design.ColorMarie, width: 3),
                                  bottom: BorderSide(
                                      color: Design.ColorMarie, width: 3),
                                  left: BorderSide(
                                      color: Design.ColorMarie, width: 3),
                                  right: BorderSide(
                                      color: Design.ColorMarie, width: 3),
                                ),
                                color: Design.colorSecondTexte,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(50),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(7),
                                child: Center(
                                  child: Image.asset(
                                    "assets/icones/PROFIL/onboarding_153_iconr_marie.png",
                                    width: myWidth / 1,
                                    height: myHeigth / 1,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                    ],
                  ),
                  Column(
                    children: [
                      const AutoSizeText(
                        "Mariage",
                        style: TextStyle(
                            fontSize: 20,
                            color: Design.colorMariee2,
                            fontWeight: FontWeight.bold),
                        maxLines: 1,
                        minFontSize: 12,
                      ),
                      Builder(builder: (context) {
                        return InkWell(
                          onTap: () {
                            setState(() {
                              //si bascule est true, affiche l'interface Mariage
                              basculer = true;
                              Scaffold.of(context).openDrawer();
                            });
                          },
                          child: Card(
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            elevation: 6,
                            child: Container(
                              width: myWidth / 4,
                              height: myHeigth / 6,
                              decoration: const BoxDecoration(
                                border: Border(
                                  top: BorderSide(
                                      color: Design.colorMariee2, width: 3),
                                  bottom: BorderSide(
                                      color: Design.colorMariee2, width: 3),
                                  left: BorderSide(
                                      color: Design.colorMariee2, width: 3),
                                  right: BorderSide(
                                      color: Design.colorMariee2, width: 3),
                                ),
                                color: Design.colorSecondTexte,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(50),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(18),
                                child: Center(
                                  child: Image.asset(
                                    "assets/icones/AGENDA/onboarding_34_alliance_.png",
                                    width: myWidth / 2,
                                    height: myHeigth / 1,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      })
                    ],
                  )
                ],
              ),
            ),
            //bouton de retour à l'accueil
            Center(
              child: AppWidgetsBoutons.boutonRetour(
                  colorbouton: Colors.grey,
                  height: myHeigth / 16,
                  width: myWidth / 1.6,
                  onPressed: () {
                    Navigator.pushNamed(context, '/AccueilMariee');
                  }),
            )
          ],
        ),
        drawer: basculer
            ? SizedBox(
                height: myHeigth / 1.1,
                child: Drawer(
                  clipBehavior: Clip.antiAlias,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(90),
                          bottomRight: Radius.circular(90))),
                  backgroundColor: Colors.white,
                  width: myWidth / 1.1,
                  child: Column(
                    children: [
                      Container(
                        color: Design.colorMariee2,
                        width: myWidth / 1.1,
                        height: myHeigth / 4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/icones/AGENDA/onboarding_34_alliance_.png",
                              width: myWidth / 3.8,
                              height: myHeigth / 6,
                            ),
                            const AutoSizeText(
                              "Votre mariage",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              maxLines: 1,
                              minFontSize: 12,
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsetsDirectional.only(
                          start: 25,
                          end: 25,
                        ),
                        width: myWidth / 1,
                        height: myHeigth / 1.7,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMariage();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Nombre d'invités :",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "200 personnes",
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Design.colorMariee2),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            const Divider(
                              color: Color.fromARGB(255, 219, 219, 219),
                            ),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMariage();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Thème couleur :",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "Rouge et bleu",
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Design.colorMariee2),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            const Divider(
                              color: Color.fromARGB(255, 219, 219, 219),
                            ),
                            SizedBox(
                              width: myWidth / 1,
                              child: const Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText(
                                    "Code mariage :",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                    minFontSize: 12,
                                  ),
                                  AutoSizeText(
                                    "M06A05E23",
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Design.colorMariee2),
                                    maxLines: 1,
                                    minFontSize: 12,
                                  ),
                                ],
                              ),
                            ),
                            const Divider(
                              color: Color.fromARGB(255, 219, 219, 219),
                            ),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMariage();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Date cérémonie :",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "15/05/2023",
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Design.colorMariee2),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            const Divider(
                              color: Color.fromARGB(255, 219, 219, 219),
                            ),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMariage();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Lieu cérémonie :",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "Camp de Gaulle",
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Design.colorMariee2),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            const Divider(
                              color: Color.fromARGB(255, 219, 219, 219),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            : SizedBox(
                height: myHeigth / 1.1,
                child: Drawer(
                  clipBehavior: Clip.antiAlias,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(90),
                          bottomRight: Radius.circular(90))),
                  backgroundColor: Colors.white,
                  width: myWidth / 1.1,
                  child: Column(
                    children: [
                      Container(
                        color: Design.ColorMarie,
                        width: myWidth / 1.1,
                        height: myHeigth / 4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/icones/PROFIL/onboarding_153_iconr_marie.png",
                              width: myWidth / 2,
                              height: myHeigth / 9,
                            ),
                            const AutoSizeText(
                              "La future",
                              style: TextStyle(
                                  fontSize: 27,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              maxLines: 1,
                              minFontSize: 12,
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsetsDirectional.only(
                            start: 25, end: 25),
                        width: myWidth / 1,
                        height: myHeigth / 1.6,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMoi();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Divider(
                                        color: Color.fromARGB(0, 255, 255, 255),
                                      ),
                                      AutoSizeText(
                                        "Nom (s) :",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "BOUMBA ",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Design.ColorMarie),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      Divider(
                                        color:
                                            Color.fromARGB(255, 219, 219, 219),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMoi();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Prénom (s) :",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "Jessica",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Design.ColorMarie),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      Divider(
                                        color:
                                            Color.fromARGB(255, 219, 219, 219),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            SizedBox(
                              width: myWidth / 1,
                              child: const Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText(
                                    "Code mariage :",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                    minFontSize: 12,
                                  ),
                                  AutoSizeText(
                                    "M06A05E23",
                                    style: TextStyle(
                                        fontSize: 18, color: Design.ColorMarie),
                                    maxLines: 1,
                                    minFontSize: 12,
                                  ),
                                  Divider(
                                    color: Color.fromARGB(255, 219, 219, 219),
                                  ),
                                ],
                              ),
                            ),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMoi();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Tel :",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "077 15 35 66",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Design.ColorMarie),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      Divider(
                                        color:
                                            Color.fromARGB(255, 219, 219, 219),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMoi();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "Mot de passe :",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "********",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Design.ColorMarie),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      Divider(
                                        color:
                                            Color.fromARGB(255, 219, 219, 219),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                            Builder(builder: (context) {
                              return InkWell(
                                onTap: () {
                                  _editerInformationMoi();
                                },
                                child: SizedBox(
                                  width: myWidth / 1,
                                  child: const Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AutoSizeText(
                                        "E-mail :",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      AutoSizeText(
                                        "boumba15@gmail.com",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Design.ColorMarie),
                                        maxLines: 1,
                                        minFontSize: 12,
                                      ),
                                      Divider(
                                        color:
                                            Color.fromARGB(255, 219, 219, 219),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ));
  }
}
