import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:profile/design/design.dart';
import 'package:profile/src/components/notifications_/liste_notifications.dart';
import 'package:profile/src/components/notifications_/widgets_boutons_global_notofication.dart';
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/demande_a_valider.dart';
import 'package:profile/src/views/profile_mariee/synchronisation/synchronisation.dart';
import 'package:zoom_tap_animation/zoom_tap_animation.dart';

class Notifications extends StatefulWidget {
  const Notifications({super.key});

  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    late final AnimationController _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    late final Animation<double> _animation =
        CurvedAnimation(parent: _controller, curve: Curves.fastOutSlowIn);

    @override
    void dispose() {
      _controller.dispose();
      super.dispose();
    }

    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    double fontSizeText1 =
        screenWidth * 0.058; // Taille de police adaptative pour text1

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
            title: const Text(
              'Notifications',
              style: TextStyle(color: Design.colorModuleGrey),
            ),
            backgroundColor: Colors.white.withOpacity(0.0),
            forceMaterialTransparency: true,
            elevation:0,
            centerTitle: true,
            leading: IconButton(
              color: Design.colorModuleGrey,
              icon: const Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(),
            )),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                padding: const EdgeInsets.all(30.0),
                color: Colors.white,
                height: screenHeight,
                width: screenWidth, // Largeur qui s'adapte
                child: Center(
                  child: Column(
                    children: [
                      ZoomTapAnimation(
                        onTap: () {
                          //  Navigator.of(context).pushNamed('/DemandeAValider');
                          Navigator.push(
                            context,
                           PageTransition(child: DemandeAValider(),
                            type:   PageTransitionType.leftToRight,
                            duration: Duration(microseconds: 10000),
                           
                            )
                          );
                        },
                        child: ButtonGlobalNotification(
                          color: const Color(0xFF7C6BD7),
                          image: "assets/icones/PROFIL/menu_68_profil.png",
                          text: 'Demandes à valider',
                          textNbre: '3',
                          largeur: screenWidth,
                          hauteur: screenHeight,
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      ZoomTapAnimation(
                        onTap: () {
                          // Navigator.of(context).pushNamed('DemandeAValider');
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Synchronisation()),
                          );
                        },
                        child: ButtonGlobalNotification(
                          color: Design.colorMembreComite,
                          image:
                              "assets/icones/PROFIL/onboarding_24_informations.png",
                          text: 'Synchronisés',
                          textNbre: '1',
                          largeur: screenWidth,
                          hauteur: screenHeight,
                        ),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Center(
                        child: Text(
                          'Liste des notifications',
                          style: TextStyle(
                            color: const Color.fromARGB(255, 0, 0, 0),
                            // fontSize: 28
                            fontSize: fontSizeText1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
                // Deuxième colonne contenant des éléments scrollables
                flex: 1, // Ajuste la largeur de la deuxième colonne
                child: ListNotification()
                // ListView.builder(
                //   itemCount: 50, // Nombre d'éléments dans la liste
                //   itemBuilder: (BuildContext context, int index) {
                //     return ListTile(
                //       title: Text('Élément $index'),
                //     );
                //   },
                // ),
                ),
          ],
        ),
      ),
    );
  }
}
