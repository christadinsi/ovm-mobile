import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/formulaire_a_valider.dart';
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/invalider._demande.dart';

class DemandeAValider extends StatefulWidget {
  const DemandeAValider({super.key});

  @override
  State<DemandeAValider> createState() => _DemandeAValiderState();
}

class _DemandeAValiderState extends State<DemandeAValider> {
 
  @override
  Widget build(BuildContext context) {
    final double myw = MediaQuery.of(context).size.width;
    final double myh = MediaQuery.of(context).size.height;

    // function confirmation
   

    return Scaffold(
        appBar: AppBar(
          leading: const BackButton(
            color: Color(0xFF9599B3),
          ),
          title: const Text(
            "Notification",
            style: TextStyle(color: Color(0xFF9599B3)),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: const Icon(Icons.dehaze_sharp),
              color: Colors.grey,
              tooltip: 'Setting Icon',
              onPressed: () {},
            ),
          ],
        ),
        body: Column(children: [
          SizedBox(
            height: myh * .06,
          ),
          Container(
            margin: EdgeInsets.only(left: myw * .32, right: myw * .33),
            width: myw * .40,
            height: myh * .178,
            // color: Colors.amber,
            child: Image.asset(
              "assets/icones/PROFIL/menu_68_profil.png",
            ),
          ),
          SizedBox(
            height: myh * .03,
          ),
          const Text(
            "Demande a valider",
            style: TextStyle(
                color: Color(0xFF7C6BD7),
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: myh * .03,
          ),
          Container(
            width: myw,
            height: myh / 1.9,
            // color: Colors.amber,
            child: ListView(children: [
              Container(
                margin: EdgeInsets.only(left: myw * .075, right: myw * .075),
                width: myw * 1.02,
                height: myh * .21,
                // color: const Color.fromARGB(255, 239, 50, 50),
                decoration: BoxDecoration(
                    border:
                        Border.all(width: 2, color: const Color(0xFFA17CFF)),
                    borderRadius: BorderRadius.circular(24),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.white, spreadRadius: .5, blurRadius: 6)
                    ]),
                child: Column(
                  children: [
                    Flexible(
                        flex: 20,
                        child: Container(
                            margin: EdgeInsets.only(
                                left: myw * .23, right: myw * .04, top: 10),
                            child: const Text(
                              "Obame jean-pierre",
                              style: TextStyle(
                                  color: Color(0xFF7C6BD7), fontSize: 18),
                              textAlign: TextAlign.right,
                            ))),
                    const SizedBox(
                      width: 18,
                    ),
                    Flexible(
                        flex: 80,
                        child: Container(
                          width: myw,
                          height: myh / 3,
                          // color: Colors.red,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Flexible(
                                  flex: 30,
                                  child: Container(
                                      padding: const EdgeInsets.all(10),
                                      child: Image.asset(
                                          "assets/icones/INVITES/onboarding_198_user_+.png"))),
                              Flexible(
                                  flex: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                     const Flexible(
                                          flex: 30,
                                          child: Text(
                                            "Integrer au comite ?",
                                            style: TextStyle(
                                                color: Color(0xFF241332),
                                                fontSize: 18),
                                          )),
                                      Flexible(
                                        flex: 70,
                                        child: Container(
                                          width: myw / 1.7,
                                          height: myh,
                                          //  color: Colors.blue,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF7C6BD7)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: (){
                                                          Navigator.push(
                                                          context,
                                                          PageTransition(
                                                            child: PageValidation(),
                                                             type: PageTransitionType.rightToLeft,
                                                             duration: Duration(milliseconds: 1000)
                                                             )
                                                        );
                                                        }
                                                    
                                                    ,
                                                    icon: Icon(
                                                      Icons.check,
                                                      size: 40,
                                                      color: Colors.amber
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF9599B3)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100),
                                                    color: const Color(
                                                        0xFF9599B3)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {
                                                         Navigator.push(
                                                          context,
                                                          PageTransition(
                                                            child: PageInvalide(),
                                                             type: PageTransitionType.rightToLeft,
                                                             duration: Duration(milliseconds: 1000)
                                                             )
                                                        );
                                                             
                                                    },
                                                    icon: Icon(
                                                      Icons.close,
                                                      size: 40,
                                                      color: Colors.white
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.only(left: myw * .075, right: myw * .075),
                width: myw * 1.02,
                height: myh * .21,
                // color: const Color.fromARGB(255, 239, 50, 50),
                decoration: BoxDecoration(
                    border:
                        Border.all(width: 2, color: const Color(0xFFA17CFF)),
                    borderRadius: BorderRadius.circular(24),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.white, spreadRadius: .5, blurRadius: 6)
                    ]),
                child: Column(
                  children: [
                    Flexible(
                        flex: 20,
                        child: Container(
                            margin: EdgeInsets.only(
                                left: myw * .23, right: myw * .04, top: 10),
                            child: const Text(
                              "Koumba audrey",
                              style: TextStyle(
                                  color: Color(0xFF7C6BD7), fontSize: 18),
                              textAlign: TextAlign.right,
                            ))),
                    const SizedBox(
                      width: 18,
                    ),
                    Flexible(
                        flex: 80,
                        child: Container(
                          width: myw,
                          height: myh / 3,
                          // color: Colors.red,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Flexible(
                                  flex: 30,
                                  child: Container(
                                      padding: const EdgeInsets.all(10),
                                      child: Image.asset(
                                          "assets/icones/INVITES/onboarding_198_user_+.png"))),
                              Flexible(
                                  flex: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      const Flexible(
                                          flex: 30,
                                          child: Text(
                                            "Integrer au comite ?",
                                            style: TextStyle(
                                                color: Color(0xFF241332),
                                                fontSize: 18),
                                          )),
                                      Flexible(
                                        flex: 70,
                                        child: Container(
                                          width: myw / 1.7,
                                          height: myh,
                                          //  color: Colors.blue,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF7C6BD7)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {},
                                                    icon: Icon(
                                                      Icons.check,
                                                      size: 40,
                                                      color: Colors.amber
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF9599B3)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100),
                                                    color: const Color(
                                                        0xFF9599B3)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {},
                                                    icon: Icon(
                                                      Icons.close,
                                                      size: 40,
                                                      color: Colors.white
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.only(left: myw * .075, right: myw * .075),
                width: myw * 1.02,
                height: myh * .21,
                // color: const Color.fromARGB(255, 239, 50, 50),
                decoration: BoxDecoration(
                    border:
                        Border.all(width: 2, color: const Color(0xFFA17CFF)),
                    borderRadius: BorderRadius.circular(24),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.white, spreadRadius: .5, blurRadius: 6)
                    ]),
                child: Column(
                  children: [
                    Flexible(
                        flex: 20,
                        child: Container(
                            margin: EdgeInsets.only(
                                left: myw * .23, right: myw * .04, top: 10),
                            child: const Text(
                              "Ayo alexandre",
                              style: TextStyle(
                                  color: Color(0xFF7C6BD7), fontSize: 18),
                              textAlign: TextAlign.right,
                            ))),
                    const SizedBox(
                      width: 18,
                    ),
                    Flexible(
                        flex: 80,
                        child: Container(
                          width: myw,
                          height: myh / 3,
                          // color: Colors.red,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Flexible(
                                  flex: 30,
                                  child: Container(
                                      padding: const EdgeInsets.all(10),
                                      child: Image.asset(
                                          "assets/icones/INVITES/onboarding_198_user_+.png"))),
                              Flexible(
                                  flex: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      const Flexible(
                                          flex: 30,
                                          child: Text(
                                            "Integrer au comite ?",
                                            style: TextStyle(
                                                color: Color(0xFF241332),
                                                fontSize: 18),
                                          )),
                                      Flexible(
                                        flex: 70,
                                        child: Container(
                                          width: myw / 1.7,
                                          height: myh,
                                          //  color: Colors.blue,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF7C6BD7)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {},
                                                    icon: Icon(
                                                      Icons.check,
                                                      size: 40,
                                                      color: Colors.amber
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF9599B3)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100),
                                                    color: const Color(
                                                        0xFF9599B3)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {},
                                                    icon: Icon(
                                                      Icons.close,
                                                      size: 40,
                                                      color: Colors.white
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.only(left: myw * .075, right: myw * .075),
                width: myw * 1.02,
                height: myh * .21,
                // color: const Color.fromARGB(255, 239, 50, 50),
                decoration: BoxDecoration(
                    border:
                        Border.all(width: 2, color: const Color(0xFFA17CFF)),
                    borderRadius: BorderRadius.circular(24),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.white, spreadRadius: .5, blurRadius: 6)
                    ]),
                child: Column(
                  children: [
                    Flexible(
                        flex: 20,
                        child: Container(
                            margin: EdgeInsets.only(
                                left: myw * .23, right: myw * .04, top: 10),
                            child: const Text(
                              "Obame jean-pierre",
                              style: TextStyle(
                                  color: Color(0xFF7C6BD7), fontSize: 18),
                              textAlign: TextAlign.right,
                            ))),
                    const SizedBox(
                      width: 18,
                    ),
                    Flexible(
                        flex: 80,
                        child: Container(
                          width: myw,
                          height: myh / 3,
                          // color: Colors.red,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Flexible(
                                  flex: 30,
                                  child: Container(
                                      padding: const EdgeInsets.all(10),
                                      child: Image.asset(
                                          "assets/icones/INVITES/onboarding_198_user_+.png"))),
                              Flexible(
                                  flex: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      const Flexible(
                                          flex: 30,
                                          child: Text(
                                            "Integrer au comite ?",
                                            style: TextStyle(
                                                color: Color(0xFF241332),
                                                fontSize: 18),
                                          )),
                                      Flexible(
                                        flex: 70,
                                        child: Container(
                                          width: myw / 1.7,
                                          height: myh,
                                          //  color: Colors.blue,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF7C6BD7)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {},
                                                    icon: Icon(
                                                      Icons.check,
                                                      size: 40,
                                                      color: Colors.amber
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        width: 2,
                                                        color: const Color(
                                                            0xFF9599B3)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            100),
                                                    color: const Color(
                                                        0xFF9599B3)),
                                                width: myw * .19,
                                                height: myh * .082,
                                                child: IconButton(
                                                    onPressed: () {},
                                                    icon: Icon(
                                                      Icons.close,
                                                      size: 40,
                                                      color: Colors.white
                                                          .withOpacity(.5),
                                                    )),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
            ]),
          ),
        ]));
  }
}

