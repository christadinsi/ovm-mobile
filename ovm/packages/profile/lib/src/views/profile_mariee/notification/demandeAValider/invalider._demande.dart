import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:profile/src/views/profile_mariee/notification/demandeAValider/demande_a_valider.dart';

class PageInvalide extends StatefulWidget {
  const PageInvalide({super.key});

  @override
  State<PageInvalide> createState() => _PageValidationState();
}

class _PageValidationState extends State<PageInvalide> {
  @override
  Widget build(BuildContext context) {
    final double myw = MediaQuery.of(context).size.width;
    final double myh = MediaQuery.of(context).size.height;

    return Material(
      child: Stack(
        children: [
          DemandeAValider(),
          Positioned(
              child: Stack(
            alignment: Alignment.center,
            children: [
              BackdropFilter(
                filter:  ui.ImageFilter.blur(
            sigmaX: 8.0,
            sigmaY: 8.0,
          ),
                child: Container(
                  width: myw,
                  height: myh,
                  decoration: BoxDecoration(
                      //  color: Color(0xFF998FA2).withOpacity(.6),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xFF998FA2).withOpacity(.6), blurRadius: 9)
                      ]),
                ),
              ),
              Positioned(
                child:Container(
                width: myw*.75,
                height: myh*.4,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40)
                ),
                child: Column(
                  children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: myw*.33,
                    height: myh*.138,
                    child: Image.asset("assets/icones/lien-rompu.png",fit: BoxFit.contain,),
                  ),
                  SizedBox(height: 20,),
                Container(child: const  Text("Obame jean-pierre",style: TextStyle(color: Color(0xFF9599B3),fontSize: 20),)),
                SizedBox(height: 50,),
                Container(
                margin: EdgeInsets.only(left: 3,right: 3),
                  child: Text("Integration au comite \n  \n rejetee !",style: TextStyle(fontSize: 14,color: Color(0xFFBF3E65)),))
                  ],
                ),
              )),
            ],
          ))
        ],
      ),
    );
  }
}
