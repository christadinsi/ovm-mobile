import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:profile/src/views/profile_mariee/forfait/forfait.dart';
import 'package:profile/src/views/profile_mariee/infos/interface_information_mariee.dart';
import 'package:profile/src/views/profile_mariee/notification/notification.dart';
import 'package:profile/src/views/profile_mariee/synchronisation/synchronisation.dart';

class ProfileMariee extends StatelessWidget {
  const ProfileMariee({super.key});

  @override
  Widget build(BuildContext context) {
    final double mywidth = MediaQuery.of(context).size.width;
    final double myheight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Profil",
          style:
              TextStyle(color: Color(0xFF7C6BD7), fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Color(0xFF7C6BD7),
        ),
        centerTitle: true,
      ),
      body: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // trait

          Container(
            margin: EdgeInsets.only(right: mywidth * .7),
            width: mywidth / 3,
            height: myheight / 20,
            // color: Colors.red,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  width: 47,
                  height: 5,
                  decoration: BoxDecoration(
                      color: const Color(0xFF9599B3),
                      borderRadius: BorderRadius.circular(150)),
                ),
                const SizedBox(
                  width: 3,
                ),
                Container(
                  width: 8,
                  height: 5,
                  decoration: BoxDecoration(
                      color: const Color(0xFF9599B3).withOpacity(.6),
                      borderRadius: BorderRadius.circular(150)),
                ),
                const SizedBox(
                  width: 2,
                ),
                Container(
                  width: 8,
                  height: 5,
                  decoration: BoxDecoration(
                      color: const Color(0xFF9599B3).withOpacity(.6),
                      borderRadius: BorderRadius.circular(150)),
                ),
              ],
            ),
          ),

          // fin premier trait
          // debut premier petit trait

          const SizedBox(
            height: 25,
          ),
          Stack(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                // margin: const EdgeInsets.only(left: 112, right: 109),
                width: mywidth / 2.5,
                height: myheight / 5.2,
                decoration: BoxDecoration(
                    // color: Colors.white,
                    border:
                        Border.all(width: 2, color: const Color(0xFF7C6BD7)),
                    borderRadius: BorderRadius.circular(140)),
                child: const CircleAvatar(
                  radius: 152,
                  backgroundColor: Color(0xFF7C6BD7),
                  backgroundImage: AssetImage("assets/images/testProfile.jpg"),
                ),
              ),
              Positioned(
                  right: -3,
                  bottom: 10,
                  child: Container(
                      margin: const EdgeInsets.all(2),
                      width: mywidth * .125,
                      height: myheight * .053,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(50)),
                      child: Container(
                          decoration: const BoxDecoration(),
                          child: Icon(
                            Icons.add_a_photo,
                            color: const Color(0xFF7C6BD7).withOpacity(.6),
                          ))))
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            // margin: const EdgeInsets.only(left: 43, right: 43),
            child: Text(
              "BOUMBA jessica",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 35,
                  color: const Color(0xFF7C6BD7).withOpacity(.8)),
            ),
          ),
          // notification bloc
          // const SizedBox(
          //   height: 10,
          // ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                // padding: EdgeInsets.only(top: 5),
                width: mywidth * .38,
                height: myheight * .13,

                decoration: const BoxDecoration(

                    // color: Colors.white,
                    // border: Border.all(color: Color(0xFF707070)),
                    ),
                child: Column(
                 crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        // Navigator.pushNamed(context, '/Notifications');
                      
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const Notifications(),
                            type: PageTransitionType.rightToLeft,
                            //childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                      },
                      child: Stack(
                        children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                           width: mywidth*.18,
                            height: myheight*.08,
                          // color: Colors.red,
                          child: Icon(
                            Icons.notifications,
                            size: 40,
                            color: const Color(0xFF9599B3).withOpacity(.6),
                          ),
                        ),
                        Positioned(
                          bottom: 33,
                          right: 2,
                          child: Container(
                            padding: const EdgeInsets.all(2),
                            width: mywidth * .078,
                            height: myheight * .033,
                            decoration: BoxDecoration(
                                color: const Color(0xFFD47FA6),
                                borderRadius: BorderRadius.circular(50)),
                            child: const Text(
                              "5",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      ]),
                    ),

                    
                    const Text(
                      "Notifications",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              const SizedBox(
                width: 30,
              ),


              // Container(
              //   width: mywidth * .38,
              //   height: myheight * .13,
              //   decoration: const BoxDecoration(

              //       // color: Colors.white,
              //       // border: Border.all(color: Color(0xFF707070)),
              //       ),


              //   child: Column(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: [
              //         InkWell(
              //           onTap: () {},
              //           child: Stack(children: [
              //             Container(
              //               width: mywidth*.18,
              //               height: myheight*.08,
              //               // color: Colors.red,
              //               child: Icon(
              //                 Icons.mail,
              //                 size: 40,
              //                 color: const Color(0xFF9599B3).withOpacity(.6),
              //               ),
              //             ),
              //             Positioned(
              //             bottom: 34,
              //             right: 0,
              //               child: Container(
              //                 width: mywidth * .078,
              //                 height: myheight * .033,
              //                 decoration: BoxDecoration(
              //                     color: const Color(0xFF7C6BD7),
              //                     // color: Colors.green,
              //                     borderRadius: BorderRadius.circular(50)),
              //                 child: Container(
              //                   padding: EdgeInsets.all(2),
              //                   child: const Text(
              //                     "7",
              //                     style: TextStyle(
              //                         fontWeight: FontWeight.bold,
              //                         fontSize: 16,
              //                         color: Colors.white),
              //                     textAlign: TextAlign.center,
              //                   ),
              //                 ),
              //               ),
              //             )
              //           ]),
              //         ),
              //         const Text(
              //           "Messages",
              //           style: TextStyle(
              //               fontSize: 16, fontWeight: FontWeight.bold),
              //         )
              //       ]),


              // ),
            ],
          ),
          // fin notification
          const SizedBox(
            height: 20,
          ),

          Container(
            margin: const EdgeInsets.only(left: 17, right: 17),
            width: mywidth * 1.06,
            height: myheight * .075,
            decoration: BoxDecoration(
                color: const Color(0xFF7C6BD7),
                borderRadius: BorderRadius.circular(24),
                border: Border.all(width: 2, color: const Color(0xFF7C6BD7))),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shadowColor: const Color(0xFF00000029),
                  backgroundColor: const Color(0xFF7C6BD7)),
              onPressed: () {
                // Navigator.pushNamed(context, '/ProgressionMariee');
                Navigator.push(
                            context,
                            PageTransition(
                              child: const ForfaitMariee(),
                              type: PageTransitionType.rightToLeft,
                              //childCurrent: widget,
                              duration: const Duration(milliseconds: 500),
                            ),
                          );
              },
              child: const Text(
                "Forfaits",
                style: TextStyle(color: Colors.white, fontSize: 25,fontWeight: FontWeight.bold),
              ),
            ),
          ),

          // fin petit trait
          const SizedBox(
            height: 25,
          ),

          Container(
            // margin: const EdgeInsets.only(left: 21, right: 21),
            width: mywidth,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    // ici l'icone message
                    Container(
                     padding: EdgeInsets.only(top: 3,bottom: 3),
                       width: mywidth * .078,
                    height: myheight * .033,
                    decoration: BoxDecoration(
                      // borderRadius: EdgeInsets.only(top: )
                      color: Color(0xFF7C6BD7),
                      borderRadius: BorderRadius.circular(30)
                    ),
                    child: Text("7",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                    ),
                    const Text(
                      "Messages",
                      style: TextStyle(
                          color: const Color(0xFF7C6BD7),
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      padding: const EdgeInsets.all(15),
                      width: mywidth * .30,
                      height: myheight * .167,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(
                              color: const Color(0xFF7C6BD7),
                              width: 2)),
                      child: InkWell(
                        onTap: () {
                          // Doit amener sur l'interface messagerie
                          // Navigator.push(
                          //   context,
                          //   PageTransition(
                          //     child: const ForfaitMariee(),
                          //     type: PageTransitionType.rightToLeft,
                          //     //childCurrent: widget,
                          //     duration: const Duration(milliseconds: 500),
                          //   ),
                          // );
                          // Navigator.pushNamed(context, '/ForfaitMariee');
                          // amene sur la page forfaits
                        },
                        child: Image.asset("assets/icones/PRESTATAIRES/email.png"),
                      ),
                    ),
                  ],
                ),
                // deuxieme bloc icone lien
                Column(
                  children: [
                    SizedBox(height: myheight * .033,),
                    const Text(
                      "Infos",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF9599B3)),
                    ),
                    Container(
                      padding: const EdgeInsets.all(15),
                      width: mywidth * .30,
                      height: myheight * .167,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(
                              color: const Color(0xFF9599B3), width: 2)),
                      child: InkWell(
                        onTap: () {
                          // Navigator.pushNamed(context, '/InfoMariee');
                          Navigator.push(
                            context,
                            PageTransition(
                              child: const InfoMariee(),
                              type: PageTransitionType.rightToLeft,
                              //childCurrent: widget,
                              duration: const Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Image.asset("assets/images/infos.png"),
                        // child: Image.asset("assets/icones/Profil/onboarding_24_information.png"),
                      ),
                    ),
                  ],
                ),

                Column(
                  children: [
                     SizedBox(height: myheight * .033,),
                    const Text(
                      "Synchro",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF52912E)),
                    ),
                    Container(
                      padding: const EdgeInsets.all(15),
                      width: mywidth * .30,
                      height: myheight * .167,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(
                              color: const Color(0xFF52912E), width: 2)),
                      child: InkWell(
                        onTap: () {
                          // Navigator.pushNamed(context, '/Synchronisation');
                          Navigator.push(
                            context,
                            PageTransition(
                              child: const Synchronisation(),
                              type: PageTransitionType.rightToLeft,
                              //childCurrent: widget,
                              duration: const Duration(milliseconds: 500),
                            ),
                          );

                        },
                        child: Image.asset("assets/images/synchro.png"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
