import 'package:flutter/material.dart';
import 'package:profile/design/design.dart';


class DrawerTroisMois extends StatelessWidget {
  final int check;

  const DrawerTroisMois({Key? key, required this.check}) : super (key:  key);


  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

     if (check == 4){
       return SizedBox(
         height: height /1.1,
         child: Drawer(
           clipBehavior: Clip.antiAlias,
           shape: const RoundedRectangleBorder(
               borderRadius: BorderRadius.only(
                   topRight: Radius.circular(90),
                   bottomRight: Radius.circular(90))),
           backgroundColor: Colors.white,
           width: width / 1.12,
           elevation: 1,

           child: Column(
             children: [
               Container(
                 color: Design.ColorMarie,
                 width: width / 1.1,
                 height: height / 3.6,
                 padding: EdgeInsets.only(left: width/12),
                 child: Column(
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.center,
                   crossAxisAlignment: CrossAxisAlignment.start,

                   children: [

                     Image.asset(
                       "assets/icones/menu_67_forfaits.png",
                       height: height / 15,
                     ),

                     const Padding(padding: EdgeInsets.only(top: 8)),

                     const Text(

                       "Forfait 12 mois",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 27,
                           color: Colors.white,
                           fontWeight: FontWeight.bold),
                     ),
                     const Text(
                       "OVM vous accompagne",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 20,
                           color: Colors.white38,
                           fontWeight: FontWeight.w200),
                     ),

                   ],
                 ),
               ),

               SizedBox(
                 // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                 height: height / 1.7,
                 child:  Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                   children: [
                     Column(
                       children: [
                         const Text("Tarif : 150 000 Fcfa",
                             style: TextStyle(
                                 color: Design.ColorMarie,
                                 fontSize: 25,
                                 fontWeight: FontWeight.w400
                             )),
                         SizedBox(
                           width: width/1.5,

                           child: Divider(),
                         ),
                       ],
                     ),


                     //Mobile money
                     Column(
                       children: [
                         const Text("Mobile money",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Design.colorBleu
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: Design.colorBleu
                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:  Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/PROFIL/product_detail_62_airtelMoney.png"),
                                   width: width/8,
                                   //height: 75,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                     //carte bancaire
                     Column(
                       children: [
                         const Text("carte bancaire",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Design.colorvert
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: Design.colorvert

                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:   Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/PROFIL/noun-credit-card-456171211.png"),
                                   width: width/5,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                   ],
                 ),
               )
             ],
           ),
         ),
       );
     }
     else if (check == 3){
       return SizedBox(
         height: height /1.1,
         child: Drawer(
           clipBehavior: Clip.antiAlias,
           shape: const RoundedRectangleBorder(
               borderRadius: BorderRadius.only(
                   topRight: Radius.circular(90),
                   bottomRight: Radius.circular(90))),
           backgroundColor: Colors.white,
           width: width / 1.12,
           elevation: 1,

           child: Column(
             children: [
               Container(
                 color: Design.colorvert,
                 width: width / 1.1,
                 height: height / 3.6,
                 padding: EdgeInsets.only(left: width/12),
                 child: Column(
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.center,
                   crossAxisAlignment: CrossAxisAlignment.start,

                   children: [

                     Image.asset(
                       "assets/icones/menu_67_forfaits.png",
                       height: height / 15,
                     ),

                     const Padding(padding: EdgeInsets.only(top: 8)),

                     const Text(

                       "Forfait 6 mois",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 27,
                           color: Colors.white,
                           fontWeight: FontWeight.bold),
                     ),
                     const Text(
                       "OVM vous accompagne",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 20,
                           color: Colors.white38,
                           fontWeight: FontWeight.w200),
                     ),

                   ],
                 ),
               ),

               SizedBox(
                 // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                 height: height / 1.7,
                 child:  Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                   children: [
                     Column(
                       children: [
                         const Text("Tarif : 100 000 Fcfa",
                             style: TextStyle(
                                 color: Design.colorvert,
                                 fontSize: 25,
                                 fontWeight: FontWeight.w400
                             )),
                         SizedBox(
                           width: width/1.5,

                           child: Divider(),
                         ),
                       ],
                     ),


                     //Mobile money
                     Column(
                       children: [
                         const Text("Mobile money",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Design.colorBleu
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: Design.colorBleu
                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:  Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/PROFIL/product_detail_62_airtelMoney.png"),
                                   width: width/8,
                                   //height: 75,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                     //carte bancaire
                     Column(
                       children: [
                         const Text("carte bancaire",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Design.colorvert
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: Design.colorvert

                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:   Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/PROFIL/noun-credit-card-456171211.png"),
                                   width: width/5,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                   ],
                 ),
               )
             ],
           ),
         ),
       );
     }
     else if (check == 2){
       return SizedBox(
         height: height /1.1,
         child: Drawer(
           clipBehavior: Clip.antiAlias,
           shape: const RoundedRectangleBorder(
               borderRadius: BorderRadius.only(
                   topRight: Radius.circular(90),
                   bottomRight: Radius.circular(90))),
           backgroundColor: Colors.white,
           width: width / 1.12,
           elevation: 1,

           child: Column(
             children: [
               Container(
                 color: Design.colorBleu,
                 width: width / 1.1,
                 height: height / 3.6,
                 padding: EdgeInsets.only(left: width/12),
                 child: Column(
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.center,
                   crossAxisAlignment: CrossAxisAlignment.start,

                   children: [

                     Image.asset(
                       "assets/icones/menu_67_forfaits.png",
                       height: height / 15,
                     ),

                     const Padding(padding: EdgeInsets.only(top: 8)),

                     const Text(

                       "Forfait 3 mois",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 27,
                           color: Colors.white,
                           fontWeight: FontWeight.bold),
                     ),
                     const Text(
                       "OVM vous accompagne",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 20,
                           color: Colors.white38,
                           fontWeight: FontWeight.w200),
                     ),

                   ],
                 ),
               ),

               SizedBox(
                 // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                 height: height / 1.7,
                 child:  Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                   children: [
                      Column(
                       children: [
                         const Text("Tarif : 50 000 Fcfa",
                             style: TextStyle(
                                 color: Design.colorBleu,
                                 fontSize: 25,
                                 fontWeight: FontWeight.w400
                             )),
                         SizedBox(
                           width: width/1.5,

                           child: Divider(),
                         ),
                       ],
                     ),


                     //Mobile money
                     Column(
                       children: [
                         const Text("Mobile money",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Design.colorBleu
                           ),),
                         InkWell(

                           onTap: () {
                             _formulaire_trois_mois(context);
                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: Design.colorBleu
                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:  Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/PROFIL/product_detail_62_airtelMoney.png"),
                                   width: width/8,
                                   //height: 75,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                     //carte bancaire
                     Column(
                       children: [
                         const Text("carte bancaire",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Design.colorvert
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: Design.colorvert

                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:   Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/PROFIL/noun-credit-card-456171211.png"),
                                   width: width/5,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                   ],
                 ),
               )
             ],
           ),
         ),
       );
     }
     else {
       return SizedBox(
         height: height /1.1,
         child: Drawer(
           clipBehavior: Clip.antiAlias,
           shape: const RoundedRectangleBorder(
               borderRadius: BorderRadius.only(
                   topRight: Radius.circular(90),
                   bottomRight: Radius.circular(90))),
           backgroundColor: Colors.white,
           width: width / 1.12,
           elevation: 1,

           child: Column(
             children: [
               Container(
                 color: Design.colorMariee,
                 width: width / 1.1,
                 height: height / 3.6,
                 padding: EdgeInsets.only(left: width/12),
                 child: Column(
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.center,
                   crossAxisAlignment: CrossAxisAlignment.start,

                   children: [

                     Image.asset(
                       "assets/icones/menu_67_forfaits.png",
                       height: height / 15,
                     ),

                     const Padding(padding: EdgeInsets.only(top: 8)),

                     const Text(

                       "Forfaits",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 27,
                           color: Colors.white,
                           fontWeight: FontWeight.bold),
                     ),
                     const Text(
                       "OVM vous accompagne",
                       style: TextStyle(
                           fontFamily: "Montserrat",
                           fontSize: 20,
                           color: Colors.white38,
                           fontWeight: FontWeight.w200),
                     ),

                   ],
                 ),
               ),

               SizedBox(
                 // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                 height: height / 1.7,
                 child:  Column(
                   crossAxisAlignment: CrossAxisAlignment.center,
                   mainAxisSize: MainAxisSize.max,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                   children: [

                     //Messages
                     Column(
                       children: [
                         const Text("Messages",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Color(0xFFB07DD1)
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: const Color(0xFFB07DD1)
                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:  const Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/AGENDA/profile_97__1_mois.png"),
                                   width: 70,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                     //Notifications
                     Column(
                       children: [
                         const Text("Notifications",
                           style: TextStyle(
                               fontSize: 20,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.bold,
                               color: Color(0xFF7C6BD7)
                           ),),
                         InkWell(

                           onTap: () {

                           },

                           child: Container(
                             width: width/4,
                             height: height/6,
                             margin: const EdgeInsets.symmetric(vertical: 5.0),
                             padding: const EdgeInsets.symmetric(vertical: 25.0),
                             decoration: BoxDecoration(
                               //color: Colors.black12,
                                 borderRadius: BorderRadius.circular(35),
                                 border: Border.all(
                                     width: 2,
                                     color: const Color(0xFF7C6BD7)

                                 )
                             ),
                             // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                             child:  const Column(
                               children: [
                                 Image(
                                   image: AssetImage("assets/icones/AGENDA/profile_97__1_mois.png"),
                                   width: 70,
                                 ),

                               ],
                             ),
                           ),

                         ),
                       ],
                     ),

                   ],
                 ),
               )
             ],
           ),
         ),
       );
     }
  }


  Future<void> _formulaire_trois_mois(BuildContext context) {

    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          icon: new Image.asset("assets/icones/PROFIL/product_detail_62_airtelMoney.png",
            height: 100,
          ),
          // icon: AssetImage("assets/icones/PROFIL/product_detail_62_airtelMoney.png"),
          title: const Text("Mobile money",
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  color: Design.colorBleu
              ),
              textAlign: TextAlign.center),
          content: Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            //height: 150,
            child: const Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [


                //AppWidgets.inputText(labelText: "Desciption", controller: appartDescriptionController),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                  style: TextButton.styleFrom(
                    textStyle: Theme.of(context).textTheme.labelLarge,
                  ),
                  child: const Text('Retour'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Enregistrer'),
                    onPressed: ()  {

                    }),
              ],
            ),
          ],
        );
      },
    );
  }
}