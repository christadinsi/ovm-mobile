import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:profile/components/widgets-boutons.dart';
import 'package:profile/components/widgets-title.dart';
import 'package:profile/design/design.dart';
import 'package:profile/routes/routes.dart';
import 'package:profile/src/views/profile_mariee/forfait/drawer_forfait.dart';


// class ForfaitMariee extends StatelessWidget {
//  const  ForfaitMariee({super.key});


//   @override
//   Widget build(BuildContext context) {
//   final router = AppRouterProfile();

//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       onGenerateRoute: router.onGenerateRoute,
//       home: const  ForfaitMarieeView(),
//     );
//   }
// }



class ForfaitMariee extends StatefulWidget {
  const ForfaitMariee({super.key});

  @override
  State<ForfaitMariee> createState() => _ForfaitMarieeState();
}

class _ForfaitMarieeState extends State<ForfaitMariee> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;


  void openDrawerWithSelectValue(int selectedValue){

    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    final Uint8List? signtech;

    return  Scaffold(
      key: _key,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,

        actions: <Widget>[

          //IconButton
         IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            //color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              _key.currentState?.openDrawer();

            },
          ), //IconButton
        ],
        title: const Text(
          'Profil',
          style: TextStyle(fontSize: 20,
              color: Colors.grey
          ),
        ),

      ),

      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
         // mainAxisSize: MainAxisSize.max,
          //mainAxisAlignment: MainAxisAlignment.spaceAround,

          children: [

            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(vertical: 30),
              child: AppWidgetsTitles.titre(
                  texte: "Forfaits OVM",
                  colorText: Design.colorMariee,
                  size: 30),
            ),

            AppWidgetsTitles.bar(),

          InkWell(
            child: Container(
              padding: const EdgeInsets.only(top: 10.0, bottom: 30.0),
              child:  Column(
                children: [
                  Image(
                    image: const AssetImage("assets/icones/menu_67_forfaits.png"),
                    width: width/2.5,
                  ),
                  const Text("Mon forfait",
                    style: TextStyle(
                        color: Colors.grey,
                      fontWeight: FontWeight.w600,

                      fontSize: 20,
                      fontFamily: 'Poppins'),
                  )
                ],
              ),
            ),
            onTap: () {
              print("Tapped on container");
            },
          ),

            const Text("Sélectionner votre forfait",
                style : TextStyle(
                    fontSize: 25,
                    fontFamily: "Poppins",
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(53, 38, 65, 0.8),
                )),

           Padding(padding:  EdgeInsets.symmetric(vertical: height/15),

           child:   Row(
             mainAxisSize: MainAxisSize.max,
             mainAxisAlignment: MainAxisAlignment.spaceBetween,

             children: [

               //bouton 3 mois
               Column(
                 children: [
                   const Text("3 mois",
                     style: TextStyle(
                         fontSize: 20,
                         fontFamily: 'Poppins',
                         fontWeight: FontWeight.bold,
                         color: Design.colorBleu
                     ),),
                   InkWell(
                     onTap: () {

                       openDrawerWithSelectValue(2);

                     },
                     child: Container(
                       width: width/4,
                       height: height/6,
                       margin: const EdgeInsets.symmetric(vertical: 5.0),
                       padding: const EdgeInsets.symmetric(vertical: 25.0),
                       decoration: BoxDecoration(
                         //color: Colors.black12,
                           borderRadius: BorderRadius.circular(35),
                           border: Border.all(
                               width: 2,
                               color: Design.colorBleu
                           )
                       ),
                       // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                       child:   Column(
                         mainAxisSize: MainAxisSize.max,
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Image(
                             image: AssetImage("assets/icones/PROFIL/noun-gift-4291451.png"),
                             width: width/6,
                           ),

                         ],
                       ),
                     ),

                   ),
                 ],

               ),

               //bouton 6 mois
               Column(
                 children: [
                   const Text("6 mois",
                     style: TextStyle(
                         fontSize: 20,
                         fontFamily: 'Poppins',
                         fontWeight: FontWeight.bold,
                         color: Design.colorvert
                     ),),
                   InkWell(
                     child: Container(
                       width: width/4,
                       height: height/6,

                       margin: const EdgeInsets.symmetric(vertical: 5.0),
                       padding: const EdgeInsets.symmetric(vertical: 25.0),
                       decoration: BoxDecoration(
                         //color: Colors.black12,
                           borderRadius: BorderRadius.circular(35),
                           border: Border.all(
                               width: 2,
                               color: Design.colorvert
                           )
                       ),
                       // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                       child:   Column(
                         mainAxisSize: MainAxisSize.max,
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Image(
                             image: AssetImage("assets/icones/PROFIL/noun-gift-4291451.png"),
                             width: width/6,
                           ),

                         ],
                       ),
                     ),

                     onTap: () {
                       openDrawerWithSelectValue(3);
                     },
                   ),
                 ],

               ),

               //bouton 12 mois
               Column(
                 children: [
                   const Text("12 mois",
                     style: TextStyle(
                         fontSize: 20,
                         fontFamily: 'Poppins',
                         fontWeight: FontWeight.bold,
                         color: Design.ColorMarie
                     ),),
                   InkWell(
                     child: Container(
                       width: width/4,
                       height: height/6,

                       margin: const EdgeInsets.symmetric(vertical: 5.0),
                       padding: const EdgeInsets.symmetric(vertical: 25.0),
                       decoration: BoxDecoration(
                         //color: Colors.black12,
                           borderRadius: BorderRadius.circular(35),
                           border: Border.all(
                               width: 2,
                               color: Design.ColorMarie
                           )
                       ),
                       // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                       child:   Column(
                         mainAxisSize: MainAxisSize.max,
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [
                           Image(
                             image: AssetImage("assets/icones/PROFIL/noun-gift-4291451.png"),
                              width: width/6,
                           ),

                         ],
                       ),
                     ),
                     onTap: () {
                       openDrawerWithSelectValue(4);
                     },
                   ),
                 ],


               ),

             ],
           ),
           ),

            AppWidgetsBoutons.boutonRetour( colorbouton: Design.colorMariee, height: height/16, width:  width/1.6, onPressed: () {  

                 Navigator.pushNamed(context, '/AccueilMariee');
    
            }),

          ],
        ),
      ),
      drawer: DrawerTroisMois(check: drawPos),
    );

  }


  Future<void> _name_Description(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          title: const Text(" Modifier les information de l'appartement",
              textAlign: TextAlign.center),
          content: Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            //height: 150,
            child: const Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                /*Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 30),

                  child: AppWidgets.input( labelText: "Nom appartement", autofocus: true, controller: appartNameController, keyboardType: TextInputType.name),
                ),*/

                //AppWidgets.inputText(labelText: "Desciption", controller: appartDescriptionController),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                  style: TextButton.styleFrom(
                    textStyle: Theme.of(context).textTheme.labelLarge,
                  ),
                  child: const Text('Retour'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Enregistrer'),
                    onPressed: ()  {
                          
                    }),
              ],
            ),
          ],
        );
      },
    );
  }

}
