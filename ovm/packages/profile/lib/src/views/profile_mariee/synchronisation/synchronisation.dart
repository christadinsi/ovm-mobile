import 'package:flutter/material.dart';
import 'package:profile/components/widgets-title.dart';
import 'package:profile/design/design.dart';

class Synchronisation extends StatelessWidget {
  const Synchronisation({super.key});

  @override
  Widget build(BuildContext context) {
    // var size = MediaQuery.of(context).size;
    // final double _height = MediaQuery.of(context).size.height;
    // final double _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        toolbarHeight: 84,
        automaticallyImplyLeading: false,
        title: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  onPressed: () {
                  Navigator.pushNamed(context, '/ProfileMariee');

                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Design.colorModuleGrey,
                    size: 24,
                  ),
                ),
                const SizedBox(
                  width: 35,
                ),
                const Text(
                  "Synchronisation",
                  style: TextStyle(fontSize: 20, color: Design.colorModuleGrey),
                ),
                const SizedBox(
                  width: 30,
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.dehaze_sharp,
                    color: Design.colorModuleGrey,
                    size: 24,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            Row(
              children: [AppWidgetsTitles.bar()],
            ),
            const Padding(
              padding: EdgeInsets.only(top: 4.0),
              child: Row(
                children: [
                  Text(
                    "ELement synchronisés",
                    style: TextStyle(color: Design.colorvert),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 30.0),
          child: Column(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 250.0, top: 20),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          width: 80,
                          height: 82,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(360.0),
                            border:
                                Border.all(width: 2, color: Design.ColorMarie),
                          ),
                          child: Image.asset(
                            'assets/icones/BUDGET/profile_19_budget.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 150.0, top: 10),
                    child: Text(
                      "Nouvelle cotisation :",
                      style: TextStyle(fontSize: 17),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 78.0),
                    child: Text(
                      "JEAN BADINGA 100 000 CFA",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 240.0),
                    child: Text(
                      "(famille)",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 250.0, top: 10),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          width: 80,
                          height: 82,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(360.0),
                            border:
                                Border.all(width: 2, color: Design.ColorMarie),
                          ),
                          child: Image.asset(
                            'assets/icones/BUDGET/onboardind_201_facture.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 140.0, top: 10),
                    child: Padding(
                      padding: EdgeInsets.only(right: 90.0),
                      child: Text(
                        "FActure :",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 200.0),
                    child: Text(
                      "Salle de fete",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 130.0),
                    child: Text(
                      "(avance 100 000 FCA)",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 250.0, top: 10),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          width: 80,
                          height: 82,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(360.0),
                            border:
                                Border.all(width: 2, color: Design.ColorMarie),
                          ),
                          child: Image.asset(
                            'assets/icones/COMITE/onboarding_201_reunion.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 210.0),
                    child: Text(
                      "Reunions :",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 63.0),
                    child: Text(
                      "historique reunion 01/23/23",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 250.0, top: 10),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          width: 80,
                          height: 82,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(360.0),
                            border:
                                Border.all(width: 2, color: Design.ColorMarie),
                          ),
                          child: Image.asset(
                            'assets/icones/PRESTATAIRES/noun-heart-100.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 180.0),
                    child: Text(
                      "Prestataires :",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 70.0),
                    child: Text(
                      "Prestataires selectionnés :",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 250.0, top: 10),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          width: 80,
                          height: 82,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(360.0),
                            border:
                                Border.all(width: 2, color: Design.ColorMarie),
                          ),
                          child: Image.asset(
                            'assets/icones/BUFFET/profile_31_buffet.png',
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 230.0),
                    child: Text(
                      "Buffet :",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 50.0),
                    child: Text(
                      "Traiteurs , plats , boissons :",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  //  const Padding(
                  //   padding: EdgeInsets.only(right: 300.0),
                  //   child: Text(
                  //     "Buffet :",
                  //     style: TextStyle(fontSize: 18),
                  //   ),

                  // ),
                  //   const Padding(
                  //   padding: EdgeInsets.only(right: 100.0),
                  //   child: Text(
                  //     "Traiteur, Plat , Boissons :",
                  //     style: TextStyle(fontSize: 18),
                  //   ),
                  // ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
