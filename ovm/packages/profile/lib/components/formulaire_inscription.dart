import 'package:flutter/material.dart';

class FormulaireInscription extends StatelessWidget {
  const FormulaireInscription({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top: 70.0, left: 15),
      child: FormulaireContainer(),
    );
  }
}

class FormulaireContainer extends StatelessWidget {
  const FormulaireContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 640,
      width: 329,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        color: Colors.white,
      ),
      child: const FormulaireContent(),
    );
  }
}

class FormulaireContent extends StatelessWidget {
  const FormulaireContent({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // FormulaireHeader(),
        SizedBox(height: 5),
        FormulaireTextField(labelText: "Code mariage : ************"),
        SizedBox(height: 5),
        FormulaireTextField(labelText: "Nom :"),
        SizedBox(height: 5),
        FormulaireTextField(labelText: "Prenoms :"),
        SizedBox(height: 4),
        FormulaireTextField(labelText: "Tels :"),
        SizedBox(height: 5),
        FormulaireTextField(labelText: "Mot de passe :"),
        SizedBox(height: 5),
        FormulaireTextField(labelText: "Mail :"),
        SizedBox(height: 5),
        FormulaireTextField(labelText: "Future epoux :"),
        // SizedBox(height: 10),
        // FormulaireButton(),
      ],
    );
  }
}

// class FormulaireHeader extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 8.0, left: 100),
//       child: Text(
//         "Inscription",
//         style: TextStyle(
//           fontSize: 28,
//           color: Design.ColorMarie,
//           fontWeight: FontWeight.bold,
//         ),
//       ),
//     );
//   }
// }

class FormulaireTextField extends StatelessWidget {
  final String labelText;

  const FormulaireTextField({super.key, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: TextFormField(
        decoration: InputDecoration(
          border: const UnderlineInputBorder(),
          labelText: labelText,
        ),
      ),
    );
  }
}

// class FormulaireButton extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 8.0, left: 70),
//       child: ButtonCreer(
//         text: "Inscription",
//         onPressed: () {},
//         color: Design.ColorMarie,
//       ),
//     );
//   }
// }
