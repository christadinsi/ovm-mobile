 import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

tables({
    required String text,
    required int nombreDePersonnes, required String nom,
  }) {
    return Column(
      children: [
        Row(
          children: [
        const SizedBox(height: 30,),

            Text(
              text,
              style: const TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.bold,
                color: Design.colorBlack,
              ),
            ),
          ],
        ),
        const Column(
          children: [
         Row(
              children: [
                Text(
                  '1. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w100,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'Ndong  Pierre',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w100,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
               
              ],
            ),
            Row(
              children: [
                Text(
                  '2. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w100,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'Ndong  Pierre',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w100,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
               
              ],
            ),
            Row(
              children: [
                Text(
                  '3. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w100,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'Ndong  Pierre',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w100,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
               
              ],
            ),
          ],
        )

      ],
    );
  }