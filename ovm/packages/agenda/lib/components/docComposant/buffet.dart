import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class BuffetDoc extends StatelessWidget {
  const BuffetDoc({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children:  [
        SizedBox(height: 40,),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //premiere partie du buffet
            Text("Les entrées",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Design.colorvert,
            ),
            ),
           
          ],
        ),
         SizedBox(height: 20,),
              Row(
              children: [
                Text(
                  '1. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Salade d\'avocat',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
            ),
              Row(
              children: [
                Text(
                  '2. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Fruit de mer',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
              ),
              Row(
              children: [
                Text(
                  '3. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Salade de lentille',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
              ),
              SizedBox(height: 30,),
              
              // second partie du buffet
               Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Les plats",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Design.colorBlueCiel,
            ),
            ),
           
          ],
          
        ),
          SizedBox(height: 20,),
              Row(
              children: [
                Text(
                  '1. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Salade d\'avocat',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
            ),
              Row(
              children: [
                Text(
                  '2. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Fruit de mer',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
              ),
              Row(
              children: [
                Text(
                  '3. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Salade de lentille',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
              ),
              SizedBox(height: 30,),
              //troisieme partie du buffet
               Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Le desert",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Design.colorMariee,
            ),
            ),
           
          ],
        ),
          SizedBox(height: 20,),
              Row(
              children: [
                Text(
                  '1. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'tarte à la pomme',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
            ),
              Row(
              children: [
                Text(
                  '2. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  ' Fruit',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              
              ],
              ),
      ],
    );
  }
}