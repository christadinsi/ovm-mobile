import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class ListMembreCotisation extends StatelessWidget {
  const ListMembreCotisation({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Row(
          children: [
            Text(
              'Nbre -',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
                color: Design.colorBlack,
              ),
            ),
            // SizedBox(width: 1,)
            Text(
              ' 80',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal,
                color: Design.colorBlack,
              ),
            ),
            SizedBox(
              width: 1,
            ),
            Text(
              ' personnes',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal,
                color: Design.colorBlack,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Column(
          children: [
            Row(
              children: [
                Text(
                  '1. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'Ndong  Pierre',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
                 Text(
                  ' 300 000  000FCFA',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  '2. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'Mbina Augustine',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
              Text(
                  ' 300 000 FCFA',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  '3. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'pambo Loic ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
                Text(
                  ' 400 000 FCFA',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  '4. ',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                // SizedBox(width: 1,)
                Text(
                  'Boussa Ghislaine',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
                SizedBox(
                  width: 1,
                ),
                Text(
                  ' 300 000 FCFA',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Design.colorBlack,
                  ),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }
}
