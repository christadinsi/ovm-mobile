import 'package:flutter/material.dart';

class PeriodeSheet {
  String nbrMois;
  Color colorBorderSheet;
  Color colorTitle;

  PeriodeSheet(
      {required this.nbrMois,
      required this.colorBorderSheet,
      required this.colorTitle});
  btnPeriodeSheet(
    BuildContext context,
  ) {
    double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    double fontSizeText1 = screenWidth * 0.065;
    double fontSizeText2 = screenWidth * 0.045;
    return showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: screenHeight * .7,
            decoration: BoxDecoration(
                color: const Color.fromARGB(255, 255, 255, 255),
                border: Border.all(width: 2, color: colorBorderSheet),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text(
                    "$nbrMois mois",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: fontSizeText1,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    "Listes des tâches à réaliser",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: colorTitle,
                      fontSize: fontSizeText2,
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}

  //  PeriodeSheet(
  //                                   colorBorderSheet: Colors.green,
  //                                   colorTitle: Colors.green,
  //                                   nbrMois: '1er',
  //                                 ).btnPeriodeSheet(context);