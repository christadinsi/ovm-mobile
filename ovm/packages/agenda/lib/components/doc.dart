import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

docComposant({
  required String titleGlobalDoc,
  required String textThemeCouleur,
  required String textNomComite,
  required String membreCommite,
  required String textTel,
  required String textLieu,
  required String textTel2,
  required String salleFete,
  required String nomMariee,
  required String nomMarie,
  required String grandTitreRight,
  required String texteDroite,
  required Color colorText,
  required Color colorGrandTitre,
  required Color colorBorder,
  required Color colorButtonTelechargement,
  required Color borderSideRight,
  required double width,
  required double height,
  required void Function()? onPressed,
  required Widget composantRightBottom,

}) {
  return  Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: AppWidgetsTitles.titreDocumentaTelecharger(
                title: titleGlobalDoc,
                color: colorGrandTitre,
                size: 35,
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 2, color:colorBorder),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // Column de gauche
                    Expanded(
                      flex: 1,
                      child: Container(
                          height: double.infinity,
                          width: double.infinity,
                          // margin: const EdgeInsets.symmetric(vertical: 5.0),
                          decoration:  BoxDecoration(
                            color:  Design.colorgreyfonce,
                            border: Border(
                              right: BorderSide(
                                color: borderSideRight,
                                width: 1.0,
                              ),
                            ),
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Image(
                                image: AssetImage(
                                    "assets/icones/AGENDA/logosansgbackground.png"),
                                // width: _width / 6,
                              ),
                              const SizedBox(
                                height: 25,
                              ),
                              Container(
                                  child:  Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Text(
                                      "Info mariage ",
                                      style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                    const Divider(
                                      color: Design.colorBlack,
                                    ),
                                    const Text(
                                      "Theme couleur:",
                                      style: TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                    Text(
                                      textThemeCouleur,
                                      style: const TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.normal,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    const Text(
                                      "Commité mariage:",
                                      style: TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                     Text(
                                      membreCommite,
                                      style: const TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.normal,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                     Text(
                                      textNomComite,
                                      style: const TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                     Row(
                                      children: [
                                      const  Text(
                                          "Tel:",
                                          style: TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.normal,
                                            color: Design.colorBlack,
                                          ),
                                        ),
                                      const   SizedBox(
                                          width: 2,
                                        ),
                                        Text(textTel,
                                            style: const TextStyle(
                                              fontSize: 8,
                                              fontWeight: FontWeight.normal,
                                              color: Design.colorBlack,
                                            )),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    const Text(
                                      "Cérémonie",
                                      style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                    const Divider(
                                      color: Design.colorBlack,
                                    ),
                                     Row(
                                      children: [
                                       const  Text(
                                          "Lieu:",
                                          style: TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.normal,
                                            color: Design.colorBlack,
                                          ),
                                        ),
                                      const   SizedBox(
                                          width: 2,
                                        ),
                                        Text(textLieu,
                                            style:const  TextStyle(
                                              fontSize: 8,
                                              fontWeight: FontWeight.normal,
                                              color: Design.colorBlack,
                                            )),
                                      ],
                                    ),
                                     Row(
                                      children: [
                                      const   Text(
                                          "Salle des fetes:",
                                          style: TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.normal,
                                            color: Design.colorBlack,
                                          ),
                                        ),
                                       const  SizedBox(
                                          width: 2,
                                        ),
                                        Text(salleFete,
                                            style: const TextStyle(
                                              fontSize: 8,
                                              fontWeight: FontWeight.normal,
                                              color: Design.colorBlack,
                                            )),
                                      ],
                                    ),
                                      Row(
                                      children: [
                                      const Text(
                                          "Tel:",
                                          style: TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.normal,
                                            color: Design.colorBlack,
                                          ),
                                        ),
                                       const  SizedBox(
                                          width: 2,
                                        ),
                                        Text(textTel2,
                                            style: const TextStyle(
                                              fontSize: 8,
                                              fontWeight: FontWeight.normal,
                                              color: Design.colorBlack,
                                            )),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    const Text(
                                      "Future Mariés",
                                      style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorBlack,
                                      ),
                                    ),
                                     const Divider(
                                      color: Design.colorBlack,
                                    ),
                                     Row(
                                      children: [
                                      const  Icon(
                                          Icons.circle,
                                          color: Design.colorBlack,
                                          size: 5,
                                        ),
                                       const SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          nomMariee,
                                          style: const TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.normal,
                                            color: Design.colorBlack,
                                          ),
                                        ),
                                       const SizedBox(
                                          width: 2,
                                        ),
                                        Text(nomMarie,
                                            style: const TextStyle(
                                              fontSize: 8,
                                              fontWeight: FontWeight.normal,
                                              color: Design.colorBlack,
                                            )),
                                      ],
                                    ),
                                  ],
                                ),
                              ))
                            ],
                          )),
                    ),
                    // Column de droite
                    Expanded(
                      flex: 2,
                      child: Container(
                        height: double.infinity,
                        width: double.infinity,
                        child:  Padding(
                          padding: const EdgeInsets.only(left:20),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                             mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: AppWidgetsTitles.TitreDocumentRight(
                                  title: grandTitreRight,
                                  title2: texteDroite,
                                  color: Design.colorBlack,
                                  size: 16,
                                  size2: 7,
                                ),
                              ),
                              
                               const Divider(
                                      color: Design.colorBlack,
                                    ),
                                     Column(
                                children: [
                                 composantRightBottom,
                                ],
                               ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: height / 18,
            ),
            AppWidgetsBoutons.telecharger(
                text: 'Télécharger', onPressed: onPressed, color:colorButtonTelechargement),
          ],
        ),
      );
}
