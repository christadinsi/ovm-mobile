import 'package:flutter/material.dart';
import 'package:agenda/design/design.dart';

class AppWidgetsBoutons {
  static Center boutonRetour({
    required Color colorbouton,
    required double width,
    required double height,
    required void Function()? onPressed,
  }) {
    return Center(
      child: SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(colorbouton)),
          onPressed: onPressed,
          child: const Icon(
            Icons.home_filled,
            color: Colors.white,
            size: 40,
          ),
        ),
      ),
    );
  }

  static SizedBox ButtonCreer({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60, 
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child: Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }

  static ButtonInscription({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child: Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }
 

  static buttonValiderTaches({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child:  Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }

    static telecharger({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child:  Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 22),
        ),
      ),
    );
  }

static buttonLink({
  required String title,
  required Color colorText,
  required Color colorBorder,
  required double width,
  required double height,
  required void Function()? onTap,
  required String image,
  required double imageWidth,
  required double imageheight,
}) {
  return Column(
    children: [
      Text(
      title,
        style: TextStyle(
            fontSize: 18,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.bold,
            color: colorText),
      ),
      InkWell(
        onTap: onTap,
        child: Container(
          width: width,
          height: height,

          margin: const EdgeInsets.symmetric(vertical: 5.0),
          padding: const EdgeInsets.symmetric(vertical: 25.0),
          decoration: BoxDecoration(
              //color: Colors.black12,
              borderRadius: BorderRadius.circular(35),
              border: Border.all(width: 2, color: colorBorder)),
          // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image:  AssetImage(image),
                width: imageWidth ,
                height: imageheight,
              ),
            ],
          ),
        ),
      ),
    ],
  );
}



  // static IconButtonChef(
  //     {
  //       required Image image,
  //     // required String text,
  //     required Function onTap,
  //     required Color color}) {
  //   return InkWell(
  //     onTap: (){  },
  //     child: Container(
  //       padding: const EdgeInsets.all(8),
  //       width: 97,
  //       height: 126,
  //       decoration: BoxDecoration(
  //       color: color,
  //         borderRadius: BorderRadius.circular(40)
  //       ),
  //       child: Column(
  //         children: [
  //          Image.asset('$image'),

  //         ],
  //       ),
  //     ),
  //   );
  // }
}
