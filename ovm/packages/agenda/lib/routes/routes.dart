// ignore_for_file: implementation_imports
import 'package:agenda/agenda.dart';
import 'package:flutter/material.dart';


class AppRouterPrestataires {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/BudgetMariee':
        return MaterialPageRoute(
            builder: (context) => const AgendaAccueil());
      //  case '/ProgressionMariee':
      //   return MaterialPageRoute(
      //       builder: (context) => const ProgressionMariee());

      default:
        return null;
    }
  }
}
