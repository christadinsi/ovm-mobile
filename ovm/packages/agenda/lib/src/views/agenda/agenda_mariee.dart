import 'package:agenda/src/views/agenda/documents/accueil_documents.dart';
import 'package:agenda/src/views/agenda/progression/button_progression.dart';
import 'package:agenda/src/views/agenda/taches/taches_menu.dart';
import 'package:flutter/material.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/design/design.dart';
import 'package:page_transition/page_transition.dart';

class AgendaAccueil extends StatefulWidget {
  const AgendaAccueil({super.key});

  @override
  State<AgendaAccueil> createState() => _AgendaAccueilState();
}

class _AgendaAccueilState extends State<AgendaAccueil> {
  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Design.colorBleu,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                // height: _height * .49,
                // height: _height * .55,
                height: _height /1.8,
                child: Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(top: 50),
                      height: _height / 1.8,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/marieProfil21.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: AppWidgetsTitles.titre(
                            texte: "Agenda",
                            colorText: (Design.colorBleu),
                            size: 35),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only( top: 25),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: [0.15, 0.40],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          //tileMode: TileMode.mirror,
                          colors: <Color>[
                            Colors.white70,
                            Colors.white.withOpacity(0.0)
                          ],
                        ),
                      ),
                      child: const Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          "Organisez tous les détails et votre cérémonie sera magnifique",
                          style: TextStyle(fontSize: 20),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    // bouton progression
                  ],
                ),
              ),

              // btn progression
              // Container(
              //   margin: const EdgeInsets.only(left: 17, right: 17),
              //   width: _width * 1.06,
              //   height: _height * .075,
              //   decoration: BoxDecoration(
              //       color: const Color(0xFF4666E5),
              //       borderRadius: BorderRadius.circular(24),
              //       border:
              //           Border.all(width: 2, color: const Color(0xFF4666E5))),
              //   child: ElevatedButton(
              //     style: ElevatedButton.styleFrom(
              //         shadowColor: const Color(0xFF00000029),
              //         backgroundColor: const Color(0xFF4666E5)),
              //     onPressed: () {
              //       Navigator.pushNamed(context, '/ProgressionMariee');
              //       // Navigator.push(
              //       //             context,
              //       //             PageTransition(
              //       //               child: const ProgressionMariee(),
              //       //               type: PageTransitionType.rightToLeft,
              //       //               //childCurrent: widget,
              //       //               duration: const Duration(milliseconds: 500),
              //       //             ),
              //       //           );
              //     },
              //     child: const Text(
              //       "Progression",
              //       style: TextStyle(
              //           color: Colors.white,
              //           fontSize: 25,
              //           fontWeight: FontWeight.bold),
              //     ),
              //   ),
              // ),
              const SizedBox(
                height: 25,
              ),
              Container(
                  height: _height / 2.5,
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //Pérodes
                          Column(
                            children: [
                              const Text(
                                "Progression",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorBleu),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.colorBleu)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/AGENDA/menu_34_periode.png"),
                                        width: _width / 6,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const ProgressionMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),

                          //Taches
                          Column(
                            children: [
                              const Text(
                                "Thèmes",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorMariee),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.colorMariee)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/AGENDA/noun-list-6058511.png"),
                                        width: _width / 5.5,
                                        height: _width / 5.5,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const TachesMenu(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),

                          //Docs
                          Column(
                            children: [
                              const Text(
                                "Docs",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorModuleGrey),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const AccueilDocument(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2,
                                          color: Design.colorModuleGrey)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/AGENDA/noun-documents.png"),
                                        width: _width / 6,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      AppWidgetsBoutons.boutonRetour(
                          colorbouton: Design.colorBleu,
                          height: _height / 16,
                          width: _width / 1.6,
                          onPressed: () {
                            Navigator.pushNamed(context, '/AccueilMariee');
                          }),
                    ],
                  ))
            ],
          )
        ],
      ),
    );
  }
}
