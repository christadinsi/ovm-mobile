import 'package:agenda/components/doc.dart';
import 'package:agenda/components/docComposant/list_invite.dart';
import 'package:agenda/components/docComposant/list_membre_cotisation.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class Invite extends StatelessWidget {
  const Invite({super.key});

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorvert),
        centerTitle: true,
        toolbarHeight: 60,
        title: const Text(
          'Document',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Design.colorModuleGrey,
          ),
        ),
      ),
      body:
      docComposant(
        width: _width,
        height: _height,
        titleGlobalDoc: "INVITES", 
        grandTitreRight: "Liste des Invites",
        texteDroite: "(La mariée)",
        colorText: Design.colorBleu,
        colorGrandTitre: Design.colorBleu, 
        colorBorder: Design.colorBleu,
        textThemeCouleur: "Blue et jaune ",
        membreCommite: "Président",
        textNomComite: "Mr OGOUA Pierre",
        textTel: "066 45 88 87",
        textLieu: "Quartier Glass", 
        salleFete: "KARE F",
        textTel2: "077 45 87 00",
        nomMariee: "Anne",
        nomMarie: "Pierre",
        borderSideRight: Design.colorModuleGrey,
        colorButtonTelechargement: Design.colorBleu,
        onPressed: () {},
        composantRightBottom: const ListInvite(),

      ),
    );
  }
}