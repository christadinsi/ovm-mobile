import 'package:agenda/components/doc.dart';
import 'package:agenda/components/docComposant/table.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class Tables extends StatelessWidget {
  const Tables({super.key});

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorvert),
        centerTitle: true,
        toolbarHeight: 60,
        title: const Text(
          'Document',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Design.colorModuleGrey,
          ),
        ),
      ),
      body:
      docComposant(
        width: _width,
        height: _height,
        titleGlobalDoc: "TAbles", 
        grandTitreRight: "Plan de table",
        texteDroite: "",
        colorText: Design.colorMariee,
        colorGrandTitre: Design.colorMariee, 
        colorBorder: Design.colorMariee,
        textThemeCouleur: "Blue et jaune",
        membreCommite: "Président ",
        textNomComite: "Mr OGOUA Pierre",
        textTel: "066 45 88 87",
        textLieu: "Quartier Glass", 
        salleFete: "KARE F",
        textTel2: "077 45 87 00",
        nomMariee: "Anne",
        nomMarie: "Pierre",
        borderSideRight: Design.colorModuleGrey,
        colorButtonTelechargement: Design.colorMariee,
        onPressed: () {},
        composantRightBottom: Column(
          children: [
             const SizedBox(height: 0,),
            Row(
              children: [
                tables(
                  text: "Table Oyem", 
                  nombreDePersonnes: 5, 
                  nom: "Mr OGOUA Pierre",
                ),
                const SizedBox(width: 10,),
                 tables(
                  text: "Table Libreville", 
                  nombreDePersonnes: 5, 
                  nom: "Mr OGOUA Pierre",
                ),
              ],
            ),
            const SizedBox(height: 40,),
            Row(
              children: [
                  tables(
                  text: "Table  Bitam", 
                  nombreDePersonnes: 5, 
                  nom: "Mr OGOUA Pierre",
                ),
                const SizedBox(width: 10,),
                 tables(
                  text: "Table Mouila", 
                  nombreDePersonnes: 5, 
                  nom: "Mr OGOUA Pierre",
                ),
              ],
            ),
          ],
        ),



      ),
    );
  }
}