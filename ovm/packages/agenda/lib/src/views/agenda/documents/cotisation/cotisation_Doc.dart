import 'package:agenda/components/doc.dart';
import 'package:agenda/components/docComposant/list_membre_cotisation.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class Cotisation extends StatelessWidget {
  const Cotisation({super.key});

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorvert),
        centerTitle: true,
        toolbarHeight: 60,
        title: const Text(
          'Document',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Design.colorModuleGrey,
          ),
        ),
      ),
      body:
      docComposant(
        width: _width,
        height: _height,
        titleGlobalDoc: "Cotisations", 
        grandTitreRight: "Cotisation",
        texteDroite: "(Total:1.800.000 FCFA)",
        colorBorder: Design.colorvert,
        colorText: Design.colorvert, 
        colorGrandTitre: Design.colorvert,
        textThemeCouleur: "Blue et jaune",
        membreCommite: "Président ",
        textNomComite: "Mr OGOUA Pierre",
        textTel: "066 45 88 87",
        textLieu: "Quartier Glass", 
        salleFete: "KARE F",
        textTel2: "077 45 87 00",
        nomMariee: "Anne",
        nomMarie: "Pierre",
        borderSideRight: Design.colorModuleGrey,
        colorButtonTelechargement: Design.colorvert,
        onPressed: () {},
        composantRightBottom: const ListMembreCotisation(),
      ),
      //  Container(
      //   padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      //   child: Column(
      //     mainAxisSize: MainAxisSize.max,
      //     crossAxisAlignment: CrossAxisAlignment.center,
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       Align(
      //         alignment: Alignment.centerLeft,
      //         child: AppWidgetsTitles.titreDocumentaTelecharger(
      //           title: "Cotisations",
      //           color: Design.colorvert,
      //           size: 35,
      //         ),
      //       ),
      //       Expanded(
      //         child: Container(
      //           decoration: BoxDecoration(
      //             color: Colors.white,
      //             border: Border.all(width: 2, color: Design.colorvert),
      //           ),
      //           child: Row(
      //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //             children: [
      //               // Column de gauche
      //               Expanded(
      //                 flex: 1,
      //                 child: Container(
      //                     height: double.infinity,
      //                     width: double.infinity,
      //                     // margin: const EdgeInsets.symmetric(vertical: 5.0),
      //                     decoration: const BoxDecoration(
      //                       color: Design.colorgreyfonce,
      //                       border: Border(
      //                         right: BorderSide(
      //                           color: Design.colorModuleGrey,
      //                           width: 1.0,
      //                         ),
      //                       ),
      //                     ),
      //                     child: Column(
      //                       mainAxisSize: MainAxisSize.max,
      //                       crossAxisAlignment: CrossAxisAlignment.start,
      //                       children: [
      //                         const Image(
      //                           image: AssetImage(
      //                               "assets/icones/AGENDA/logosansgbackground.png"),
      //                           // width: _width / 6,
      //                         ),
      //                         const SizedBox(
      //                           height: 25,
      //                         ),
      //                         Container(
      //                             child: const Padding(
      //                           padding: EdgeInsets.all(8.0),
      //                           child: Column(
      //                             mainAxisAlignment: MainAxisAlignment.center,
      //                             crossAxisAlignment: CrossAxisAlignment.start,
      //                             children: [
      //                               Text(
      //                                 "Info mariage ",
      //                                 style: TextStyle(
      //                                   fontSize: 10,
      //                                   fontWeight: FontWeight.bold,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               Divider(
      //                                 color: Design.colorBlack,
      //                               ),
      //                               Text(
      //                                 "Theme couleur:",
      //                                 style: TextStyle(
      //                                   fontSize: 8,
      //                                   fontWeight: FontWeight.bold,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               Text(
      //                                 "Blue et jaune ",
      //                                 style: TextStyle(
      //                                   fontSize: 8,
      //                                   fontWeight: FontWeight.normal,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               SizedBox(
      //                                 height: 10,
      //                               ),
      //                               Text(
      //                                 "Commité mariage:",
      //                                 style: TextStyle(
      //                                   fontSize: 8,
      //                                   fontWeight: FontWeight.bold,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               Text(
      //                                 "Président ",
      //                                 style: TextStyle(
      //                                   fontSize: 8,
      //                                   fontWeight: FontWeight.normal,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               SizedBox(
      //                                 height: 10,
      //                               ),
      //                               Text(
      //                                 "Mr OGOUA Pierre:",
      //                                 style: TextStyle(
      //                                   fontSize: 8,
      //                                   fontWeight: FontWeight.bold,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               Row(
      //                                 children: [
      //                                   Text(
      //                                     "Tel:",
      //                                     style: TextStyle(
      //                                       fontSize: 8,
      //                                       fontWeight: FontWeight.normal,
      //                                       color: Design.colorBlack,
      //                                     ),
      //                                   ),
      //                                   SizedBox(
      //                                     width: 2,
      //                                   ),
      //                                   Text("066 45 88 87",
      //                                       style: TextStyle(
      //                                         fontSize: 8,
      //                                         fontWeight: FontWeight.normal,
      //                                         color: Design.colorBlack,
      //                                       )),
      //                                 ],
      //                               ),
      //                               SizedBox(
      //                                 height: 20,
      //                               ),
      //                               Text(
      //                                 "Cérémonie",
      //                                 style: TextStyle(
      //                                   fontSize: 10,
      //                                   fontWeight: FontWeight.bold,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               Divider(
      //                                 color: Design.colorBlack,
      //                               ),
      //                               Row(
      //                                 children: [
      //                                   Text(
      //                                     "Lieu:",
      //                                     style: TextStyle(
      //                                       fontSize: 8,
      //                                       fontWeight: FontWeight.normal,
      //                                       color: Design.colorBlack,
      //                                     ),
      //                                   ),
      //                                   SizedBox(
      //                                     width: 2,
      //                                   ),
      //                                   Text("Quartier Glass",
      //                                       style: TextStyle(
      //                                         fontSize: 8,
      //                                         fontWeight: FontWeight.normal,
      //                                         color: Design.colorBlack,
      //                                       )),
      //                                 ],
      //                               ),
      //                               Row(
      //                                 children: [
      //                                   Text(
      //                                     "Salle des fetes:",
      //                                     style: TextStyle(
      //                                       fontSize: 8,
      //                                       fontWeight: FontWeight.normal,
      //                                       color: Design.colorBlack,
      //                                     ),
      //                                   ),
      //                                   SizedBox(
      //                                     width: 2,
      //                                   ),
      //                                   Text("KARE F",
      //                                       style: TextStyle(
      //                                         fontSize: 8,
      //                                         fontWeight: FontWeight.normal,
      //                                         color: Design.colorBlack,
      //                                       )),
      //                                 ],
      //                               ),
      //                               Row(
      //                                 children: [
      //                                   Text(
      //                                     "Tel:",
      //                                     style: TextStyle(
      //                                       fontSize: 8,
      //                                       fontWeight: FontWeight.normal,
      //                                       color: Design.colorBlack,
      //                                     ),
      //                                   ),
      //                                   SizedBox(
      //                                     width: 2,
      //                                   ),
      //                                   Text("077 45 88 87",
      //                                       style: TextStyle(
      //                                         fontSize: 8,
      //                                         fontWeight: FontWeight.normal,
      //                                         color: Design.colorBlack,
      //                                       )),
      //                                 ],
      //                               ),
      //                               SizedBox(
      //                                 height: 20,
      //                               ),
      //                               Text(
      //                                 "Future Mariés",
      //                                 style: TextStyle(
      //                                   fontSize: 10,
      //                                   fontWeight: FontWeight.bold,
      //                                   color: Design.colorBlack,
      //                                 ),
      //                               ),
      //                               Divider(
      //                                 color: Design.colorBlack,
      //                               ),
      //                               Row(
      //                                 children: [
      //                                   Icon(
      //                                     Icons.circle,
      //                                     color: Design.colorBlack,
      //                                     size: 5,
      //                                   ),
      //                                   SizedBox(
      //                                     width: 5,
      //                                   ),
      //                                   Text(
      //                                     "Anne:",
      //                                     style: TextStyle(
      //                                       fontSize: 8,
      //                                       fontWeight: FontWeight.normal,
      //                                       color: Design.colorBlack,
      //                                     ),
      //                                   ),
      //                                   SizedBox(
      //                                     width: 2,
      //                                   ),
      //                                   Text("Paul",
      //                                       style: TextStyle(
      //                                         fontSize: 8,
      //                                         fontWeight: FontWeight.normal,
      //                                         color: Design.colorBlack,
      //                                       )),
      //                                 ],
      //                               ),
      //                             ],
      //                           ),
      //                         ))
      //                       ],
      //                     )),
      //               ),
      //               // Column de droite
      //               Expanded(
      //                 flex: 2,
      //                 child: Container(
      //                   height: double.infinity,
      //                   width: double.infinity,
      //                   // margin: const EdgeInsets.symmetric(vertical: 5.0),
      //                   // decoration: BoxDecoration(
      //                   //   border: Border.all(
      //                   //       width: 2, color: Design.colorMariee),
      //                   // ),
      //                   // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
      //                   child:  Padding(
      //                     padding: const EdgeInsets.only(left:20),
      //                     child: Column(
                          
      //                       mainAxisSize: MainAxisSize.max,
      //                       crossAxisAlignment: CrossAxisAlignment.center,
      //                        mainAxisAlignment: MainAxisAlignment.start,
      //                       children: [
      //                         Align(
      //                           alignment: Alignment.center,
      //                           child: AppWidgetsTitles.TitreDocumentRight(
      //                             title: "Cotisations",
      //                             title2: "(Total 1.800.000 FCFA)",
      //                             color: Design.colorBlack,
      //                             size: 18,
      //                             size2: 8,
      //                           ),
      //                         ),
      //                          const Divider(
      //                                 color: Design.colorBlack,
      //                               ),
      //                       ],
      //                     ),
      //                   ),
      //                 ),
      //               )
      //             ],
      //           ),
      //         ),
      //       ),
      //       SizedBox(
      //         height: _height / 18,
      //       ),
      //       AppWidgetsBoutons.telecharger(
      //           text: 'Télécharger', onPressed: () {}, color: Design.colorvert),
      //     ],
      //   ),
      // ),
    );
  }
}
