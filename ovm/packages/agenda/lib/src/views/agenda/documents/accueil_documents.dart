import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/design/design.dart';
import 'package:agenda/src/views/agenda/documents/buffet/bufffet_Doc.dart';
import 'package:agenda/src/views/agenda/documents/cotisation/cotisation_Doc.dart';
import 'package:agenda/src/views/agenda/documents/invit%C3%A9/invite_Doc.dart';
import 'package:agenda/src/views/agenda/documents/tables/tables_Doc.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class AccueilDocument extends StatelessWidget {
  const AccueilDocument({super.key});

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 60,

        title: const Text(
          'Documents',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            AppWidgetsTitles.bar(),
            const Padding(
                padding: EdgeInsets.only(top: 40, bottom: 60),
                child: Column(
                  children: [
                    Text(
                      "Consulter et imprimer",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "vos documents",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
            Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                // Bouton cotisation
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AppWidgetsBoutons.buttonLink(
                      title: "Cotisations",
                      colorText: Design.colorvert,
                      colorBorder: Design.colorvert,
                      width: _width / 4,
                      height: _height / 6,
                      image: "assets/icones/AGENDA/onboarding_26_budget.png",
                      imageWidth: _width / 5.5,
                      imageheight: _height / 11,
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const Cotisation(),
                            type: PageTransitionType.rightToLeft,
                            //childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                      },
                    ),

                    // Bouton buffet
                    AppWidgetsBoutons.buttonLink(
                      title: "Buffet",
                      colorText: Design.ColorMarie,
                      colorBorder: Design.ColorMarie,
                      width: _width / 4,
                      height: _height / 6,
                      image: "assets/icones/AGENDA/noun-caterer-455764.png",
                      imageWidth: _width / 6,
                      imageheight: _height / 11,
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const Buffet(),
                            type: PageTransitionType.rightToLeft,
                            //childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                      },
                    )
                  ],
                ),
                SizedBox(
                  height: _height / 12,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AppWidgetsBoutons.buttonLink(
                      title: "Invités",
                      colorText: Design.colorBleu,
                      colorBorder: Design.colorBleu,
                      width: _width / 4,
                      height: _height / 6,
                      image: "assets/icones/AGENDA/notification_1_payement.png",
                      imageWidth: _width / 6,
                      imageheight: _height / 11,
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const Invite(),
                            type: PageTransitionType.rightToLeft,
                            //childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                      },
                    ),

                    // Bouton buffet
                    AppWidgetsBoutons.buttonLink(
                      title: "Tables",
                      colorText: Design.colorMariee,
                      colorBorder: Design.colorMariee,
                      width: _width / 4,
                      height: _height / 6,
                      image: "assets/icones/AGENDA/onboarding_26_budget.png",
                      imageWidth: _width / 6,
                      imageheight: _height / 11,
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const Tables(),
                            type: PageTransitionType.rightToLeft,
                            //  childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                      },
                    )
                  ],
                ),
                SizedBox(
                  height: _height / 12,
                ),
                AppWidgetsBoutons.boutonRetour(
                    colorbouton: Design.colorModuleGrey,
                    width: _width / 1.6,
                    height: _height / 16,
                    onPressed: () {
                      Navigator.pushNamed(context, '/AccueilMariee');
                    })
              ],
            ),
          ],
        ),
      ),
    );
  }
}
