import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';




class ProgressionMariee extends StatefulWidget {
  const ProgressionMariee({super.key});

  @override
  State<ProgressionMariee> createState() => _ProgressionMarieeState();
}

class _ProgressionMarieeState extends State<ProgressionMariee> {
  @override
  Widget build(BuildContext context) {
    final myWidth = MediaQuery.of(context).size.width;
    final myHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Agenda",
          style: TextStyle(color: Design.colorBleu, fontSize: 20),
        ),
        centerTitle: true,
        leading: const BackButton(
          color: Design.colorBleu,
          
        ),
        // actions: const [
        //   Icon(
        //     Icons.dehaze,
        //     color: Colors.black,
        //   )
        // ],
      ),
      backgroundColor: Colors.transparent,
      body: Container(
        width: myWidth,
        height: myHeight,
        color: const Color(0xFFFFFFFF),
        child: Column(
          children: [
            // trait
            Container(
              margin: const EdgeInsets.only(left: 20),
              width: myWidth,
              height: myHeight / 50,
              // color: Colors.blue,
              child: AppWidgetsTitles.bar(),
            ),
            const SizedBox(
              height: 20,
            ),
            // fin
            // col qui contient les deux text
            Container(
              width: myWidth,
              height: myHeight / 7,
              //  color: Colors.red,
              child: Column(
                children: [
                  const Text(
                    "Ma progression",
                    style: TextStyle(
                        color: Design.colorBleu,
                        fontSize: 27,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Faites le point sur chaque Thème",
                    style: TextStyle(
                        color: const Color(0xFF352641).withOpacity(.7), fontSize: 20),
                  )
                ],
              ),
            ),
            // fin du col qui contient les deux text
            Container(
              width: myWidth,
              height: 370,
              color: const Color(0xFFFFFFFF),
              child: ListView(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(20),
                            width: 81,
                            height: 74,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/BUFFET/profile_31_buffet.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 24, right: 24),
                                child: Text(
                                  "Prestataires",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "15%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .15,
                                progressColor: const Color(0xFF7C6BD7),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // deuxieme card
                  const SizedBox(
                    height: 13,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(10),
                            width: 81,
                            height: 74,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/AGENDA/onboarding_26_agenda.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 73),
                                child: Text(
                                  "Agenda",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "20%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .20,
                                progressColor: const Color(0xFF4666E5),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // 3 card
                  const SizedBox(
                    height: 13,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(10),
                            width: 81,
                            height: 74,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 80),
                                child: Text(
                                  "Budget",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "50%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .5,
                                progressColor: const Color(0xFF52912E),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // 4 card
                  const SizedBox(
                    height: 13,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(10),
                            width: 81,
                            height: 78,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/COMITE/noun-staff-883920.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 73),
                                child: Text(
                                  "Comités",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "15%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .15,
                                progressColor: const Color(0xFf5F4591),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // 5 card
                  const SizedBox(
                    height: 13,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(10),
                            width: 81,
                            height: 74,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/INVITES/profile_24_invite.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 88),
                                child: Text(
                                  "Invites",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "15%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .15,
                                progressColor: const Color(0xFFE3478F),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // 6 card
                  const SizedBox(
                    height: 13,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(10),
                            width: 81,
                            height: 74,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/TABLES/profile_25_table.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 88),
                                child: Text(
                                  "Tables",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "15%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .15,
                                progressColor: const Color(0xFFFF6310),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // 7 card
                  const SizedBox(
                    height: 13,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 24, right: 24),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xFF00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                            padding: const EdgeInsets.all(10),
                            width: 81,
                            height: 74,
                            // color: Colors.red,
                            child: Image.asset(
                              "assets/icones/BUFFET/profile_31_buffet.png",
                              fit: BoxFit.cover,
                            )),

                        // le titre  et la bare de progress
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                margin: const EdgeInsets.only(right: 88),
                                child: Text(
                                  "Buffet",
                                  style: TextStyle(
                                      color: const Color(0xFF9599B3).withOpacity(.7),
                                      fontSize: 28),
                                )),
                            SizedBox(
                              height: 20,
                              child: Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  // color: Colors.red,
                                  child: Text(
                                    "80%",
                                    style: TextStyle(
                                        color:
                                        const Color(0xFF352641).withOpacity(.7),
                                        fontSize: 14),
                                  )),
                            ),
                            Container(
                              width: 186,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30)),
                              child: LinearPercentIndicator(
                                barRadius: const Radius.circular(30),
                                lineHeight: 20,
                                percent: .8,
                                progressColor: const Color(0xFF4666E5),
                                backgroundColor: const Color(0xFFEAE7F0),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 13,
                  ),
                ],
              ),
            ),
            Expanded(
              child:  Container(
                // color: Colors.green,
                width: myWidth,
                height: myHeight / .3,
                child: AppWidgetsBoutons.boutonRetour(
                    colorbouton: Design.colorBleu,   height: myHeight/16, width:  myWidth/1.6, onPressed: () {
Navigator.pushNamed(context, '/AccueilMariee');
                }),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// creation de ma carte

Widget mycardProgress = Container(
  margin: const EdgeInsets.all(24),
  width: 327,
  height: 105,
  decoration: BoxDecoration(
      boxShadow: const [
        BoxShadow(
            blurRadius: 20,
            color: Color(0xFF00000029),
            offset: Offset(0, 20.0),
            spreadRadius: 10.0)
      ],
      borderRadius: BorderRadius.circular(24),
      color: const Color(0xFFFFFFFF),
      border: Border.all(width: 2, color: const Color(0xFF9599B3))),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Container(
          padding: const EdgeInsets.all(20),
          width: 81,
          height: 74,
          // color: Colors.red,
          child: Image.asset(
            "assets/icones/BUFFET/profile_31_buffet.png",
            fit: BoxFit.cover,
          )),

      // le titre  et la bare de progress
      Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
              margin: const EdgeInsets.only(right: 15),
              child: Text(
                "Prestataires",
                style: TextStyle(
                    color: const Color(0xFF9599B3).withOpacity(.7), fontSize: 28),
              )),
          SizedBox(
            height: 20,
            child: Container(
                margin: const EdgeInsets.only(right: 10),
                // color: Colors.red,
                child: Text(
                  "15%",
                  style: TextStyle(
                      color: const Color(0xFF352641).withOpacity(.7), fontSize: 14),
                )),
          ),
          Container(
            width: 186,
            height: 15,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
            child: LinearPercentIndicator(
              barRadius: const Radius.circular(30),
              lineHeight: 20,
              percent: .15,
              progressColor: const Color(0xFF7C6BD7),
              backgroundColor: const Color(0xFFEAE7F0),
            ),
          )
        ],
      ),
    ],
  ),
);
