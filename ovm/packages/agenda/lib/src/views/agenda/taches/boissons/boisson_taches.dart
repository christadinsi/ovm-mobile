import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class BoissonTaches extends StatefulWidget {
  const BoissonTaches({super.key});

  @override
  State<BoissonTaches> createState() => _BoissonTachesState();
}

class _BoissonTachesState extends State<BoissonTaches> {

  bool check = false;
  TextDecoration? decoration = TextDecoration.none;

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(

        appBar: AppBar(
          //automaticallyImplyLeading: false,
          leading: const BackButton(),
          iconTheme: const IconThemeData(color: Design.colorModuleGrey),
          centerTitle: true,
          toolbarHeight: 60,

          title: const Text(
            'Tâche',
            style: TextStyle(fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Design.colorModuleGrey
            ),
          ),

        ),
        body:  Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child:  Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                AppWidgetsTitles.bar(),
                AppWidgetsTitles.titre(texte: "Boisson", colorText: (Design.colorMariee), size: 25),

                const  Padding(padding: EdgeInsets.only(top: 20, bottom: 40),
                  child: Text(
                    "La fete sera belle avec de la boisson a volonté. alors on ouvre le bar",
                    style: TextStyle(fontSize: 18, color: Colors.black54,
                    ),
                    textAlign: TextAlign.center,
                  )
                ),

                Container(
                  height: _height/2,
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: CheckboxListTile(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          activeColor: Design.colorMariee ,
                          checkboxShape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)
                          ),
                          title: Text("Determiner la quantite de boisson",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              decoration: decoration,
                            ),
                          ),
                          value: check,
                          onChanged: (bool? value) {
                            setState(() {
                              check = !check;
                              if(decoration == TextDecoration.lineThrough)
                                decoration = TextDecoration.none;
                              else
                                decoration = TextDecoration.lineThrough;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,


                        ),
                      ),

                    ],
                  ),
                ),




                AppWidgetsBoutons.buttonValiderTaches(text: 'Valider', onPressed: (){}, color:Design.colorModuleGrey ),

              ]
          ),
        )
    );  }
}
