import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/design/design.dart';
import 'package:agenda/src/views/agenda/taches/alliance/alliance.dart';
import 'package:agenda/src/views/agenda/taches/beaute/beaute_taches.dart';
import 'package:agenda/src/views/agenda/taches/boissons/boisson_taches.dart';
import 'package:agenda/src/views/agenda/taches/budget/budget_taches.dart';
import 'package:agenda/src/views/agenda/taches/buffets/taches_budget.dart';
import 'package:agenda/src/views/agenda/taches/fleurs/taches_fleurs.dart';
import 'package:agenda/src/views/agenda/taches/gadgets/taches_gadgets.dart';
import 'package:agenda/src/views/agenda/taches/gateau/gateau_taches.dart';
import 'package:agenda/src/views/agenda/taches/invit%C3%A9s/taches_invites.dart';
import 'package:agenda/src/views/agenda/taches/mairie/mairie_taches.dart';
import 'package:agenda/src/views/agenda/taches/musique/musiques_taches.dart';
import 'package:agenda/src/views/agenda/taches/papeterie/taches_papeterie.dart';
import 'package:agenda/src/views/agenda/taches/photos/photo_taches.dart';
import 'package:agenda/src/views/agenda/taches/salles/taches_salle.dart';
import 'package:agenda/src/views/agenda/taches/vetements/taches_vetements.dart';
import 'package:agenda/src/views/agenda/taches/videos/video_taches.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class TachesMenu extends StatefulWidget {
  const TachesMenu({super.key});

  @override
  State<TachesMenu> createState() => _TachesMenuState();
}

class _TachesMenuState extends State<TachesMenu> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 2;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
        key: _key,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          leading: const BackButton(),
          iconTheme: const IconThemeData(color: Design.colorMariee),
          centerTitle: true,
          toolbarHeight: 30,
          actions: <Widget>[
            //IconButton
            IconButton(
              icon: const Icon(Icons.dehaze_sharp),
              //color: Colors.grey,
              tooltip: 'Setting Icon',
              onPressed: () {
                _key.currentState?.openDrawer();
              },
            ),
            //IconButton
          ],

          title: const Text(
            'Taches',
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Design.colorModuleGrey),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          //height: _height,
          //width: _width,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              AppWidgetsTitles.bar(),
              Text("Accomplissez toutes les taches propres à chaque sujet",
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 19)),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                height: _height / 1.4,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child:
                            //Budget & Mairie & Invité
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Budget
                            Column(
                              children: [
                                const Text(
                                  "Budget",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const BudgetTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_26_budget.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Mairie
                            Column(
                              children: [
                                const Text(
                                  "Mairie",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const MairieTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_mairie.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Invités
                            Column(
                              children: [
                                const Text(
                                  "Invités",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const InvitesBudget(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/iconInviteAgenda.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child:
                            //Papeterie & Vetements & Salles
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Papeterie
                            Column(
                              children: [
                                const Text(
                                  "Papeterie",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const PapeterieTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_papeterie.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Vetements
                            Column(
                              children: [
                                const Text(
                                  "Vetements",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const VetementTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_vetement.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Salle
                            Column(
                              children: [
                                const Text(
                                  "Salle",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const SalleTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_salle.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child:
                            //Fleurs & Gadgets & Buffet
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Fleurs
                            Column(
                              children: [
                                const Text(
                                  "Fleurs",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const FleursTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_fleure.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Gadgets
                            Column(
                              children: [
                                const Text(
                                  "Gadgets",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const GadgetsTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/profile_97__1_mois.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Buffet
                            Column(
                              children: [
                                const Text(
                                  "Buffet",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    /*Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );*/
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const BuffutTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_buffet.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child:
                            //Boisson & Photos & Vidéos
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Boisson
                            Column(
                              children: [
                                const Text(
                                  "Boisson",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const BoissonTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_boisson.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Photos
                            Column(
                              children: [
                                const Text(
                                  "Photos",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const PhotoTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_photo.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Vidéos
                            Column(
                              children: [
                                const Text(
                                  "Vidéos",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const VideosTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_video.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child:
                            //Gateau & Alliance & Beauté
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Gateau
                            Column(
                              children: [
                                const Text(
                                  "Gateau",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const GateauTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_gateau.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Alliance
                            Column(
                              children: [
                                const Text(
                                  "Alliance",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const AllianceTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_alliance_.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Beauté
                            Column(
                              children: [
                                const Text(
                                  "Beauté",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const BeauteTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_beaute.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child:
                            //Musique
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Musique
                            Column(
                              children: [
                                const Text(
                                  "Musique",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.colorMariee),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const MusiquesTaches(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.colorMariee)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_musique.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              AppWidgetsBoutons.boutonRetour(
                  colorbouton: Design.colorMariee,
                  height: _height / 16,
                  width: _width / 1.6,
                  onPressed: () {
                    Navigator.pushNamed(context, '/AccueilMariee');
                  }),
            ],
          ),
        ));
  }
}
