import 'package:agenda/components/widgets-boutons.dart';
import 'package:agenda/components/widgets-title.dart';
import 'package:agenda/design/design.dart';
import 'package:flutter/material.dart';

class FleursTaches extends StatefulWidget {
  const FleursTaches({super.key});

  @override
  State<FleursTaches> createState() => _FleursTachesState();
}

class _FleursTachesState extends State<FleursTaches> {

  bool check = false;
  TextDecoration? decoration = TextDecoration.none;

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(

        appBar: AppBar(
          //automaticallyImplyLeading: false,
          leading: const BackButton(),
          iconTheme: const IconThemeData(color: Design.colorModuleGrey),
          centerTitle: true,
          toolbarHeight: 60,

          title: const Text(
            'Tâche',
            style: TextStyle(fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Design.colorModuleGrey
            ),
          ),

        ),
        body:  Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child:  Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                AppWidgetsTitles.bar(),
                AppWidgetsTitles.titre(texte: "Fleurs", colorText: (Design.colorMariee), size: 25),

                const  Padding(padding: EdgeInsets.only(top: 20, bottom: 40),
                  child: Text(
                    "Mettez une touche de douceur et d'emotion pendant votre mariage",
                    style: TextStyle(fontSize: 18, color: Colors.black54,
                    ),
                    textAlign: TextAlign.center,
                  )
                ),

               Container(
                width: _width,
                height:_height*.5,
                // color:Colors.green,
                child: ListView(
                  children: [
                     Flexible(
                  flex: 2,
                  child: Container(
                    height: _height*.12,
                    child: Column(
                      children: [
                        CheckboxListTile(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          activeColor: Design.colorMariee ,
                          checkboxShape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)
                          ),
                          title: Text("Evaluez tous les postes de depenses,faites une estimation",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              decoration: decoration,
                            ),
                          ),
                          value: check,
                          onChanged: (bool? value) {
                            setState(() {
                              check = !check;
                              if(decoration == TextDecoration.lineThrough)
                                decoration = TextDecoration.none;
                              else
                                decoration = TextDecoration.lineThrough;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,
                                          
                                          
                        ),
                  
                      ],
                    ),
                  ),
                ),
                 Flexible(
                  flex: 2,
                  child: Container(
                    height: _height*.12,
                    child: Column(
                      children: [
                        CheckboxListTile(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          activeColor: Design.colorMariee ,
                          checkboxShape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)
                          ),
                          title: Text("Determiner un budget pour chaque poste de depenses",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              decoration: decoration,
                            ),
                          ),
                          value: check,
                          onChanged: (bool? value) {
                            setState(() {
                              check = !check;
                              if(decoration == TextDecoration.lineThrough)
                                decoration = TextDecoration.none;
                              else
                                decoration = TextDecoration.lineThrough;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,
                                          
                                          
                        ),
                  
                      ],
                    ),
                  ),
                ),
                 Flexible(
                  flex: 2,
                  child: Container(
                    height: _height*.12,
                    child: Column(
                      children: [
                        CheckboxListTile(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          activeColor: Design.colorMariee ,
                          checkboxShape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)
                          ),
                          title: Text("Definissez vos priorites",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              decoration: decoration,
                            ),
                          ),
                          value: check,
                          onChanged: (bool? value) {
                            setState(() {
                              check = !check;
                              if(decoration == TextDecoration.lineThrough)
                                decoration = TextDecoration.none;
                              else
                                decoration = TextDecoration.lineThrough;
                            });
                          },
                          controlAffinity: ListTileControlAffinity.leading,
                                          
                                          
                        ),
                  
                      ],
                    ),
                  ),
                ),
               
               
                  ],
                ),
               ),

                // d



                AppWidgetsBoutons.buttonValiderTaches(text: 'Valider', onPressed: (){}, color:Design.colorModuleGrey ),

              ]
          ),
        )
    );  }
}
