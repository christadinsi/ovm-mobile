// import 'package:flutter/material.dart';
// import 'package:prestataires/components/widgets-boutons.dart';
// import 'package:prestataires/components/widgets-title.dart';
// import 'package:prestataires/design/design.dart';
// import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/acceuil_materiel.dart';
// import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';

// class PeriodeTroisMois extends StatefulWidget {
//   const PeriodeTroisMois({super.key});

//   @override
//   State<PeriodeTroisMois> createState() => _PeriodeTroisMoisState();
// }

// class _PeriodeTroisMoisState extends State<PeriodeTroisMois> {
//   final GlobalKey<ScaffoldState> _key = GlobalKey();
//   int drawPos = 2;

//   void openDrawerWithSelectValue(int selectedValue) {
//     setState(() {
//       drawPos = selectedValue;
//     });
//     _key.currentState?.openDrawer();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final double _width = MediaQuery.of(context).size.width;
//     final double _height = MediaQuery.of(context).size.height;

//     return Scaffold(
//       key: _key,
//       appBar: AppBar(
//         //automaticallyImplyLeading: false,
//         leading: const BackButton(),
//         iconTheme: const IconThemeData(color: Design.colorModuleGrey),
//         centerTitle: true,
//         toolbarHeight: 30,
//         actions: <Widget>[
//           //IconButton
//           IconButton(
//             icon: const Icon(Icons.dehaze_sharp),
//             //color: Colors.grey,
//             tooltip: 'Setting Icon',
//             onPressed: () {
//               _key.currentState?.openDrawer();
//             },
//           ),
//           //IconButton
//         ],

//         title: const Text(
//           '3 mois',
//           style: TextStyle(
//               fontSize: 20,
//               fontWeight: FontWeight.bold,
//               color: Design.colorModuleGrey),
//         ),
//       ),
//       body: Container(
//         padding: const EdgeInsets.symmetric(horizontal: 20),
//         child: Column(
//           mainAxisSize: MainAxisSize.max,
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           children: [
//             AppWidgetsTitles.bar(),
//             AppWidgetsTitles.titre(
//                 texte: "Mariage prévus dans 3 mois",
//                 colorText: (Design.colorvert),
//                 size: 22),
//             Image(
//               image: const AssetImage(
//                   "assets/icones/AGENDA/menu_34_periode.png"),
//               width: _width / 2.5,
//             ),
//             const Text(
//               "Planifier tous les details !",
//               textAlign: TextAlign.center,
//               style: TextStyle(fontSize: 17, color: Colors.black54),
//             ),
//             Container(
//               height: _height / 3,
//               child: Column(
//                 mainAxisSize: MainAxisSize.max,
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: [
//                   //Salles
//                   Row(
//                     mainAxisSize: MainAxisSize.max,
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: [
//                       //1er mois
//                       Column(
//                         children: [
//                           // Image(
//                           //   image: const AssetImage(
//                           //       "assets/icones/PRESTATAIRES/Plandetravail2.png"),
//                           //   width: _width / 5,
//                           //   height: _width / 5,
//                           // ),

//                           InkWell(
//                             child: Container(
//                               width: _width / 4,
//                               height: _height / 7,
//                               margin: const EdgeInsets.symmetric(vertical: 5.0),
//                               decoration: BoxDecoration(
//                                   //color: Colors.black12,
//                                   borderRadius: BorderRadius.circular(35),
//                                   border: Border.all(
//                                       width: 2, color: Design.colorvert)),
//                               child: Column(
//                                 mainAxisSize: MainAxisSize.max,
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Image(
//                                     image: const AssetImage(
//                                         "assets/icones/AGENDA/menu_34_periode.png"),
//                                     width: _width / 5,
//                                     height: _width / 5,
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             onTap: () {},
//                           ),
//                           const Text(
//                             "1er mois",
//                             style: TextStyle(
//                                 fontSize: 20,
//                                 fontFamily: 'Poppins',
//                                 fontWeight: FontWeight.bold,
//                                 color: Design.colorvert),
//                           ),
//                         ],
//                       ),

//                       //2eme mois
//                       Column(
//                         children: [
//                           // SizedBox(
//                           //   width: _width / 5,
//                           //   height: _width / 5,
//                           // ),
//                           InkWell(
//                             child: Container(
//                               width: _width / 4,
//                               height: _height / 7,
//                               margin: const EdgeInsets.symmetric(vertical: 5.0),
//                               decoration: BoxDecoration(
//                                   //color: Colors.black12,
//                                   borderRadius: BorderRadius.circular(35),
//                                   border: Border.all(
//                                       width: 2, color: Design.colorvert)),
//                               child: Column(
//                                 mainAxisSize: MainAxisSize.max,
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Image(
//                                     image: const AssetImage(
//                                         "assets/icones/AGENDA/menu_34_periode.png"),
//                                     width: _width / 5,
//                                     height: _width / 5,
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             onTap: () {},
//                           ),
//                           const Text(
//                             "2eme mois",
//                             style: TextStyle(
//                                 fontSize: 20,
//                                 fontFamily: 'Poppins',
//                                 fontWeight: FontWeight.bold,
//                                 color: Design.colorvert),
//                           ),
//                         ],
//                       ),

//                       // 3eme mois
//                       Column(
//                         children: [
//                           // SizedBox(
//                           //   width: _width / 5,
//                           //   height: _width / 5,
//                           // ),
//                           InkWell(
//                             child: Container(
//                               width: _width / 4,
//                               height: _height / 7,
//                               margin: const EdgeInsets.symmetric(vertical: 5.0),
//                               decoration: BoxDecoration(
//                                   //color: Colors.black12,
//                                   borderRadius: BorderRadius.circular(35),
//                                   border: Border.all(
//                                       width: 2, color: Design.colorvert)),
//                               child: Column(
//                                 mainAxisSize: MainAxisSize.max,
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Image(
//                                     image: const AssetImage(
//                                         "assets/icones/AGENDA/menu_34_periode.png"),
//                                     width: _width / 5,
//                                     height: _width / 5,
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             onTap: () {},
//                           ),
//                           const Text(
//                             "3eme mois",
//                             style: TextStyle(
//                                 fontSize: 20,
//                                 fontFamily: 'Poppins',
//                                 fontWeight: FontWeight.bold,
//                                 color: Design.colorvert),
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//             AppWidgetsBoutons.boutonRetour(
//                 colorbouton: Design.colorvert,
//                 height: _height / 16,
//                 width: _width / 1.6,
//                 onPressed: () {
//                   Navigator.pushNamed(context, '/AccueilMariee');
//                 }),
//           ],
//         ),
//       ),
//       drawer: DrawerPrestataires(check: drawPos),
//     );
//   }
// }
