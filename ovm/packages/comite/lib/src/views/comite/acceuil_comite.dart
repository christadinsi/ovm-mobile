import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/components/widgets-title.dart';
import 'package:comite/design/design.dart';
import 'package:comite/src/views/comite/accueil_comite/pop_search_commite.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'accueil_comite/reunions/reunion_comite__accueil.dart';

class AccueilCommite extends StatefulWidget {
  const AccueilCommite({super.key});

  @override
  State<AccueilCommite> createState() => _AccueilCommiteState();
}

class _AccueilCommiteState extends State<AccueilCommite> {
  TextEditingController value = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;
    
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Design.ColorMarie,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: _height / 1.8,
                child: Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(top: 50),
                      height: _height / 1.8,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/marieSplashScreen15.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: AppWidgetsTitles.titre(
                            texte: "Comité",
                            colorText: (Design.ColorMarie),
                            size: 35),
                      ),
                    ),

                    Container(
                      padding: const EdgeInsets.only(bottom: 25),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: [0.15, 0.50],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          //tileMode: TileMode.mirror,
                          colors: <Color>[
                            Colors.white,
                            Colors.white70.withOpacity(0.0)
                          ],
                        ),
                      ),
                      child: const Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                            "Amis , frere et sœurs  des futurs maries, s'organisent pour la réussite du mariage",
                          textAlign: TextAlign.center,

                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
                    Center(
                      child: SizedBox(
                        width: _width /1.2,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Design.ColorMarie
                          ),
                            onPressed: (){
                              Navigator.push(
                                context,
                                PageTransition(
                                  child:  const PopSearchCommite(),
                                  type: PageTransitionType.rightToLeft,
                                  //duration: const Duration(milliseconds: 500),
                                ),
                              );
                            }, child:const Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text('Rechercher membres',
                              style: TextStyle(
                                  color: Colors.white,
                                fontSize: 20
                              ),
                            ),
                            Icon(Icons.search, color: Colors.white,  size: 25,)
                          ],
                        )
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  height: _height / 2.5,
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //Cotisations
                          Column(
                            children: [
                              const Text(
                                "Membres",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.ColorMarie),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                  const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                    //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.ColorMarie)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/COMITE/noun-staff-883920.png"),
                                        width: _width * .15,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  //  path
                                },
                              ),
                            ],
                          ),

                          //Depenses
                          Column(
                            children: [
                              const Text(
                                "Réunions",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorvert),
                              ),
                              InkWell(
                                onTap: () {
                                  //  path
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const ReunionAccueil(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,
                                  margin:
                                  const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                    //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.colorvert)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/COMITE/noun-meeting.png"),
                                        width: _width * .15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          //Prevision
                          Column(
                            children: [
                              const Text(
                                "Contacts",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorModuleGrey),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                  const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                    //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2,
                                          color: Design.colorModuleGrey)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/BUDGET/agenda.png"),
                                        width: _width * .15,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  //  path
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                      AppWidgetsBoutons.boutonRetour(
                          colorbouton: const Color(0xFF7C6BD7),
                          height: _height / 16,
                          width: _width / 1.6,
                          onPressed: () {
                            Navigator.pushNamed(context, '/AccueilMariee');
                          }),
                    ],

                  ))
            ],
          )
        ],
      ),
    );
  }
}
