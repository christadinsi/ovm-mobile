
import "package:comite/design/design.dart";
import "package:flutter/material.dart";

BtnSheetHistorique({
  required String title,
  required Icon icon,
  required String personOrSubjet,
  context,
}) {
 {
   
    return showModalBottomSheet(
      backgroundColor: Design.ColorWhite,
        context: context,
        builder: (BuildContext context) {
          return Container(
             height:503,
             decoration: BoxDecoration(
              border: Border.all(width: 1,color: Design.colorModuleGrey),
              borderRadius: BorderRadius.only(topLeft:Radius.circular(25),topRight: Radius.circular(25))
             ),
             child: Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      title,
                    style: const TextStyle(fontSize: 30,color: Design.colorModuleGrey,fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                  
                  Container(
                    width: 62,
                    height: 62,
                    decoration: BoxDecoration(
                      color: Design.colorModuleGrey,
                      borderRadius: BorderRadius.circular(50)
                    ),
                    child: IconButton(
                      icon: Icon(Icons.add,size: 20,color: Design.ColorWhite,),
                      onPressed: (){},
                      ),
                  ),
                    Container(
                    width: 62,
                    height: 62,
                    decoration: BoxDecoration(
                      color: Design.colorModuleGrey,
                      borderRadius: BorderRadius.circular(50)
                    ),
                    child: IconButton(
                      icon: Icon(Icons.remove,size: 20,color: Design.ColorWhite,),
                      onPressed: (){},
                      ),
                  ),
                ],),
                SizedBox(height: 20,),
                Container(
                  height: 200,
                  child: ListView(
                    children: [
                      Row(
                        
                        children: [
                        Padding(
                          padding: const EdgeInsets.only(left:50.0),
                          child: icon,
                        ),
                        SizedBox(width: 20,),
                          Expanded(child: Text(personOrSubjet,style: TextStyle(color: Design.colorModuleGrey),))
                      ],)
                    ],
                  ),
                )
              ],
             ),

          );
        });
  }
}
