import 'package:comite/components/widgets-title.dart';
import 'package:comite/design/design.dart';
import 'package:comite/src/views/comite/accueil_comite/reunions/historique/composant_reunion.dart';
import 'package:comite/src/views/comite/accueil_comite/reunions/historique/fichier_drawer.dart';
import 'package:flutter/material.dart';




class HistoriqueCommite extends StatefulWidget {
  const HistoriqueCommite({super.key});

  @override
  State<HistoriqueCommite> createState() => _HistoriqueCommiteState();
}

class _HistoriqueCommiteState extends State<HistoriqueCommite> {
   final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

   void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
     final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
     final time =  DateTime.now();

    return Scaffold(
      key: _key,
      drawer: DrawerBudget(check: drawPos),

      appBar: AppBar(
        title: const Text("Comité",style: TextStyle(color: Color(0xFF9599B3),fontSize: 20,fontWeight: FontWeight.bold),),
        leading: const BackButton(
          color: Color(0xFF5F4591)
        ),
          actions: [
           IconButton(
             icon: const Icon(
               Icons.dehaze,
              color: Design.violet,
             ),
             onPressed: () {
               _key.currentState?.openDrawer();
             },
           )
         ],
        
      ),
      body: Column(
        children: [
           AppWidgetsTitles.bar(),
           const Padding(
             padding: EdgeInsets.only(top:15.0,bottom: 10),
             child: Align(
              alignment: Alignment.topCenter,
              child: Text("Historique des réunions",
              style: TextStyle(
                fontFamily: 'poppins',
                fontSize: 28,
                color: Design.violet,
                fontWeight: FontWeight.bold
              ),
              ),
             ),
           ),
         Padding(
           padding: const EdgeInsets.only(left:16.0,top: 10),
           child: Align(
              alignment: Alignment.topRight,
           
              child: Text(  "Amis , frere et sœurs  des futurs maries, s'organisent pour la réussite du mariage",
                  style: TextStyle(fontSize: 15,color: Design.colorModuleGrey.withOpacity(.9)),
              ),
             ),
         ),
         SizedBox(height: 20,),
         Container(
          width: myWidth*.98,
          height: myHeigth*.07,
          decoration: BoxDecoration(
            color: Design.violet,
            borderRadius: BorderRadius.circular(24),
          ),
          child: const Align(
            alignment: Alignment.center,
            child: Text("Tout partager",
            style: TextStyle(color: Design.ColorWhite,fontSize: 20,fontWeight: FontWeight.bold),
            )),
          
         ),
         SizedBox(height: 29,),
       Container(
        width: myWidth,
        height: 430,
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 1',context: context),
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 2',context: context),
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 3',context: context),

              ],
            ),
             SizedBox(height: 10,),
            // deuxieme range
              Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 4',context: context),
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 5',context: context),
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 6',context: context),

              ],
            ),
            // troisieme range
            SizedBox(height: 10,),
              Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 7',context: context,),
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 8',context: context),
                ReunionComposant(img: 'assets/icones/COMITE/noun-meeting.png',title: 'Reunion 9',context: context),

              ],
            )
          ],
        ),

       )
        ],
      ),
    );
  }
}