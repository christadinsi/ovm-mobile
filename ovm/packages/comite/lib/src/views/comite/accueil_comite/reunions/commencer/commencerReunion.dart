import 'package:comite/components/popu_partager_reunion_composant.dart';
import 'package:comite/components/reunion.dart';
import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/design/design.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class CommencerReunion extends StatelessWidget {
  const CommencerReunion({super.key});

  @override
  Widget build(BuildContext context) {
     final double height = MediaQuery.of(context).size.height;
     final double width = MediaQuery.of(context).size.width;
    return  reunionComponent(
      heightSizebox: height/30,
      width: width,
      height: height,
      title: "Réunion", 
      title2: "1",
      titledate: "13/03/2022",
      backColorConatainerInAppbar: Design.colorvert,
      widgetComposant1: AppWidgetsBoutons.reunionButton(text: 'Étaient spéciaux', onPressed: () {}, color: Design.colorBleu , width:width*3 , height: height/2.7),
      widgetComposant2: AppWidgetsBoutons.reunionButton(text: 'Ordres du jour', onPressed: () {}, color: Design.ColorMarie , width:width*3 , height: height/2.7),
      widgetComposant3: AppWidgetsBoutons.reunionButton(text: 'A retenir', onPressed: () {}, color: Design.colorvert , width:width*3 , height: height/2.7),
       onTap: () { 
               Navigator.push(
                           context,
                           PageTransition(
                             child: const  PopuPartagerReunion(),
                            type: PageTransitionType.rightToLeft,
                            //  childCurrent: widget,
                             duration: const Duration(milliseconds: 500),
                           ),
                         );

        },
    );
  }
  }