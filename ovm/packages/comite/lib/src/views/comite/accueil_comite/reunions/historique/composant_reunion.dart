

import 'package:comite/design/design.dart';
import 'package:comite/src/views/comite/accueil_comite/reunions/historique/voir_historique_reunion.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

ReunionComposant({
  required String img,
  required String title,
  String? date,
   context,
}) {
  return Column(
    children: [
      InkWell(
        onTap: () {
          print("ok");
              Navigator.push(
                          context,
                          PageTransition(
                            child: const VoirHistoriqueReunion(),
                            type: PageTransitionType.rightToLeft,
                            //  childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
        },
        child: Container(
          padding: const EdgeInsets.all(13),
          width: 97,
          height: 126,
          decoration: BoxDecoration(
              border: Border.all(width: 2, color: Design.violet),
              borderRadius: BorderRadius.circular(40)),
          child: Image.asset(img),
        ),
      ),
      Align(
        alignment: Alignment.center,
        child: Text(
          title,
          style: const TextStyle(
              fontWeight: FontWeight.bold, fontFamily: 'poppins', fontSize: 18),
        ),
      ),
      Align(
        alignment: Alignment.center,
        child: Text(
          '${date}',
          style: const TextStyle(
              fontWeight: FontWeight.bold, fontFamily: 'poppins', fontSize: 18),
        ),
      )
    ],
  );
}
