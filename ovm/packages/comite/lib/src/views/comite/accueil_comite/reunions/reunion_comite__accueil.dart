import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/components/widgets-title.dart';
import 'package:comite/design/design.dart';
import 'package:comite/src/views/comite/accueil_comite/reunions/commencer/commencerReunion.dart';
import 'package:comite/src/views/comite/accueil_comite/reunions/historique/historique_reunion.dart';
import 'package:comite/src/views/comite/drawer_comite.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class ReunionAccueil extends StatefulWidget {
  const ReunionAccueil({super.key});

  @override
  State<ReunionAccueil> createState() => _ReunionAccueilState();
}

class _ReunionAccueilState extends State<ReunionAccueil> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,
        actions: <Widget>[
          //IconButton
          IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              openDrawerWithSelectValue(6);
            },
          ),
          //IconButton
        ],

        title: const Text(
          'Comité',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            AppWidgetsTitles.bar(),
            AppWidgetsTitles.titre(
                texte: "Réunions", colorText: (Design.colorvert), size: 25),
            Image(
              image: const AssetImage(
                  "assets/icones/COMITE/noun-meeting.png"),
              width: _width / 2.5,
            ),
            const Column(
              mainAxisSize: MainAxisSize.max,
              // mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "C'est le moment de faire",
                      style: TextStyle(fontSize: 20, color: Colors.black54),
                    ),
                  ],
                ),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "le point sur tous les détails",
                      style: TextStyle(fontSize: 20, color: Colors.black54),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              height: _height / 3,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //Décorateurs & Fleuristes
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      AppWidgetsBoutons.buttonLink(
                        title: 'Commencer', 
                        colorText: Design.colorvert, 
                        colorBorder: Design.colorvert,
                         width: _width / 4,
                          height: _height / 6, 
                           image: "assets/icones/COMITE/noun-meeting.png",
                        imageWidth: _width / 6, 
                        imageheight: _height / 11, 
                          onTap: () {
                              Navigator.push(
                           context,
                           PageTransition(
                             child: const CommencerReunion(),
                             type: PageTransitionType.rightToLeft,
                               childCurrent: widget,
                             duration: const Duration(milliseconds: 500),
                           ),
                         );
                          },
                       
                       ), AppWidgetsBoutons.buttonLink(
                        title: 'Historique', 
                        colorText: Design.colorModuleGrey, 
                        colorBorder: Design.colorModuleGrey,
                         width: _width / 4,
                          height: _height / 6, 
                           image: "assets/icones/COMITE/noun-clock.png",
                        imageWidth: _width / 6, 
                        imageheight: _height / 11, 
                          onTap: () {
                            Navigator.push(
                          context,
                          PageTransition(
                            child: const HistoriqueCommite(),
                            type: PageTransitionType.rightToLeft,
                            //  childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                          },
                       
                       )
                    ],
                  ),
                ],
              ),
            ),
            AppWidgetsBoutons.boutonRetour(
                colorbouton: Design.colorvert,
                height: _height / 16,
                width: _width / 1.6,
                onPressed: () {
                  Navigator.pushNamed(context, '/AccueilMariee');
                }),
          ],
        ),
      ),
       drawer: DrawerComiteMarie(check: drawPos),
    );
  }
}
