import 'package:comite/components/reunion.dart';
import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/design/design.dart';
import 'package:comite/src/views/comite/accueil_comite/reunions/historique/liste_presence.dart';
import 'package:flutter/material.dart';

class VoirHistoriqueReunion extends StatelessWidget {
  const VoirHistoriqueReunion({super.key});

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return reunionComponent(
      heightSizebox: height/30,
      width: width,
      height: height,
      title: "Réunion",
      title2: "1",
      titledate: "13/03/2022",
      backColorConatainerInAppbar: Design.colorModuleGrey,
      widgetComposant1: AppWidgetsBoutons.reunionButton(
          text: 'Étaient present',
          onPressed: () {
            BtnSheetHistorique(
                title: 'Étaient present',
                icon: Icon(Icons.person),
                personOrSubjet: 'Loundou sabi',
                context: context);
            print("okey");
          },
          color: Design.colorModuleGrey,
          width: width * 3,
          height: height / 2.7),
      widgetComposant2: AppWidgetsBoutons.reunionButton(
          text: 'Ordres du jour',
          onPressed: () {
            BtnSheetHistorique(
                title: 'Ordre du jour',
                icon: Icon(Icons.dangerous),
                personOrSubjet: 'Les boisons',context: context);
          },
          color: Design.colorModuleGrey,
          width: width * 3,
          height: height / 2.7),
      widgetComposant3: AppWidgetsBoutons.reunionButton(
          text: 'A retenir',
          onPressed: () {
            BtnSheetHistorique(title: 'A retenir', icon: Icon(Icons.dangerous), personOrSubjet:'Achat des boisons demain',context: context );
          },
          color: Design.colorModuleGrey,
          width: width * 3,
          height: height / 2.7),
      onTap: () {},
    );
  }
}
