// import 'package:flutter/material.dart';
// import 'dart:ui' as ui;


// class PopSearchCommite extends StatefulWidget {
//   const PopSearchCommite({super.key});

//   @override
//   State<PopSearchCommite> createState() => _PopSearchCommiteState();
// }

// class _PopSearchCommiteState extends State<PopSearchCommite> {
//   @override
//   Widget build(BuildContext context) {


//     final retour = null;
    
//     final double myw = MediaQuery.of(context).size.width;
//     final double myh = MediaQuery.of(context).size.height;
//     final mytop = MediaQuery.of(context).size.height*.25;
//     return Material(
//       child: Stack(
//         children: [
//           // AccueilProvisions(),
//           Positioned(
//             child: Stack(
//               alignment: Alignment.center,
//               children: [
//                 BackdropFilter(
//                     filter: ui.ImageFilter.blur(
//                       sigmaX: 8.0,
//                       sigmaY: 8.0,
//                     ),
//                     child: GestureDetector(
//                       child: Container(
//                         width: myw,
//                         height: myh,
//                         decoration: BoxDecoration(
//                             //  color: Color(0xFF998FA2).withOpacity(.6),
//                             boxShadow: [
//                               BoxShadow(
//                                   color: Color(0xFF998FA2).withOpacity(.6),
//                                   blurRadius: 9)
//                             ]),
//                       ),
//                     ))
//               ],
//             ),
//           ),
      
         
//         ],
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class PopSearchCommite extends StatefulWidget {
  const PopSearchCommite({Key? key}) : super(key: key);

  @override
  State<PopSearchCommite> createState() => _PopSearchCommiteState();
}

class _PopSearchCommiteState extends State<PopSearchCommite> {
  TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final double myw = MediaQuery.of(context).size.width;
    final double myh = MediaQuery.of(context).size.height;

    return Material(
      child: Stack(
        children: [
          Positioned(
            child: Stack(
              alignment: Alignment.center,
              children: [
                BackdropFilter(
                  filter: ui.ImageFilter.blur(
                    sigmaX: 8.0,
                    sigmaY: 8.0,
                  ),
                  child: GestureDetector(
                    onTap: () {
                      // Close the search screen when tapping outside the search area
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: myw,
                      height: myh,
                      decoration: BoxDecoration(
                        color: Color(0xFF998FA2).withOpacity(.6),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0xFF998FA2).withOpacity(.6),
                            blurRadius: 9,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: myh * 0.25,
                  child: Container(
                    width: myw * 0.8,
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Column(
                      children: [
                        TextField(
                          controller: _searchController,
                          decoration: InputDecoration(
                            hintText: 'Recherche membres',
                            suffixIcon: IconButton(
                              icon: Icon(Icons.search),
                              onPressed: () {
                                // Perform search here
                                String query = _searchController.text;
                                // Perform search operation and update searchResults
                                // For now, let's just print the query
                                print('Search Query: $query');
                                // TODO: Add your search logic here
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        // Add your search results UI here
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
