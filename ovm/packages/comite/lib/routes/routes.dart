// ignore_for_file: implementation_imports
import 'package:comite/comite.dart';
import 'package:flutter/material.dart';

class AppRouterPrestataires {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/AccueilCommite':
        return MaterialPageRoute(builder: (context) => const AccueilCommite());
      
      default:
        return null;
    }
  }
}
