import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/design/design.dart';
import 'package:flutter/material.dart';

reunionComponent({
  required String title,
   required Color backColorConatainerInAppbar,
  required double width,
  required double height,
  required double heightSizebox,
  required void Function()? onTap,
  // required String image,
  // required double imageWidth,
  // required double imageheight,
  required String title2,
   required String titledate,
  // required String image2,
  required Widget widgetComposant1,
  required Widget widgetComposant2,
  required Widget widgetComposant3,

  

}) {
  return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: const BackButton(
          color: Design.colorModuleGrey,
        ),
        iconTheme: const IconThemeData(color: Colors.black),
        actions: <Widget>[
          // Icône de menu sur le conteneur

          Row(
            // mainAxisSize: MainAxisSize.min,
            // mainAxisAlignment: MainAxisAlignment.start,
            children: [
              IconButton(
                icon: const Icon(Icons.dehaze_sharp),
                color: Colors.white,
                tooltip: 'Setting Icon',
                onPressed: () {
                },
              ),
            const  SizedBox(width: 45,)
            ],
          ),
        ],
      ),
      // extendBody: true,
       extendBodyBehindAppBar: true,
      body: Column(
        children: [
          const SizedBox(height:25),
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
           const  SizedBox(width: 20,),
              // Icône de menu sur le conteneur
               Image.asset(
              'assets/icones/COMITE/noun-meeting.png',
              width: 100,
              height: 100,
              fit: BoxFit.cover,
                       ),
              // Conteneur

              Container(
                height: height / 2.7,
                width: width/2.5,
                decoration:  BoxDecoration(
                  color: backColorConatainerInAppbar,
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(22),
                    bottomRight: Radius.circular(22),
                  ),
                ),
                child:  Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 40,),
                      Padding(
                        padding: const EdgeInsets.only(left: 6, right: 4),
                        child: Text(
                          title,
                          style: const TextStyle(color: Colors.white,
                          fontSize: 38,
                          ),
                        ),
                      ),
                        Text(
                        title2,
                        style:const  TextStyle(color: Colors.white,
                        fontSize: 38,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
            const SizedBox(height:20),
             Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 20,),
                  const Text(
                    'Commité d\'organisation',
                    style: TextStyle(color: Colors.black,
                    fontSize: 30,
                    )
                    
              ),
               Text(
                    titledate,
                    style: const TextStyle(color: Colors.black,
                    fontSize: 20,
                    ),
               ) ,
               const Divider(),
                    ],
                  ),
            ),
  const  SizedBox(height: 20,),
             GestureDetector(
               onTap: onTap,
               child: Image(
                    image: const AssetImage(
                        "assets/icones/COMITE/noun-share-1076066.png"),
                    width: width / 10.5,
                  ),
             ),
              SizedBox(height: heightSizebox,),
             widgetComposant1,
               SizedBox(height: heightSizebox,),
             widgetComposant2,
                SizedBox(height: heightSizebox,),
             widgetComposant3

    ],
    ),
      );
}