import 'package:flutter/material.dart';

PopUpPrevision(
    {TextEditingController? value,
    required void Function() onPressed,
    required String img,
    required String title}) {
  return Container(
    width: 329,
    height: 420,
    decoration: BoxDecoration(
        color: Colors.white, borderRadius: BorderRadius.circular(40)),
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
              width: 102,
              height: 116,
              child: Image.asset(
                img,
                fit: BoxFit.cover,
              )),
          // le title
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            title,
            style: const TextStyle(
                fontSize: 25,
                color: Color(0xFF52912E),
                fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            "Budget prévu ",
            style: TextStyle(
                fontSize: 20,
                color: const Color(0xFF241332).withOpacity(.32),
                fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          width: 297,
          height: 37,
          child: TextField(
            controller: value,
            decoration: InputDecoration(
                hintText: 'Montant : $value Fcfa',
                hintStyle: TextStyle(
                    color: const Color(0xFF241332).withOpacity(.32),
                    fontSize: 20)),
          ),
        ),
        const SizedBox(
          height: 50,
        ),
        Container(
          width: 250,
          height: 50,
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: const Color(0xFF52912E),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
              onPressed: () {
                onPressed();
              },
              child: const Text(
                "Prévision",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              )),
        )
      ],
    ),
  );
}
