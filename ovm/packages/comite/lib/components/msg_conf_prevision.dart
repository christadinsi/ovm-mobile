import 'package:flutter/material.dart';

MsgConfirmerAjouterPrevision(
    {

    required String img,
     required String img2,
    required String title}) {
  return Container(
    width: 329,
    height: 320,
    decoration: BoxDecoration(
        color: Colors.white, borderRadius: BorderRadius.circular(40)),
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
              width: 102,
              height: 116,
              child: Image.asset(
                img,
                fit: BoxFit.cover,
              )),
          // le title
        ),
       
        const SizedBox(
          height: 20,
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            " prévision - $title ",
            style: TextStyle(
                fontSize: 20,
                color: const Color(0xFF241332).withOpacity(.32),
                fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Divider(),
      
        SizedBox(
          height: 5,
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(10),
            width: 102,
            height: 100,
            child: Image.asset(img2,fit: BoxFit.cover,)),
        )
       
        
      ],
    ),
  );
}
