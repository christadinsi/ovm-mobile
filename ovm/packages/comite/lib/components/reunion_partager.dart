import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/design/design.dart';
import 'package:flutter/material.dart';

class ReuinionParatger extends StatelessWidget {
  const ReuinionParatger({super.key});

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Material(
      type: MaterialType.transparency,
      child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          width: double.infinity,
          height: double.infinity,
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
               SizedBox(
                height: height/2.5,
              ),
              const Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Réunion Partagée',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              // const SizedBox(
              //   height: 20,
              // ),
             const Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Text(
                   'Pésident',
                   style: TextStyle(
                     fontSize: 25,
                     fontWeight: FontWeight.bold,
                     color: Design.colorvert,
                   ),
                 ),
               ],
             ),
              // const SizedBox(
              //   height: 20,
              // ),
             const Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Text(
                   'Coordonateur',
                   style: TextStyle(
                     fontSize: 25,
                     fontWeight: FontWeight.bold,
                     color: Design.colorvert,
                   ),
                 ),
               ],
             ),
              const SizedBox(
                height: 40,
              ),
              Expanded(
                flex:2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppWidgetsBoutons.buttonLinkSimple(
                        colorBorder: Design.ColorMarie,
                        width: width / 3,
                        height: height / 5,
                        image: "assets/icones/COMITE/onboarding_201_reunion.png",
                        imageWidth: width / 6,
                        imageheight: height / 11)
                  ],
                ),
              )
            ],
          )),
    );
  }
}
