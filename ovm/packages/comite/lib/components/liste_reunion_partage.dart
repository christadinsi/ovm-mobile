
import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/design/design.dart';
import 'package:flutter/material.dart';


class ListReunionPartage extends StatefulWidget {
  const ListReunionPartage({super.key});

  @override
  State<ListReunionPartage> createState() => _ListReunionPartageState();
}

class _ListReunionPartageState extends State<ListReunionPartage> {
   bool check = false;
  TextDecoration? decoration = TextDecoration.none;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return
                                 Expanded(
                                   child:  ListView(
  children: [
    CheckboxListTile(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      activeColor: Design.colorvert,
      checkboxShape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AppWidgetsBoutons.buttonLinkTitreAgauche(
            colorBorder: Design.colorvert,
            title: 'Présidents',
            width: width / 4,
            height: height / 6,
            image: "assets/icones/COMITE/noun-user-3601335.png",
            imageWidth: width / 6,
            imageheight: height / 11,
          ),
        ],
      ),
      value: check,
      onChanged: (bool? value) {
        setState(() {
          check = !check;
        });
      },
      controlAffinity: ListTileControlAffinity.leading,
    ),
     CheckboxListTile(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      activeColor: Design.colorvert,
      checkboxShape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AppWidgetsBoutons.buttonLinkTitreAgauche(
            colorBorder: Design.colorvert,
            title: 'Présidents',
            width: width / 4,
            height: height / 6,
            image: "assets/icones/COMITE/noun-user-3601335.png",
            imageWidth: width / 6,
            imageheight: height / 11,
          ),
        ],
      ),
      value: check,
      onChanged: (bool? value) {
        setState(() {
          check = !check;
        });
      },
      controlAffinity: ListTileControlAffinity.leading,
    ),
     CheckboxListTile(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      activeColor: Design.colorvert,
      checkboxShape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AppWidgetsBoutons.buttonLinkTitreAgauche(
            colorBorder: Design.colorvert,
            title: 'Présidents',
            width: width / 4,
            height: height / 6,
            image: "assets/icones/COMITE/noun-user-3601335.png",
            imageWidth: width / 6,
            imageheight: height / 11,
          ),
        ],
      ),
      value: check,
      onChanged: (bool? value) {
        setState(() {
          check = !check;
        });
      },
      controlAffinity: ListTileControlAffinity.leading,
    ),
    
    // Ajoutez d'autres CheckboxListTiles ici si nécessaire
  ],
)

                                 );
  }
}  