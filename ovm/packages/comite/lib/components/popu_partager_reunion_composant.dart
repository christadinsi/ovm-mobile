import 'dart:ui';
import 'package:comite/components/liste_reunion_partage.dart';
import 'package:comite/components/reunion_partager.dart';
import 'package:comite/components/widgets-boutons.dart';
import 'package:comite/design/design.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class PopuPartagerReunion extends StatefulWidget {
  const PopuPartagerReunion({super.key});

  @override
  State<PopuPartagerReunion> createState() => _PopuPartagerReunionState();
}

class _PopuPartagerReunionState extends State<PopuPartagerReunion> {
   bool check = false;
  TextDecoration? decoration = TextDecoration.none;

  @override
  @override
  Widget build(BuildContext context) {
       final double height = MediaQuery.of(context).size.height;
     final double width = MediaQuery.of(context).size.width;
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          // Background blur effect
          BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              width: width,
              height:height,
              color: Colors.grey.withOpacity(0.5),
              // You can add other decorations here if needed
            ),
          ),
          // Centered container
          Center(
            child: Container(
              width: width/1.3,
              height: height/1.29, 
               decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15.0), // Adjust the radius as needed
              ),
              child:  Column(
                children: [
                  const SizedBox(height: 10,),
                 Image.asset(
                  'assets/icones/COMITE/noun-meeting.png',
                  width: 90,
                  height: 90,
                  fit: BoxFit.cover,
                   ),
                  const SizedBox(height: 20,),
                  const Padding(
                    padding: EdgeInsets.only(left:20, right: 20),
                    child: Text('Partager la reunion 1',style: TextStyle(fontSize: 20,
                    color: Design.colorModuleGrey,
                    ),),
                  ),
                  // Column(
                  //   children: [
                  //     ListView(
                  //       shrinkWrap: true,
                  //       children: const [
                  //         ListReunionPartage(),
                  //       ],
                  //     ),
                  //   ],
                  // )
const ListReunionPartage(),
Container(
  width: 250,
  height: 100,
  decoration: const BoxDecoration(
color: Colors.white,
  ),
  child:  Padding(
    padding: const EdgeInsets.all(20.0),
    child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: const Color(0xFF52912E),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        onPressed: () {
            Navigator.push(
                          context,
                          PageTransition(
                            child: const ReuinionParatger(),
                            type: PageTransitionType.rightToLeft,
                            //  childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
        },
        child: const Text(
          "Partager",
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20),
        )),
  ),
)

                     
        ],
      ),
      ),
      ),
    ]),
    );
  }
}
