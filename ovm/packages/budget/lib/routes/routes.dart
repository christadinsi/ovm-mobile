// ignore_for_file: implementation_imports
import 'package:budget/src/views/budget/budget_mariee.dart';
import 'package:flutter/material.dart';

class AppRouterPrestataires {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/BudgetMariee':
        return MaterialPageRoute(builder: (context) => const BudgetMariee());
   
      default:
        return null;
    }
  }
}
