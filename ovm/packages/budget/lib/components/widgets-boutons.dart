import 'package:budget/design/design.dart';
import 'package:flutter/material.dart';

class AppWidgetsBoutons {
  static Center boutonRetour({
    required Color colorbouton,
    required double width,
    required double height,
    required void Function()? onPressed,
  }) {
    return Center(
      child: SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(colorbouton)),
          onPressed: onPressed,
          child: const Icon(
            Icons.home_filled,
            color: Colors.white,
            size: 40,
          ),
        ),
      ),
    );
  }

  static SizedBox buttonCreer({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60, 
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child: Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }

  static buttonInscription({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child: Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }
 

  static buttonValiderTaches({
    required String text,
    required void Function()? onPressed,
    required Color color,
  }) {
    return SizedBox(
      width: 200,
      height: 60,
      child: ElevatedButton(
        onPressed: () => onPressed!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        child:  Text(
          text,
          style: const TextStyle(color: Design.colorSecondTexte, fontSize: 24),
        ),
      ),
    );
  }


  static buttonBilanBudgetDetail({
    required String titre,
    required String prix,
    required String icon,
    required double width,
    required double height,
    required void Function()? onPressed,
    required Color color,
    required Color colorPrix,
  }) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Card(
          margin: const EdgeInsets.symmetric(vertical: 20),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(60)),
          elevation: 6,
          child:
          Container(
            width: width / 3.5,
            height: height / 5,
            decoration:  BoxDecoration(
              border: Border.all(color: color,width: 3),
              color: Design.colorSecondTexte,
              borderRadius: const BorderRadius.all(
                Radius.circular(50),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Center(
                child: Image.asset(
                  icon,
                  width: width/5,
                  height: width/5,
                ),
              ),
            ),
          ),

        ),

        Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(titre,
              style: const TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),),
            Padding(
              padding: const EdgeInsets.only( top : 15),
              child: RichText(
                text: TextSpan(
                  text: prix,
                  style: TextStyle(
                      fontSize: 25,
                      color: colorPrix,
                      fontWeight: FontWeight.bold),
                  children:  const <TextSpan>[
                    TextSpan(text: ' Fcfa'),
                  ],
                ),
              ),
            ),

          ],
        )
      ],
    );
  }
  
 static buttonLink({
  required String title,
  required Color colorText,
  required Color colorBorder,
  required double width,
  required double height,
  required void Function()? onTap,
  required String image,
  required double imageWidth,
  required double imageheight,
}) {
  return Container(

    child: Column(
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 18,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.bold,
              color: colorText),
        ),
        InkWell(
          
          onTap: onTap,
          child: Container(
            width: width,
            height: height,
    
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: const EdgeInsets.symmetric(vertical: 25.0),
            decoration: BoxDecoration(
                //color: Colors.black12,
                borderRadius: BorderRadius.circular(35),
                border: Border.all(width: 2, color: colorBorder)),
            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage(image),
                  width: imageWidth,
                  height: imageheight,
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

  // static IconButtonChef(
  //     {
  //       required Image image,
  //     // required String text,
  //     required Function onTap,
  //     required Color color}) {
  //   return InkWell(
  //     onTap: (){  },
  //     child: Container(
  //       padding: const EdgeInsets.all(8),
  //       width: 97,
  //       height: 126,
  //       decoration: BoxDecoration(
  //       color: color,
  //         borderRadius: BorderRadius.circular(40)
  //       ),
  //       child: Column(
  //         children: [
  //          Image.asset('$image'),

  //         ],
  //       ),
  //     ),
  //   );
  // }
}
