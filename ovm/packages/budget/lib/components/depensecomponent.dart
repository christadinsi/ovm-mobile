import 'package:budget/design/design.dart';
import 'package:flutter/material.dart';
depensesComponent({
  required String title,
  required Color colorText,
  required double width,
  required double height,
  required String image,
  required int  montant,
}) {
  return Container(
                    margin: const EdgeInsets.only(left: 24, right: 24, bottom: 20),
                    width: 327,
                    height: 105,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 20,
                              color: Color(0xff00000029),
                              offset: Offset(0, 20.0),
                              spreadRadius: 10.0)
                        ],
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xFFFFFFFF),
                        border: Border.all(width: 2, color: const Color(0xFF9599B3))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          flex:1,
                          child: Container(
                              padding: const EdgeInsets.all(20),
                              width: 100,
                              height: 100,
                              // color: Colors.red,
                              child: Image.asset(
                                image,
                                fit: BoxFit.cover,
                              )),
                        ),

                        Expanded(
                          flex:2,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              //le titre 
                              Container(
                                  margin: const EdgeInsets.only(left: 24, right: 24),
                                  child: Text(
                                    title,
                                    style: const TextStyle(
                                        color: Design.colorBleu,
                                        fontSize: 24),
                                  )),
                              SizedBox(
                                height: 20,
                                child: Container(
                                    // le montant 
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          ' $montant',
                                          style: TextStyle(
                                              color:
                                              const Color(0xFF352641).withOpacity(.7),
                                              fontSize: 20),
                                        ),
                                          Text(
                                          ' Fcfa',
                                          style: TextStyle(
                                              color:
                                              const Color(0xFF352641).withOpacity(.7),
                                              fontSize: 20),
                                        ),
                                      ],
                                    )),
                              ),
                              
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
}
