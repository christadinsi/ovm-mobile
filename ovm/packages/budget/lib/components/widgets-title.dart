import 'package:flutter/material.dart';

class AppWidgetsTitles {

  static Text titre({
    required String texte,
    required Color colorText,
    required double size,
  }) {
    return Text(
      texte,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: size,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.bold,
          color: colorText),
    );

  }

  static Row bar(){
    return Row(
      children: [

        Container(
            height: 5,
            width: 60,

            decoration: BoxDecoration(
              color: Colors.black26,
              borderRadius: BorderRadius.circular(50),
            )
        ),

        Padding(padding: const EdgeInsets.symmetric(horizontal: 5),
            child:  Container(
                height: 5,
                width: 10,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(50),
                )
            ) ),
        Container(
            height: 5,
            width: 10,
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(50),
            )                ),
      ],
    );
  }

  static TextInscription({
    required Color color
  })
  {
    return Padding(
      padding: const EdgeInsets.only(top:8.0,left:100),
      child: Text("Inscription",style: TextStyle(color: color,fontSize: 28,fontWeight: FontWeight.bold),),
    );
  }

}