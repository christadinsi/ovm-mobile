import 'dart:ui';

import 'package:flutter/material.dart';

class ConfirmMessageCotisation {
  static Confirm_message_cotisation({
    required Color colorMyIcon,
    required String myTitle,
    required Color colorMyTitle,
    required String myImage,
  }) {
    return Builder(builder: (context) {
      final double myWidth = MediaQuery.of(context).size.width;
      final double myHeight = MediaQuery.of(context).size.height;
      // final timer = Timer(const Duration(seconds: 3), () {
      //   // Effectuer une action lorsque le minuteur se termine
      //   Navigator.pop(context);
      // });
      return Material(
        color: const Color.fromARGB(255, 153, 143, 162),
        child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 9.0,
              sigmaY: 9.0,
            ),
            child: GestureDetector(
              child: AlertDialog(
                backgroundColor: Colors.white,
                icon: Icon(
                  Icons.check_circle_rounded,
                  size: myHeight * .10,
                  color: colorMyIcon,
                ),
                content: SizedBox(
                  width: myWidth * .6,
                  height: myHeight * .25,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              myTitle,
                              style: TextStyle(
                                color: colorMyTitle,
                                fontSize: myWidth / 25,
                              ),
                              maxLines: 1,
                            ),
                            const Divider(
                              color: Colors.grey,
                            ),
                          ],
                        ),
                        Image.asset(
                          myImage,
                          height: myHeight * .13,
                        ),
                      ],
                    ),
                  ),
                ),
                actions: [],
              ),
            )),
      );
    });
  }
}
