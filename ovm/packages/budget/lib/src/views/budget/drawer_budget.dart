import 'package:budget/components/confirm_message_cotisation.dart';
import 'package:budget/src/views/budget/cotisations/accueil_modif_ami.dart';
import 'package:budget/src/views/budget/cotisations/accueil_modifier_famille.dart';
import 'package:budget/src/views/budget/cotisations/popupCotisation/ajout_cotisation.dart';
import 'package:budget/src/views/budget/cotisations/popupCotisation/modif_cotisation.dart';
import 'package:flutter/material.dart';
import 'package:budget/design/design.dart';
import 'package:page_transition/page_transition.dart';

class DrawerBudget extends StatelessWidget {
  final int check;

  const DrawerBudget({Key? key, required this.check}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    if (check == 6) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: const Color.fromARGB(255, 127, 129, 148),
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/price-tag.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Solde facture",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Salle des fêtes",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Désignations',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'KARE F',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Tel : 062 22 22 34',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Détails',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Paiement effectué',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            '1 100 000Fcfa',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Reste à payer',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Date : 05/08/2023',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            height: height / 60,
                          ),
                          const Text(
                            '0 Fcfa',
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ),
      );
    } else if (check == 5) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Colors.green,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/noun-money-1346.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Avance",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Salle des fêtes",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Désignations',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'KARE F',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Tel : 062 22 22 34',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Détails',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Paiement effectué',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            '1 400 000 Fcfa',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Reste à payer',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Date : 05/08/2023',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            height: height / 60,
                          ),
                          const Text(
                            '1 100 000 Fcfa',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ),
      );
    } else if (check == 4) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Design.ColorMarie,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/noun-bills-45964.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Facture",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Salle des fêtes",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Désignations',
                            style: TextStyle(
                                color: Design.ColorMarie,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'KARE F',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Tel : 062 22 22 34',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Détails',
                            style: TextStyle(
                                color: Design.ColorMarie,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Location de la salle',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Service traiteur',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Montant à payer',
                            style: TextStyle(
                                color: Design.ColorMarie,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Date : 05/08/2023',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            height: height / 60,
                          ),
                          const Text(
                            '2 500 000 Fcfa',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ),
      );
    } else if (check == 3) {
      return SizedBox(
        height: height,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            topRight: Radius.circular(90),
          )),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                width: width / 1.1,
                height: height / 3.3,
                padding: EdgeInsets.only(
                  left: width / 12,
                ),
                decoration: const BoxDecoration(
                    color: Design.colorBleu,
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(100))),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      radius: 35,
                      backgroundColor: Colors.white,
                      child: Image.asset(
                        "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                        width: width / 15,
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Cotisations - amis",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "300 000 Fcfa",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              SizedBox(
                // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                height: height / 5.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    //cotisation-amis
                    SizedBox(
                      height: height / 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Scaffold.of(context).closeDrawer();
                            Navigator.push(
                              context,
                              PageTransition(
                                child: AjoutCotisation.Ajout_cotisation(
                                  colorMybutton: Design.colorBleu,
                                  myTitle: "Cotisations - Amis",
                                  colorMyTitle: Design.colorBleu,
                                  myImage:
                                      "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                                  myMessage: ConfirmMessageCotisation
                                      .Confirm_message_cotisation(
                                          colorMyIcon: Design.colorBleu,
                                          myTitle: 'Cotisation - Ajoutée',
                                          colorMyTitle: Design.colorBleu,
                                          myImage:
                                              "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
                                  myInfo: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: "Nom :",
                                          labelStyle: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 20,
                                          ),
                                          alignLabelWithHint: true,
                                        ),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                        initialValue: "",
                                      ),
                                      TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: "Montant :",
                                          labelStyle: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 20,
                                          ),
                                          alignLabelWithHint: true,
                                        ),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                        initialValue: "",
                                      ),
                                    ],
                                  ),
                                ),
                                type: PageTransitionType.rightToLeft,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: const CircleAvatar(
                              radius: 35,
                              backgroundColor: Design.colorBleu,
                              child: Icon(
                                Icons.add,
                                color: Design.ColorWhite,
                              )),
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        InkWell(
                          onTap: () {
                            Scaffold.of(context).closeDrawer();
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const AccueilModifierAmi(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: const CircleAvatar(
                              radius: 35,
                              backgroundColor: Design.colorModuleGrey,
                              child: Icon(
                                Icons.close,
                                color: Design.ColorWhite,
                              )),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height / 40,
                    ),
                    const Text(
                      "20 - personne(s)",
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Design.colorBleu),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: height / 40),
                child: SizedBox(
                  height: height / 2.1,
                  child: ListView.builder(
                    padding: const EdgeInsets.all(0),
                    itemCount: 30,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 100,
                        child: const Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Icone contact
                                Icon(
                                  Icons.person_outlined,
                                  color: Color.fromARGB(255, 201, 202, 206),
                                  size: 35,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Bertrand - Julien",
                                    style: TextStyle(fontSize: 20)),
                              ],
                            ),
                            Text("50 0000 Fcfa",
                                style: TextStyle(
                                    fontSize: 17, color: Design.colorBleu)),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      );
    } else if (check == 2) {
      return SizedBox(
        height: height,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            topRight: Radius.circular(90),
          )),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                width: width / 1.1,
                height: height / 3.3,
                padding: EdgeInsets.only(
                  left: width / 12,
                ),
                decoration: const BoxDecoration(
                    color: Design.colorvert,
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(100))),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      radius: 35,
                      backgroundColor: Colors.white,
                      child: Image.asset(
                        "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                        width: width / 15,
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Cotisations - Famille",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "2 500 000 Fcfa",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              SizedBox(
                // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                height: height / 5.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    //cotisation-famille
                    SizedBox(
                      height: height / 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Scaffold.of(context).closeDrawer();
                            Navigator.push(
                              context,
                              PageTransition(
                                child: AjoutCotisation.Ajout_cotisation(
                                  colorMybutton: Design.colorvert,
                                  myTitle: "Cotisations - Famille",
                                  colorMyTitle: Design.colorvert,
                                  myImage:
                                      "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                                  myMessage: ConfirmMessageCotisation
                                      .Confirm_message_cotisation(
                                          colorMyIcon: Design.colorvert,
                                          myTitle: 'Cotisation - Ajoutée',
                                          colorMyTitle: Design.colorvert,
                                          myImage:
                                              "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
                                  myInfo: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: "Nom :",
                                          labelStyle: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 20,
                                          ),
                                          alignLabelWithHint: true,
                                        ),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                        initialValue: "",
                                      ),
                                      TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: "Montant :",
                                          labelStyle: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 20,
                                          ),
                                          alignLabelWithHint: true,
                                        ),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                        initialValue: "",
                                      ),
                                    ],
                                  ),
                                ),
                                type: PageTransitionType.rightToLeft,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: const CircleAvatar(
                              radius: 35,
                              backgroundColor: Design.colorvert,
                              child: Icon(
                                Icons.add,
                                color: Design.ColorWhite,
                              )),
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        InkWell(
                          onTap: () {
                            Scaffold.of(context).closeDrawer();
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const AccueilModifierFamille(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: const CircleAvatar(
                              radius: 35,
                              backgroundColor: Design.colorModuleGrey,
                              child: Icon(
                                Icons.close,
                                color: Design.ColorWhite,
                              )),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height / 40,
                    ),
                    const Text(
                      "80 - personne(s)",
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Design.colorvert),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: height / 40),
                child: SizedBox(
                  height: height / 2.1,
                  child: ListView.builder(
                    padding: const EdgeInsets.all(0),
                    itemCount: 30,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 100,
                        child: const Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Icone contact
                                Icon(
                                  Icons.person_outlined,
                                  color: Color.fromARGB(255, 201, 202, 206),
                                  size: 35,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text("Papa Jean-Pierre",
                                    style: TextStyle(fontSize: 20)),
                              ],
                            ),
                            Text("200 0000 Fcfa",
                                style: TextStyle(
                                    fontSize: 17, color: Design.colorvert)),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      );
    } else {
      return SizedBox(
        height: height,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            topRight: Radius.circular(90),
          )),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                width: width / 1.1,
                height: height / 3.3,
                padding: EdgeInsets.only(
                  left: width / 12,
                ),
                decoration: const BoxDecoration(
                    color: Design.colorMariee,
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(100))),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      radius: 35,
                      backgroundColor: Colors.white,
                      child: Image.asset(
                        "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                        width: width / 15,
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Apport du couple",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Cotisations",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              SizedBox(
                // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                height: height / 1.7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //couple
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Couple",
                          style: TextStyle(
                              fontSize: 27,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorMariee),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: width / 4,
                          height: height / 6,

                          margin: const EdgeInsets.symmetric(vertical: 5.0),
                          padding: const EdgeInsets.symmetric(vertical: 25.0),
                          decoration: BoxDecoration(
                              //color: Colors.black12,
                              borderRadius: BorderRadius.circular(35),
                              border: Border.all(
                                  width: 2, color: Design.colorMariee)),
                          // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                image: const AssetImage(
                                    "assets/icones/BUDGET/profile_161_couple.png"),
                                width: width * .15,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Text(
                          "4 000 000 Fcfa",
                          style: TextStyle(
                              fontSize: 28,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorModuleGrey),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Scaffold.of(context).closeDrawer();
                            Navigator.push(
                              context,
                              PageTransition(
                                child: AjoutCotisation.Ajout_cotisation(
                                  colorMybutton: Design.colorMariee,
                                  myTitle: "Apport du couple",
                                  colorMyTitle: Design.colorMariee,
                                  myImage:
                                      "assets/icones/BUDGET/profile_161_couple.png",
                                  myMessage: ConfirmMessageCotisation
                                      .Confirm_message_cotisation(
                                          colorMyIcon: Design.colorMariee,
                                          myTitle: 'Apport - Ajouté',
                                          colorMyTitle: Design.colorMariee,
                                          myImage:
                                              "assets/icones/BUDGET/profile_161_couple.png"),
                                  myInfo: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      const Text(
                                        "Saisir le montant",
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 19,
                                        ),
                                      ),
                                      TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: "Montant :",
                                          labelStyle: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 20,
                                          ),
                                          alignLabelWithHint: true,
                                        ),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                        initialValue: "",
                                      ),
                                    ],
                                  ),
                                ),
                                type: PageTransitionType.rightToLeft,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: const CircleAvatar(
                              radius: 35,
                              backgroundColor: Design.colorMariee,
                              child: Icon(
                                Icons.add,
                                color: Design.ColorWhite,
                              )),
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        InkWell(
                          onTap: () {
                            Scaffold.of(context).closeDrawer();
                            Navigator.push(
                              context,
                              PageTransition(
                                child: ModifCotisation.Modif_cotisation(
                                  colorMybutton: Design.colorModuleGrey,
                                  myTitle: "Modifier Apport",
                                  colorMyTitle: Design.colorModuleGrey,
                                  myImage:
                                      "assets/icones/BUDGET/profile_161_couple.png",
                                  myMessage: ConfirmMessageCotisation
                                      .Confirm_message_cotisation(
                                          colorMyIcon: Design.colorModuleGrey,
                                          myTitle: 'Apport - Modifié',
                                          colorMyTitle: Design.colorModuleGrey,
                                          myImage:
                                              "assets/icones/BUDGET/profile_161_couple.png"),
                                  myInfo: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      const Text(
                                        "Saisir le montant",
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 19,
                                        ),
                                      ),
                                      TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: "Montant :",
                                          labelStyle: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 20,
                                          ),
                                          alignLabelWithHint: true,
                                        ),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontSize: 20,
                                        ),
                                        initialValue: "",
                                      ),
                                    ],
                                  ),
                                ),
                                type: PageTransitionType.rightToLeft,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: const CircleAvatar(
                              radius: 35,
                              backgroundColor: Design.colorModuleGrey,
                              child: Icon(
                                Icons.close,
                                color: Design.ColorWhite,
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }
  }
}
