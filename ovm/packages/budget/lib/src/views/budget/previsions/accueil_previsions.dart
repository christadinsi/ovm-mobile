import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/src/views/budget/previsions/popPrevisions/form_previsions.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class AccueilProvisions extends StatefulWidget {
  const AccueilProvisions({super.key});

  @override
  State<AccueilProvisions> createState() => _ProgressionMarieeState();
}

class _ProgressionMarieeState extends State<AccueilProvisions> {
  @override
  Widget build(BuildContext context) {
    final myWidth = MediaQuery.of(context).size.width;
    final myHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Budget",
          style: TextStyle(color: Color(0xFF707070), fontSize: 20),
        ),
        centerTitle: true,
        leading: const BackButton(
          color: Color(0xFF52912E),
        ),
        actions: const [
          Icon(
            Icons.dehaze,
            color: Colors.grey,
          )
        ],
      ),
      backgroundColor: Colors.transparent,
      body: Container(
        width: myWidth,
        height: myHeight,
        color: const Color(0xFFFFFFFF),
        child: Column(
          children: [
            // trait
            Container(
              margin: const EdgeInsets.only(left: 20),
              width: myWidth,
              height: myHeight / 50,
              // color: Colors.blue,
              child: AppWidgetsTitles.bar(),
            ),
            const SizedBox(
              height: 20,
            ),
            // fin
            // col qui contient les deux text
            Container(
              width: myWidth,
              height: myHeight / 7,
              //  color: Colors.red,
              child: Column(
                children: [
                  const Text(
                    "Prévisions",
                    style: TextStyle(
                        color: Color(0xFF52912E),
                        fontSize: 27,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                      width: myWidth * .9,
                      height: myHeight * .075,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 2, color: const Color(0xFF52912E)),
                          borderRadius: BorderRadius.circular(24)),
                      child: const Align(
                        alignment: Alignment.center,
                        child: Text(
                          "6 500 000 Fcfa",
                          style: TextStyle(
                              color: Color(0xFF52912E),
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        ),
                      ))
                ],
              ),
            ),
            // fin du col qui contient les deux text
            Container(
              width: myWidth,
              height: myHeight * .55,
              // color: Colors.amber,
              child: ListView(
                children: [
                  InkWell(
                    onTap: () {
                      print("okey");
                     Navigator.push(
                      context,
                      PageTransition(
                     child: const  FairePrevisions() ,
                     type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                     duration:
                      const Duration(milliseconds: 500),
                      ),
                      );
                    },
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(10),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/AGENDA/onboarding_34_salle.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                               mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Salle de reception',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('0 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  //  deuxieme ligne
                  InkWell(
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(10),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/BUFFET/onboarding_34_buffet.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding:
                                         EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Restauration',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('400 000 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // fin de la deuxieme ligne
                  // debut troisiem ligne
                  InkWell(
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(10),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/AGENDA/onboarding_34_boisson.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Boissons',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('500 000 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // fin de la troisieme ligne
                  // quatrieme lignes
                  InkWell(
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(10),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/PRESTATAIRES/noun-photographer-3920.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Photo',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('250 000 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // fin quatrieme lignes
                  // 5 ligne
                  InkWell(
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(10),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/PRESTATAIRES/noun-videographer-45165.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [
                                  Padding(
                                    padding:
                                       EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Videos',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('500 000 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // fin 5
                  // 6 debut
                  InkWell(
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(17),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/PRESTATAIRES/noun-wedding-dress-1855076.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [
                                  Padding(
                                    padding:
                                         EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Robe de mariee',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('500 000 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // fin 6
                  // debut 7
                  InkWell(
                    child: Container(
                      width: myWidth,
                      height: myHeight * .19,
                      // color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 30,
                            child: Container(
                                margin: EdgeInsets.only(right: 13, left: 13),
                                padding: const EdgeInsets.all(10),
                                width: myWidth * .30,
                                height: myHeight * .16,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 2,
                                        color: const Color(0xFF9599B3)),
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xFF9599B3),
                                          blurRadius: 2,
                                          blurStyle: BlurStyle.outer)
                                    ]),
                                child: Image.asset(
                                    "assets/icones/AGENDA/onboarding_34_beaute.png")),
                          ),
                          Flexible(
                            flex: 70,
                            child: Container(
                              width: myWidth * .70,
                              // color: Colors.green,
                              height: myHeight * .19,
                              child: const Column(
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [
                                  Padding(
                                    padding:
                                         EdgeInsets.only(top: 0.0, left: 1),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Soin  beaute',
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF52912E),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 30.0),
                                      child: Text('150 000 Fcfa',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF9599B3),
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  // fin 7
                ],
              ),
            ),
            SizedBox(height: 20,),
            Expanded(
              child: Container(
                // color: Colors.green,
                width: myWidth,
                height: myHeight / .3,
                child: AppWidgetsBoutons.boutonRetour(
                    colorbouton: const Color(0xFF52912E),
                    height: myHeight / 16,
                    width: myWidth / 1.6,
                    onPressed: () {
                      Navigator.pushNamed(context, '/AccueilMariee');
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// creation de ma carte

Widget mycardProgress = Container(
  margin: const EdgeInsets.all(24),
  width: 327,
  height: 105,
  decoration: BoxDecoration(
      boxShadow: const [
        BoxShadow(
            blurRadius: 20,
            color: Color(0xFF00000029),
            offset: Offset(0, 20.0),
            spreadRadius: 10.0)
      ],
      borderRadius: BorderRadius.circular(24),
      color: const Color(0xFFFFFFFF),
      border: Border.all(width: 2, color: const Color(0xFF9599B3))),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Container(
          padding: const EdgeInsets.all(20),
          width: 81,
          height: 74,
          // color: Colors.red,
          child: Image.asset(
            "assets/icones/BUFFET/profile_31_buffet.png",
            fit: BoxFit.cover,
          )),

      // le titre  et la bare de progress
      Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
              margin: const EdgeInsets.only(right: 15),
              child: Text(
                "Prestataires",
                style: TextStyle(
                    color: const Color(0xFF9599B3).withOpacity(.7),
                    fontSize: 28),
              )),
          SizedBox(
            height: 20,
            child: Container(
                margin: const EdgeInsets.only(right: 10),
                // color: Colors.red,
                child: Text(
                  "15%",
                  style: TextStyle(
                      color: const Color(0xFF352641).withOpacity(.7),
                      fontSize: 14),
                )),
          ),
          Container(
            width: 186,
            height: 15,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
            child: LinearPercentIndicator(
              barRadius: const Radius.circular(30),
              lineHeight: 20,
              percent: .15,
              progressColor: const Color(0xFF7C6BD7),
              backgroundColor: const Color(0xFFEAE7F0),
            ),
          )
        ],
      ),
    ],
  ),
);
