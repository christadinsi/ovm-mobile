import 'package:budget/components/popup_prevision.dart';
import 'package:budget/src/views/budget/previsions/accueil_previsions.dart';
import 'package:budget/src/views/budget/previsions/popPrevisions/update_prevision.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:page_transition/page_transition.dart';

class FairePrevisions extends StatefulWidget {
  const FairePrevisions({super.key});

  @override
  State<FairePrevisions> createState() => _FairePrevisionsState();
}

class _FairePrevisionsState extends State<FairePrevisions> {
  @override
  Widget build(BuildContext context) {
  final my_pop_up=  PopUpPrevision(
        img: 'assets/icones/AGENDA/onboarding_34_salle.png',
        title: 'Salle de reception',onPressed: () {
              Navigator.push(
                 context,
                 PageTransition(
                                      child: const FaireModification(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
        },);

    final retour = null;
    final double myw = MediaQuery.of(context).size.width;
    final double myh = MediaQuery.of(context).size.height;
    return Material(
      child: Stack(
        children: [
          AccueilProvisions(),
          Positioned(
            child: Stack(
              alignment: Alignment.center,
              children: [
                BackdropFilter(
                    filter: ui.ImageFilter.blur(
                      sigmaX: 8.0,
                      sigmaY: 8.0,
                    ),
                    child: GestureDetector(
                      child: Container(
                        width: myw,
                        height: myh,
                        decoration: BoxDecoration(
                            //  color: Color(0xFF998FA2).withOpacity(.6),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFF998FA2).withOpacity(.6),
                                  blurRadius: 9)
                            ]),
                      ),
                    ))
              ],
            ),
          ),
        Positioned(
          top: 130,
          left: 18,
          child: my_pop_up,
        )
       
             
        ],
      ),
    );
  }
}
