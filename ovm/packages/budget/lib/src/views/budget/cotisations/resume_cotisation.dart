import 'package:auto_size_text/auto_size_text.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:flutter/material.dart';

class ResumeCotisation extends StatefulWidget {
  const ResumeCotisation({super.key});

  @override
  State<ResumeCotisation> createState() => _ResumeCotisationState();
}

class _ResumeCotisationState extends State<ResumeCotisation> {
  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      // key: _key,
      // drawer: DrawerBudget(check: drawPos),
      appBar: AppBar(
        // actions: [
        //   IconButton(
        //     icon: Icon(
        //       Icons.dehaze,
        //       color: Colors.grey,
        //     ),
        //     onPressed: () {
        //       _key.currentState?.openDrawer();
        //     },
        //   )
        // ],
        title: const Text(
          "Cotisations",
          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Design.colorvert,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: AppWidgetsTitles.bar(),
          ),
          const Text(
            "Résumé",
            style: TextStyle(
                fontSize: 25,
                color: Design.colorvert,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

            height: myHeight / 1.7,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //Couple
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        const Text(
                          "Couple",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorModuleGrey),
                        ),
                        Container(
                          width: myWidth / 4,
                          height: myHeight / 6,
                          margin: const EdgeInsets.symmetric(vertical: 5.0),
                          padding: const EdgeInsets.symmetric(vertical: 25.0),
                          decoration: BoxDecoration(
                              //color: Colors.black12,
                              borderRadius: BorderRadius.circular(35),
                              border: Border.all(
                                  width: 2, color: Design.colorModuleGrey)),
                          // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                image: const AssetImage(
                                    "assets/icones/BUDGET/profile_161_couple.png"),
                                width: myWidth * .15,
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          "4 000 000 Fcfa",
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorMariee),
                        ),
                      ],
                    ),
                    //Famille
                    Column(
                      children: [
                        const Text(
                          "Famille",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorModuleGrey),
                        ),
                        Container(
                          width: myWidth / 4,
                          height: myHeight / 6,
                          margin: const EdgeInsets.symmetric(vertical: 5.0),
                          padding: const EdgeInsets.symmetric(vertical: 25.0),
                          decoration: BoxDecoration(
                              //color: Colors.black12,
                              borderRadius: BorderRadius.circular(35),
                              border: Border.all(
                                  width: 2, color: Design.colorModuleGrey)),
                          // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                image: const AssetImage(
                                    "assets/icones/BUDGET/profile_161_famille.png"),
                                width: myWidth * .15,
                              ),
                            ],
                          ),
                        ),
                        const Text(
                          "2 500 000 Fcfa",
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorvert),
                        ),
                      ],
                    ),
                  ],
                ),

                //Amis
                Column(
                  children: [
                    const Text(
                      "Amis",
                      style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Design.colorModuleGrey),
                    ),
                    Container(
                      width: myWidth / 4,
                      height: myHeight / 6,

                      margin: const EdgeInsets.symmetric(vertical: 5.0),
                      padding: const EdgeInsets.symmetric(vertical: 25.0),
                      decoration: BoxDecoration(
                          //color: Colors.black12,
                          borderRadius: BorderRadius.circular(35),
                          border: Border.all(
                              width: 2, color: Design.colorModuleGrey)),
                      // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            image: const AssetImage(
                                "assets/icones/BUDGET/profile_161_amis.png"),
                            width: myWidth * .15,
                          ),
                        ],
                      ),
                    ),
                    const Text(
                      "500 000 Fcfa",
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Design.colorBleu),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
              width: myWidth * .8,
              height: myHeight * .070,
              decoration: BoxDecoration(
                  color: Design.colorvert,
                  border: Border.all(width: 2, color: const Color(0xFF52912E)),
                  borderRadius: BorderRadius.circular(24)),
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  "6 300 000 Fcfa",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 23),
                ),
              )),
          SizedBox(
            height: myHeight / 750,
          ),
        ],
      ),
    );
  }
}
