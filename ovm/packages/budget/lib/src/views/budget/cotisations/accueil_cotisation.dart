import 'package:auto_size_text/auto_size_text.dart';
import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/cotisations/resume_cotisation.dart';
import 'package:budget/src/views/budget/drawer_budget.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class AccueilCotisation extends StatefulWidget {
  const AccueilCotisation({super.key});

  @override
  State<AccueilCotisation> createState() => _AccueilCotisationState();
}

class _AccueilCotisationState extends State<AccueilCotisation> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _key,
      drawer: DrawerBudget(check: drawPos),
      appBar: AppBar(
        // actions: [
        //   IconButton(
        //     icon: Icon(
        //       Icons.dehaze,
        //       color: Colors.grey,
        //     ),
        //     onPressed: () {
        //       _key.currentState?.openDrawer();
        //     },
        //   )
        // ],
        title: const Text(
          "Budget",
          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Design.colorvert,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: AppWidgetsTitles.bar(),
          ),
          const Text(
            "Liste des cotisations",
            style: TextStyle(
                fontSize: 25,
                color: Design.colorvert,
                fontWeight: FontWeight.bold),
          ),
          Image(
            image: const AssetImage(
                "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
            width: myWidth / 3.5,
          ),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                PageTransition(
                  child: const ResumeCotisation(),
                  type: PageTransitionType.rightToLeft,
                  //childCurrent: widget,
                  duration: const Duration(milliseconds: 500),
                ),
              );
            },
            child: Container(
                width: myWidth * .8,
                height: myHeight * .075,
                decoration: BoxDecoration(
                    border:
                        Border.all(width: 2, color: const Color(0xFF52912E)),
                    borderRadius: BorderRadius.circular(24)),
                child: const Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Résumé",
                    style: TextStyle(
                        color: Design.colorvert,
                        fontWeight: FontWeight.bold,
                        fontSize: 25),
                  ),
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              //couple
              Column(
                children: [
                  const AutoSizeText(
                    "Couple",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.colorMariee,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        openDrawerWithSelectValue(1);
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeight / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                  color: Design.colorMariee, width: 3),
                              bottom: BorderSide(
                                  color: Design.colorMariee, width: 3),
                              left: BorderSide(
                                  color: Design.colorMariee, width: 3),
                              right: BorderSide(
                                  color: Design.colorMariee, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/BUDGET/profile_161_couple.png",
                                width: myWidth * .15,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              //famille
              Column(
                children: [
                  const AutoSizeText(
                    "Famille",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.colorvert,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        openDrawerWithSelectValue(2);
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeight / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top:
                                  BorderSide(color: Design.colorvert, width: 3),
                              bottom:
                                  BorderSide(color: Design.colorvert, width: 3),
                              left:
                                  BorderSide(color: Design.colorvert, width: 3),
                              right:
                                  BorderSide(color: Design.colorvert, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(13),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/BUDGET/profile_161_famille.png",
                                width: myWidth * .15,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              //amis
              Column(
                children: [
                  const AutoSizeText(
                    "Amis",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.colorBleu,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        openDrawerWithSelectValue(3);
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeight / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top:
                                  BorderSide(color: Design.colorBleu, width: 3),
                              bottom:
                                  BorderSide(color: Design.colorBleu, width: 3),
                              left:
                                  BorderSide(color: Design.colorBleu, width: 3),
                              right:
                                  BorderSide(color: Design.colorBleu, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/BUDGET/profile_161_amis.png",
                                width: myWidth * .15,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  })
                ],
              )
            ],
          ),
          SizedBox(
            height: myHeight / 750,
          ),
          AppWidgetsBoutons.boutonRetour(
              colorbouton: Design.colorvert,
              height: myHeight / 16,
              width: myWidth / 1.6,
              onPressed: () {
                Navigator.pushNamed(context, '/AccueilMariee');
              }),
        ],
      ),
    );
  }
}
