// ignore_for_file: non_constant_identifier_names

import 'dart:async';
import 'dart:ui';
import 'package:budget/components/confirm_message_cotisation.dart';
import 'package:budget/design/design.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class ModifCotisation {
  static Modif_cotisation({
    required Color colorMybutton,
    required String myTitle,
    required Color colorMyTitle,
    required String myImage,
    required Widget myMessage,
    required Widget myInfo,
    // required void Function() myOnTap,
  }) {
    return Builder(builder: (context) {
      final double myWidth = MediaQuery.of(context).size.width;
      final double myHeight = MediaQuery.of(context).size.height;
      return Material(
        color: const Color.fromARGB(157, 153, 143, 162),
        child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 9.0,
              sigmaY: 9.0,
            ),
            child: GestureDetector(
              child: AlertDialog(
                backgroundColor: Colors.white,
                title: Text(
                  myTitle,
                  style: TextStyle(
                      color: colorMyTitle,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                ),
                icon: Image.asset(
                  myImage,
                  height: myHeight * .13,
                ),
                content: SizedBox(
                    width: myWidth / 1.2,
                    height: myHeight / 3.5,
                    child: myInfo),
                actions: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);

                      Navigator.push(
                        context,
                        PageTransition(
                          child: myMessage,
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 500),
                        ),
                      );
                    },
                    child: Padding(
                      padding: EdgeInsets.only(bottom: myHeight / 30),
                      child: Center(
                        child: Container(
                            width: myWidth / 1.7,
                            height: myHeight / 18,
                            decoration: BoxDecoration(
                                color: colorMybutton,
                                borderRadius: BorderRadius.circular(50)),
                            child: const Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Modifier",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25),
                              ),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      );
    });
  }
}
