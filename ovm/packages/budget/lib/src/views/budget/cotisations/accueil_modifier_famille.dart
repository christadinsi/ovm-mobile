import 'package:budget/components/confirm_message_cotisation.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/cotisations/popupCotisation/ajout_cotisation.dart';
import 'package:budget/src/views/budget/cotisations/popupCotisation/modif_cotisation.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class AccueilModifierFamille extends StatefulWidget {
  const AccueilModifierFamille({super.key});

  @override
  State<AccueilModifierFamille> createState() => _AccueilModifierFamilleState();
}

class _AccueilModifierFamilleState extends State<AccueilModifierFamille> {
  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Cotisations",
          style:
              TextStyle(color: Design.colorvert, fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Design.colorModuleGrey,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: AppWidgetsTitles.bar(),
          ),
          const Text(
            "Modifications",
            style: TextStyle(
                fontSize: 25,
                color: Design.colorModuleGrey,
                fontWeight: FontWeight.bold),
          ),
          Image(
            image: const AssetImage(
                "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
            width: myWidth / 3.5,
          ),
          Padding(
            padding: EdgeInsets.only(top: myHeight / 100),
            child: SizedBox(
              height: myHeight / 2.1,
              child: ListView.builder(
                padding: const EdgeInsets.all(0),
                itemCount: 30,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          child: ModifCotisation.Modif_cotisation(
                            colorMybutton: Design.colorvert,
                            myTitle: "Cotisation - Famille",
                            colorMyTitle: Design.colorvert,
                            myImage:
                                "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                            myMessage: ConfirmMessageCotisation
                                .Confirm_message_cotisation(
                                    colorMyIcon: Design.colorvert,
                                    myTitle: 'Cotisation - Modifiée',
                                    colorMyTitle: Design.colorModuleGrey,
                                    myImage:
                                        "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
                            myInfo: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                TextFormField(
                                  decoration: const InputDecoration(
                                    labelText: "Nom :",
                                    labelStyle: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 20,
                                    ),
                                    alignLabelWithHint: true,
                                  ),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    color: Colors.grey,
                                    fontSize: 20,
                                  ),
                                  initialValue: "",
                                ),
                                TextFormField(
                                  decoration: const InputDecoration(
                                    labelText: "Montant :",
                                    labelStyle: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 20,
                                    ),
                                    alignLabelWithHint: true,
                                  ),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    color: Colors.grey,
                                    fontSize: 20,
                                  ),
                                  initialValue: "",
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: ConfirmMessageCotisation
                                            .Confirm_message_cotisation(
                                                colorMyIcon:
                                                    Design.colorModuleGrey,
                                                myTitle: 'Cotisation - Retirée',
                                                colorMyTitle:
                                                    Design.colorModuleGrey,
                                                myImage:
                                                    "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(top: myHeight / 30),
                                    child: Center(
                                      child: Container(
                                          width: myWidth / 1.7,
                                          height: myHeight / 18,
                                          decoration: BoxDecoration(
                                              color: Colors.grey,
                                              borderRadius:
                                                  BorderRadius.circular(50)),
                                          child: const Align(
                                            alignment: Alignment.center,
                                            child: Text(
                                              "Retirer",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 25),
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 500),
                        ),
                      );
                    },
                    child: Container(
                      height: 100,
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // Icone contact
                              Icon(
                                Icons.lens_outlined,
                                color: Color.fromARGB(255, 201, 202, 206),
                                size: 35,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Papa Jean-Pierre",
                                  style: TextStyle(fontSize: 20)),
                            ],
                          ),
                          Text("200 0000 Fcfa",
                              style: TextStyle(
                                  fontSize: 17, color: Design.colorvert)),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
