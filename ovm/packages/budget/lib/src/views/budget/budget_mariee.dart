import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/cotisations/accueil_cotisation.dart';
import 'package:budget/src/views/budget/depenses/depense_mariee.dart';
import 'package:budget/src/views/budget/previsions/accueil_previsions.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class BudgetMariee extends StatefulWidget {
  const BudgetMariee({super.key});

  @override
  State<BudgetMariee> createState() => _BudgetMarieeState();
}

class _BudgetMarieeState extends State<BudgetMariee> {
  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Design.colorvert,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: _height / 1.7,
                child: Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 50),
                      height: _height / 1.9,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/What Do Instructional Designers Do _ Why Are Teachers Some of the BEST Instructional Designers_.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: AppWidgetsTitles.titre(
                            texte: "Budget",
                            colorText: (Design.colorvert),
                            size: 35),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 25),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: [0.10, 0.35],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          //tileMode: TileMode.mirror,
                          colors: <Color>[
                            Colors.white,
                            Colors.white70.withOpacity(0.0)
                          ],
                        ),
                      ),
                      child: const Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          "Budgétiser vos dépenses",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  height: _height / 2.5,
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //Cotisations
                          Column(
                            children: [
                              const Text(
                                "Cotisations",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorvert),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.colorvert)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png"),
                                        width: _width * .15,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const AccueilCotisation(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),

                          //Depenses
                          Column(
                            children: [
                              const Text(
                                "Dépenses",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.ColorMarie),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepenseMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.ColorMarie)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/BUDGET/onboardind_201_facture.png"),
                                        width: _width * .15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          //Prevision
                          Column(
                            children: [
                              const Text(
                                "Prévisions",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorModuleGrey),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2,
                                          color: Design.colorModuleGrey)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: AssetImage(
                                            "assets/icones/BUDGET/profile_32_previtions.png"),
                                        width: _width * .15,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const AccueilProvisions(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                      AppWidgetsBoutons.boutonRetour(
                          colorbouton: Design.colorvert,
                          height: _height / 16,
                          width: _width / 1.6,
                          onPressed: () {
                            Navigator.pushNamed(context, '/AccueilMariee');
                          }),
                    ],
                  ))
            ],
          )
        ],
      ),
    );
  }
}
