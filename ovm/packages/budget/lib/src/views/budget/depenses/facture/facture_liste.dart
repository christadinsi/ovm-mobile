
import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/depenses/facture/accesoire/accesoire_depenser.dart';
import 'package:budget/src/views/budget/depenses/facture/alliance/alliance_depense.dart';
import 'package:budget/src/views/budget/depenses/facture/animateur/animateur_depense.dart';
import 'package:budget/src/views/budget/depenses/facture/beaute/depense_beaute.dart';
import 'package:budget/src/views/budget/depenses/facture/boisson/boisson_depense.dart';
import 'package:budget/src/views/budget/depenses/facture/cadeau/depense_cadeau.dart';
import 'package:budget/src/views/budget/depenses/facture/coiffure/depense_coiffure.dart';
import 'package:budget/src/views/budget/depenses/facture/decoration/depense_decoration.dart';
import 'package:budget/src/views/budget/depenses/facture/divers/depense_divers.dart';
import 'package:budget/src/views/budget/depenses/facture/draget/depense_draget.dart';
import 'package:budget/src/views/budget/depenses/facture/fleure/depense_fleure.dart';
import 'package:budget/src/views/budget/depenses/facture/gateau/depense_gateau.dart';
import 'package:budget/src/views/budget/depenses/facture/lune_de_miel/depense_lune_miel.dart';
import 'package:budget/src/views/budget/depenses/facture/musique/depense_musique.dart';
import 'package:budget/src/views/budget/depenses/facture/papeterie/depense_papeterie.dart';
import 'package:budget/src/views/budget/depenses/facture/photo/depense_photo.dart';
import 'package:budget/src/views/budget/depenses/facture/robe/depense_robe.dart';
import 'package:budget/src/views/budget/depenses/facture/salle/depense_salle.dart';
import 'package:budget/src/views/budget/depenses/facture/traiteur/depense_trateur.dart';
import 'package:budget/src/views/budget/depenses/facture/transport/transport_depense.dart';
import 'package:budget/src/views/budget/depenses/facture/video/video_transport.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class FactureListe extends StatefulWidget {
  const FactureListe({super.key});

  @override
  State<FactureListe> createState() => _FactureListeState();
}

class _FactureListeState extends State<FactureListe> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 2;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
        key: _key,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          leading: const BackButton(),
          iconTheme: const IconThemeData(color: Design.ColorMarie),
          centerTitle: true,
          toolbarHeight: 30,
          actions: <Widget>[
            //IconButton
            IconButton(
              icon: const Icon(Icons.dehaze_sharp),
              //color: Colors.grey,
              tooltip: 'Setting Icon',
              onPressed: () {
                _key.currentState?.openDrawer();
              },
            ),
            //IconButton
          ],

          title: const Text(
            'Depense',
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Design.colorModuleGrey),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          //height: _height,
          //width: _width,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              AppWidgetsTitles.bar(),
              const Text("Toutes vos factures",
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 19,color: Design.ColorMarie)),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                height: _height / 1.4,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Budget & Mairie & Invité
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Salle
                            Column(
                              children: [
                                const Text(
                                  "Salle",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseSalle(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_salle.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Traiteur
                            Column(
                              children: [
                                const Text(
                                  "Traiteur",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseTraiteur(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUFFET/onboarding_34_buffet.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Boisson
                            Column(
                              children: [
                                const Text(
                                  "Boissons",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                     Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseBoisson(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const InvitesBudget(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/TABLES/noun-champagne-45976.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Papeterie & Vetements & Salles
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Photo
                            Column(
                              children: [
                                const Text(
                                  "Photos",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepensePhoto(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const PapeterieTaches(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_photo.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Video
                            Column(
                              children: [
                                const Text(
                                  "Videos",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepenseVideo(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const VetementTaches(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_video.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Accessoire
                            Column(
                              children: [
                                const Text(
                                  "Accessoire",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepenseAccesoir(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const SalleTaches(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_accessoire.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Fleurs & Gadgets & Buffet
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Beaute
                            Column(
                              children: [
                                const Text(
                                  "Beaute",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepenseBeaute(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const FleursTaches(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/PRESTATAIRES/onboarding_34_beaute.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Coiffure
                            Column(
                              children: [
                                const Text(
                                  "Coiffure",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepenseCoiffure(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const GadgetsTaches(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_coiffure.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Fleure
                            Column(
                              children: [
                                const Text(
                                  "Fleure",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const DepenseFleure(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                      const Duration(milliseconds: 500),
                                    ),
                                  );
                                    // Navigator.push(
                                    //   context,
                                    //   PageTransition(
                                    //     child: const BuffutTaches(),
                                    //     type: PageTransitionType.rightToLeft,
                                    //     //childCurrent: widget,
                                    //     duration:
                                    //         const Duration(milliseconds: 500),
                                    //   ),
                                    // );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/AGENDA/onboarding_34_fleure.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Boisson & Photos & Vidéos
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Musique
                            Column(
                              children: [
                                const Text(
                                  "Musique",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseMusique(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/AGENDA/noun-dj-10927.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Papeterie
                            Column(
                              children: [
                                const Text(
                                  "Papeterie",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepensePapeterie(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_popeterie.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Transport
                            Column(
                              children: [
                                const Text(
                                  "Transport",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseTransport(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_transport.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Gateau & Alliance & Beauté
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Alliance
                            Column(
                              children: [
                                const Text(
                                  "Alliance",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseAlliance(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                             "assets/icones/PRESTATAIRES/onboarding_24_mariage.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Draget
                            Column(
                              children: [
                                const Text(
                                  "Draget",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseDraguet(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_draget.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Cadeaux
                            Column(
                              children: [
                                const Text(
                                  "Cadeaux",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepsenseCadeau(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/PROFIL/menu_67_forfaits.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                        Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Gateau & Alliance & Beauté
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Gateau
                            Column(
                              children: [
                                const Text(
                                  "Gateau",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseGateau(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_gateau.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Animateur
                            Column(
                              children: [
                                const Text(
                                  "Animateur",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseAnimateur(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/PRESTATAIRES/noun-microphone-116.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Lune de miel
                            Column(
                              children: [
                                const Text(
                                  "Lune de miel",
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseLuneMiel(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/BUDGET/onboarding_20_lune_miel.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                        Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child:
                            //Gateau & Alliance & Beauté
                            Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            //Robe
                            Column(
                              children: [
                                const Text(
                                  "Robe",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseRobe(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/PRESTATAIRES/noun-wedding-dress-1855076.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Decoration
                            Column(
                              children: [
                                const Text(
                                  "Decoration",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseDecoration(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/PRESTATAIRES/onboarding_20_prestataire.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            //Divers
                            Column(
                              children: [
                                const Text(
                                  "Divers",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const DepenseDivers(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: _width / 4,
                                    height: _height / 6.5,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5.0),
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 25.0),
                                    decoration: BoxDecoration(
                                        //color: Colors.black12,
                                        borderRadius: BorderRadius.circular(35),
                                        border: Border.all(
                                            width: 2,
                                            color: Design.ColorMarie)),
                                    // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image(
                                          image: const AssetImage(
                                              "assets/icones/AGENDA/menu_41_theme.png"),
                                          width: _width / 6,
                                          height: _width / 6,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              AppWidgetsBoutons.boutonRetour(
                  colorbouton: Design.colorModuleGrey,
                  height: _height / 16,
                  width: _width / 1.6,
                  onPressed: () {
                    Navigator.pushNamed(context, '/AccueilMariee');
                  }),
            ],
          ),
        ));
  }
}
