import 'package:auto_size_text/auto_size_text.dart';
import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/depenses/facture/facture_liste.dart';
import 'package:budget/src/views/budget/drawer_budget.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class DepenseBoisson extends StatefulWidget {
  const DepenseBoisson({super.key});

  @override
  State<DepenseBoisson> createState() => _DepenseBoissonState();
}

class _DepenseBoissonState extends State<DepenseBoisson> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  int drawPos = 1;
  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      drawer: DrawerBudget(check: drawPos),
      appBar: AppBar(
        title: const Text(
          "Dépenses",
          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Colors.grey,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: AppWidgetsTitles.bar(),
          ),
          Image(
            image: const AssetImage(
                "assets/icones/BUDGET/onboarding_120_bouteilleVerreBoisson.png"),
            width: myWidth / 3.5,
          ),
          Column(
            children: [
              const Text(
                "Boisson",
                style: TextStyle(
                  fontSize: 20,
                  color: Design.ColorMarie,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: myHeigth / 50,
              ),
              InkWell(
                onTap: () {
                  showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)),
                            color: Design.ColorMarie),
                        height: 500,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              const Text(
                                "Partager Facture",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 30),
                              ),
                              Container(
                                height: 300,
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      InkWell(
                                        child: Row(children: [
                                          Container(
                                            margin: EdgeInsets.all(20),
                                            width: 94,
                                            height: 115,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(40),
                                                color: Colors.white),
                                            child: Image.asset(
                                                "assets/icones/COMITE/noun-user-3601335.png"),
                                          ),
                                          const Text(
                                            "President",
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.white),
                                          )
                                        ]),
                                      ),
                                      Row(children: [
                                        Container(
                                          margin: EdgeInsets.all(20),
                                          width: 94,
                                          height: 115,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(40),
                                              color: Colors.white),
                                          child: Image.asset(
                                              "assets/icones/COMITE/noun-user-3601335.png"),
                                        ),
                                        const Text(
                                          "President",
                                          style: TextStyle(
                                              fontSize: 25,
                                              color: Colors.white),
                                        )
                                      ]),
                                      Row(children: [
                                        Container(
                                          margin: EdgeInsets.all(20),
                                          width: 94,
                                          height: 115,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(40),
                                              color: Colors.white),
                                          child: Image.asset(
                                              "assets/icones/COMITE/noun-user-3601335.png"),
                                        ),
                                        const Text(
                                          "President",
                                          style: TextStyle(
                                              fontSize: 25,
                                              color: Colors.white),
                                        )
                                      ]),
                                      Row(children: [
                                        Container(
                                          margin: EdgeInsets.all(20),
                                          width: 94,
                                          height: 115,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(40),
                                              color: Colors.white),
                                          child: Image.asset(
                                              "assets/icones/COMITE/noun-user-3601335.png"),
                                        ),
                                        const Text(
                                          "President",
                                          style: TextStyle(
                                              fontSize: 25,
                                              color: Colors.white),
                                        )
                                      ]),
                                      Row(children: [
                                        Container(
                                          margin: EdgeInsets.all(20),
                                          width: 94,
                                          height: 115,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(40),
                                              color: Colors.white),
                                          child: Image.asset(
                                              "assets/icones/COMITE/noun-user-3601335.png"),
                                        ),
                                        const Text(
                                          "President",
                                          style: TextStyle(
                                              fontSize: 25,
                                              color: Colors.white),
                                        )
                                      ])
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
                child: Image(
                  image: const AssetImage(
                      "assets/icones/COMITE/noun-share-1076066.png"),
                  width: myWidth / 10.5,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  minimumSize: MaterialStateProperty.all(const Size(315, 57)),
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  side: MaterialStateProperty.all(const BorderSide(
                    color: Design.ColorMarie,
                  )),
                ),
                onPressed: () {
                  
                },
                child: const Text(
                  "Photos",
                  style: TextStyle(
                    fontSize: 20,
                    color: Design.ColorMarie,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  const AutoSizeText(
                    "Facture",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.ColorMarie,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        openDrawerWithSelectValue(4);

                        // Navigator.push(
                        //   context,
                        //   PageTransition(
                        //     child: const FactureListe(),
                        //     type: PageTransitionType.rightToLeft,
                        //     //childCurrent: widget,
                        //     duration: const Duration(milliseconds: 500),
                        //   ),
                        // );
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              bottom: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              left: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              right: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/PRESTATAIRES/noun-bills-45964.png",
                                width: myWidth / 1,
                                height: myHeigth / 1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              Column(
                children: [
                  const AutoSizeText(
                    "Avance",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.colorvert,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        openDrawerWithSelectValue(5);
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top:
                                  BorderSide(color: Design.colorvert, width: 3),
                              bottom:
                                  BorderSide(color: Design.colorvert, width: 3),
                              left:
                                  BorderSide(color: Design.colorvert, width: 3),
                              right:
                                  BorderSide(color: Design.colorvert, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(13),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/PRESTATAIRES/noun-money-1346.png",
                                width: myWidth / 1,
                                height: myHeigth / 1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              Column(
                children: [
                  const AutoSizeText(
                    "Solde",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        openDrawerWithSelectValue(6);
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey, width: 3),
                              bottom: BorderSide(color: Colors.grey, width: 3),
                              left: BorderSide(color: Colors.grey, width: 3),
                              right: BorderSide(color: Colors.grey, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/PRESTATAIRES/price-tag.png",
                                width: myWidth / 2,
                                height: myHeigth / 1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  })
                ],
              )
            ],
          ),
          AppWidgetsBoutons.boutonRetour(
            colorbouton: Design.ColorMarie,
            height: myHeigth / 16,
            width: myWidth / 1.6,
            onPressed: () {
              Navigator.pushNamed(context, '/AccueilMariee');
            },
          ),
        ],
      ),
    );
  }
}
