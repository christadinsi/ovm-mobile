import 'package:auto_size_text/auto_size_text.dart';
import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/depenses/bilan/bilan.dart';
import 'package:budget/src/views/budget/depenses/facture/facture_liste.dart';
import 'package:budget/src/views/budget/depenses/liste/%20list_depense.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class DepenseMariee extends StatefulWidget {
  const DepenseMariee({super.key});

  @override
  State<DepenseMariee> createState() => _DepenseMarieeState();
}

class _DepenseMarieeState extends State<DepenseMariee> {
  // final GlobalKey<ScaffoldState> _key = GlobalKey();
  // int drawPos = 1;

  // void openDrawerWithSelectValue(int selectedValue) {
  //   setState(() {
  //     drawPos = selectedValue;
  //   });
  //   _key.currentState?.openDrawer();
  // }

  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
    return Scaffold(
      // key: _key,
      // drawer: DrawerBudget(check: drawPos),
      appBar: AppBar(
        // actions: [
        //   IconButton(
        //     icon: Icon(
        //       Icons.dehaze,
        //       color: Colors.grey,
        //     ),
        //     onPressed: () {
        //       _key.currentState?.openDrawer();
        //     },
        //   )
        // ],
        title: const Text(
          "Dépenses",
          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Colors.grey,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: AppWidgetsTitles.bar(),
          ),
          const Text(
            "Contrôlez vos dépenses",
            style: TextStyle(
                fontSize: 25,
                color: Design.ColorMarie,
                fontWeight: FontWeight.bold),
          ),
          Image(
            image: const AssetImage(
                "assets/icones/BUDGET/onboardind_201_facture.png"),
            width: myWidth / 3.5,
          ),
          const Column(
            children: [
              Text(
                "Un mariage réussit",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "passe par une bonne gestion",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          SizedBox(
            height: myHeigth / 150,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  const AutoSizeText(
                    "Facture",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.ColorMarie,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                          Navigator.push(
                                      context,
                                      PageTransition(
                                        child: const FactureListe(),
                                        type: PageTransitionType.rightToLeft,
                                        //childCurrent: widget,
                                        duration:
                                            const Duration(milliseconds: 500),
                                      ),
                                    );
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              bottom: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              left: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              right: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/BUDGET/noun-bills-5029462.png",
                                width: myWidth * .15,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              Column(
                children: [
                  const AutoSizeText(
                    "Liste",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.colorBleu,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                         Navigator.push(
                          context,
                          PageTransition(
                            child: const ListDepense(),
                            type: PageTransitionType.rightToLeft,
                            //  childCurrent: widget,
                            duration: const Duration(milliseconds: 500),
                          ),
                        );
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top:
                                  BorderSide(color: Design.colorBleu, width: 3),
                              bottom:
                                  BorderSide(color: Design.colorBleu, width: 3),
                              left:
                                  BorderSide(color: Design.colorBleu, width: 3),
                              right:
                                  BorderSide(color: Design.colorBleu, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(13),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/BUDGET/noun-summary-5188666.png",
                                width: myWidth * .15,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              Column(
                children: [
                  const AutoSizeText(
                    "Bilan",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.rouge,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          PageTransition(
                            child: const BilanMariee(),
                            type: PageTransitionType.rightToLeft,
                            //childCurrent: widget,
                            duration:
                            const Duration(milliseconds: 500),
                          ),
                        );
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                  color: Design.colorMariee, width: 3),
                              bottom: BorderSide(
                                  color: Design.colorMariee, width: 3),
                              left: BorderSide(
                                  color: Design.colorMariee, width: 3),
                              right: BorderSide(
                                  color: Design.colorMariee, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/BUDGET/menu_44_bilan.png",
                                width: myWidth * .15,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  })
                ],
              )
            ],
          ),
          AppWidgetsBoutons.boutonRetour(
              colorbouton: Design.ColorMarie,
              height: myHeigth / 16,
              width: myWidth / 1.6,
              onPressed: () {
                Navigator.pushNamed(context, '/AccueilMariee');
              }),
        ],
      ),
    );
  }
}
