// import 'package:budget/components/btnnavigator_sheet.dart';
import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:budget/src/views/budget/drawer_budget.dart';
import 'package:flutter/material.dart';

class BilanMariee extends StatefulWidget {
  const BilanMariee({super.key});

  @override
  State<BilanMariee> createState() => _BilanMarieeState();
}

class _BilanMarieeState extends State<BilanMariee> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {

    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _key,
      drawer: DrawerBudget(check: drawPos),
      appBar: AppBar(
        title: const Text(
          "Budget",
          style: TextStyle(color: Color(0xFF707070), fontSize: 20),
        ),
        centerTitle: true,
        leading: const BackButton(
          color: Color(0xFF52912E),
        ),
         actions: [
           IconButton(
             icon: const Icon(
               Icons.dehaze,
              color: Colors.grey,
             ),
             onPressed: () {
               _key.currentState?.openDrawer();
             },
           )
         ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: AppWidgetsTitles.bar(),
          ),


          const Column(
            children: [
               Text(
                "Bilan",
                style: TextStyle(
                    fontSize: 40,
                    color: Design.rouge,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "Réussir votre cérémonie",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                "c'est aussi bien gérer vos finances",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  const Text(
                    "Cotisations",
                    style: TextStyle(
                        fontSize: 20,
                        color: Design.colorModuleGrey,
                        fontWeight: FontWeight.bold),
                  ),
                  Column(
                    children: [
                      Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(60)),
                        elevation: 6,
                        child:
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                                isScrollControlled: true,

                                backgroundColor: Colors.white,
                                context: context,
                                builder: (BuildContext context) {
                                  return Container(
                                    padding:const  EdgeInsets.all(20),
                                    height: myHeigth/1.2,
                                    width: myWidth,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Design.colorvert, width: 5),
                                      borderRadius: const  BorderRadius.only(topLeft:  Radius.circular(20), topRight:  Radius.circular(20)),
                                    ),
                                    child: Column(
                                      children: [
                                        Padding(padding: const EdgeInsets.only(bottom: 30),
                                          child: AppWidgetsTitles.titre(texte: 'Cotisations', colorText: Design.colorvert, size: 40),
                                        ),
                                        SingleChildScrollView(
                                          child: Column(
                                            children: [

                                              AppWidgetsBoutons.buttonBilanBudgetDetail(titre: 'Famille', prix: '800 000', icon: "assets/icones/BUDGET/profile_161_couple.png", width: myWidth, height: myHeigth, onPressed: (){}, color: Design.colorvert, colorPrix: Design.colorvert),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }
                            );
                          },
                          child:  Container(
                            width: myWidth / 3.5,
                            height: myHeigth / 5,
                            decoration:  BoxDecoration(
                              border: Border.all(color: Design.colorModuleGrey,width: 3),
                              color: Design.colorSecondTexte,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(50),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Center(
                                child: Image.asset(
                                  "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                                  width: myWidth/5,
                                  height: myWidth/5,
                                ),
                              ),
                            ),
                          ),
                        ),

                        /* Container(
                              width: myWidth / 3.5,
                              height: myHeigth / 5,
                              decoration:  BoxDecoration(
                                border: Border.all(color: Design.colorModuleGrey,width: 3),
                                color: Design.colorSecondTexte,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(50),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Center(
                                  child: Image.asset(
                                    "assets/icones/BUDGET/noun-dollar-money-bag-2717374.png",
                                    width: myWidth/5,
                                    height: myWidth/5,
                                  ),
                                ),
                              ),
                            ),*/
                      ),
                      const Text('4 000 000 FCFA',
                        style: TextStyle(
                            fontSize: 20,
                            color: Design.colorModuleGrey,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )
                ],
              ),

              Column(
                children: [
                  const Text(
                    "Factures",
                    style: TextStyle(
                        fontSize: 20,
                        color: Design.colorModuleGrey,
                        fontWeight: FontWeight.bold),
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                        onTap: () {

                          showModalBottomSheet(
                              isScrollControlled: true,

                              backgroundColor: Colors.white,
                              context: context,
                              builder: (BuildContext context) {
                                return Container(
                                  padding: const EdgeInsets.all(20),
                                  height: myHeigth/1.2,
                                  width: myWidth,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Design.ColorMarie, width: 5),
                                    borderRadius: const BorderRadius.only(topLeft:  Radius.circular(20), topRight:  Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(padding: const EdgeInsets.only(bottom: 30),
                                        child: AppWidgetsTitles.titre(texte: 'Factures', colorText: Design.ColorMarie, size: 40),
                                      ),
                                      SingleChildScrollView(
                                        child: Column(
                                          children: [

                                            AppWidgetsBoutons.buttonBilanBudgetDetail(titre: 'Salle de réception', prix: '800 000', icon: "assets/icones/BUDGET/profile_161_couple.png", width: myWidth, height: myHeigth, onPressed: (){}, color: Design.ColorMarie, colorPrix: Design.ColorMarie),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              }
                          );

                        },
                        child: Column(
                          children: [
                            Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(60)),
                              elevation: 6,
                              child: Container(
                                width: myWidth / 3.5,
                                height: myHeigth / 5,
                                decoration:  BoxDecoration(
                                  border: Border.all(color: Design.colorModuleGrey,width: 3),
                                  color: Design.colorSecondTexte,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(50),
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Center(
                                    child: Image.asset(
                                      "assets/icones/BUDGET/noun-bills-5029462.png",
                                      width: myWidth/5,
                                      height: myWidth/5,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const Text('500 000 FCFA',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Design.colorModuleGrey,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        )
                    );
                  }),
                ],
              ),

            ],
          ),
          Card(

            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40)),
            elevation: 6,
            child: Container(
              padding: const EdgeInsets.all(15),
              width: myWidth / 1.1,
              height: myHeigth / 7,
              decoration:  BoxDecoration(
                border: Border.all(color: Design.rouge,width: 3),
                color: Design.colorSecondTexte,
                borderRadius: const BorderRadius.all(
                  Radius.circular(40),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.asset(
                    "assets/icones/BUDGET/profile_19_budget.png",
                    width: myWidth/5,
                    height: myWidth/5,
                  ),
                  const Column(
                    children: [
                      Text('Disponible',
                        style: TextStyle(
                            fontSize: 25,
                            color: Design.rouge,
                            fontWeight: FontWeight.bold),),
                      Text('3 500 000 Fcfa',
                        style: TextStyle(
                            fontSize: 25,
                            color: Design.colorModuleGrey,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          AppWidgetsBoutons.boutonRetour(
              colorbouton: Design.rouge,
              height: myHeigth / 12,
              width: myWidth / 1.5,
              onPressed: () {
                Navigator.pushNamed(context, '/AccueilMariee');
              }),
        ],
      ),

    );
  }
}
