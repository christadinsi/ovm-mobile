import 'package:budget/components/depensecomponent.dart';
import 'package:budget/components/widgets-boutons.dart';
import 'package:budget/components/widgets-title.dart';
import 'package:budget/design/design.dart';
import 'package:flutter/material.dart';

class ListDepense extends StatelessWidget {
  const ListDepense({super.key});

  @override
  Widget build(BuildContext context) {
    final myWidth = MediaQuery.of(context).size.width;
    final myHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Dépenses',
          style: TextStyle(
            color: Design.colorModuleGrey,
            fontSize: 20,
            fontWeight: FontWeight.bold,

          ),
        ),
      ),
      body:Container(
         width: myWidth,
        height: myHeight,
        color: const Color(0xFFFFFFFF),
        child:  Column(
          children: [
             Container(
              margin: const EdgeInsets.only(left: 20),
              width: myWidth,
              height: myHeight / 50,
              // color: Colors.blue,
              child: AppWidgetsTitles.bar(),
            ),
            const SizedBox(
              height: 20,
            ),
              Container(
              width: myWidth,
              height: myHeight / 7,
              //  color: Colors.red,
              child: const Column(
                children: [
                  Text(
                    "Liste des dépenses",
                    style: TextStyle(
                        color: Design.colorBleu,
                        fontSize: 27,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Total : 6 500 000 Fcfa",
                    style: TextStyle(
                        color: Design.colorMariee, fontSize: 20),
                  )
                ],
              ),
            ),
            Container(
               width: myWidth,
              height: 370,
              color: const Color(0xFFFFFFFF),
              child: ListView(
                children: [
                  depensesComponent(
                    title: 'Salle des fêtes',
                    colorText: const Color(0xFF7C6BD7),
                    width: myWidth,
                    height: myHeight,
                    // imageWidth: 40,
                    // imageheight: 40,
                    image: 'assets/icones/PRESTATAIRES/noun-hall-4870131.png',
                    montant: 300000 ,

                  ),
                   depensesComponent(
                    title: 'Salle des fêtes',
                    colorText: const Color(0xFF7C6BD7),
                    width: myWidth,
                    height: myHeight,
                    // imageWidth: 40,
                    // imageheight: 40,
                    image: 'assets/icones/PRESTATAIRES/noun-hall-4870131.png',
                    montant: 300000 ,

                  ),
                   depensesComponent(
                    title: 'Salle des fêtes',
                    colorText: const Color(0xFF7C6BD7),
                    width: myWidth,
                    height: myHeight,
                    // imageWidth: 40,
                    // imageheight: 40,
                    image: 'assets/icones/PRESTATAIRES/noun-hall-4870131.png',
                    montant: 300000 ,

                  ),
                   depensesComponent(
                    title: 'Salle des fêtes',
                    colorText: const Color(0xFF7C6BD7),
                    width: myWidth,
                    height: myHeight,
                    // imageWidth: 40,
                    // imageheight: 40,
                    image: 'assets/icones/PRESTATAIRES/noun-hall-4870131.png',
                    montant: 300000 ,

                  ),
                ],
              ),

            ),
            Expanded(
              child: Container(
                  // color: Colors.green,
                  width: myWidth,
                  height: myHeight / 3,
                  child: AppWidgetsBoutons.boutonRetour(
                      colorbouton: Color(0xFF5F4591),   height: myHeight/16, width:  myWidth/1.6, onPressed: () {
              Navigator.pushNamed(context, '/AccueilMariee');
                  }),
                ),
            ),
          ],
          
        ),
        
      ),
    );
  }
}