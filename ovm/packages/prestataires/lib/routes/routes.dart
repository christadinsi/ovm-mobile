// ignore_for_file: implementation_imports
import 'package:flutter/material.dart';
import 'package:prestataires/prestataires.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/beaute/institut_beaute.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/beaute/liste_vetement.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/categories.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/decoration/liste_fleuriste.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/papeterie/liste_papeterie.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/acceuil_materiel.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/location_materiels/location_materiel.dart/acceuil_location.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/reception.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/salles/liste_salle.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/traiteurs/acceuil_service_traiteur.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/traiteurs/service_traiteur.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/traiteurs/service_traiteur_chef.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/decoration_fav/decorateur_fav/decorateur_favori.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/decoration_fav/decoration_fav.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/favoris.dart';

class AppRouterPrestataires {
  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/PrestatairesMariee':
        return MaterialPageRoute(
            builder: (context) => const PrestatairesMariee());
      case '/AccueilMateriel':
        return MaterialPageRoute(builder: (context) => const AccueilMateriel());
      case '/AcceuilLoaction':
        return MaterialPageRoute(builder: (context) => const AcceuilLoaction());
      case '/PrestatairesMariee':
        return MaterialPageRoute(
            builder: (context) => const PrestatairesMariee());
      case '/CategoriesMariee':
        return MaterialPageRoute(
            builder: (context) => const CategoriesMariee());
      case '/FavorisMariee':
        return MaterialPageRoute(builder: (context) => const FavorisMariee());
      case '/ListeDesSalles':
        return MaterialPageRoute(builder: (context) => const ListeDesSalles());
      case '/ListeVetements':
        return MaterialPageRoute(builder: (context) => const ListeVetements());
      case '/InstitutBeaute':
        return MaterialPageRoute(builder: (context) => const InstitutBeaute());
      case '/ListeFleuriste':
        return MaterialPageRoute(builder: (context) => const ListeFleuristes());
      case '/ListeDj':
        return MaterialPageRoute(builder: (context) => const ListeFleuristes());
      case '/ListeDj':
        return MaterialPageRoute(builder: (context) => const ListePapeterie());
      default:
        return null;
    }
  }
}
