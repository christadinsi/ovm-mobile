import 'package:flutter/material.dart';
import 'package:prestataires/design/design.dart';

class InputGlobal  {

  static TextField input({
    //required String hintText,
    required String labelText,
    required bool autofocus,
    required dynamic controller,
    required dynamic keyboardType,
  }) {
    return TextField(
      style: const TextStyle(
          fontSize: 16,
          fontFamily: 'Lato',
          fontWeight: FontWeight.bold,
          color:Colors.black),
      keyboardType: keyboardType,
      controller: controller,

      autofocus: autofocus,
      decoration: InputDecoration(
        hintStyle: TextStyle(color: Design.colorSecondTexte),
        labelStyle: TextStyle(
          color:Design.colorSecondTexte,

        ),
        //hintText: hintText,
        contentPadding: const EdgeInsets.symmetric(horizontal: 30),
        labelText: labelText,
        filled: true,

        fillColor: const Color.fromRGBO(225, 235, 245, 0.5),
        border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10)),
      ),
    );
  }

  static TextField inputText({
    required String labelText,
    required dynamic controller,
  }) {
    return TextField(
      maxLines: 5,
      decoration: InputDecoration(
        hintStyle: TextStyle(color: Design.colorSecondTexte),
        labelStyle: const TextStyle(
          color: Colors.black,

        ),
        //hintText: hintText,
        contentPadding: const EdgeInsets.symmetric(horizontal: 20 ,vertical: 20),
        labelText: labelText,
        filled: true,

        fillColor: const Color.fromRGBO(225, 235, 245, 0.5),
        border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10)),
      ),
      controller: controller,
    );
  }

  static TextField inputPass({
    //required String hintText,
    required String labelText,
    required dynamic controller,
    required dynamic keyboardType,
  }) {
    return TextField(
        style: const TextStyle(
            fontSize: 16,
            fontFamily: 'Lato',
            fontWeight: FontWeight.bold,
            color: Colors.black),
        keyboardType: keyboardType,
        controller: controller,
        obscureText: true,
        obscuringCharacter: "*",
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Design.colorSecondTexte),
          labelStyle: TextStyle(
            color: Design.colorSecondTexte,
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 30),
          labelText: labelText,
          filled: true,

          fillColor: const Color.fromRGBO(225, 235, 245, 0.5),
          border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(10)),
        ));
  }
}