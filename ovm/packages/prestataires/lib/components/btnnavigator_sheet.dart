import 'package:flutter/material.dart';

class MycomposantBtnSheet {
  String title;
  String subtitle;

  // final  favoris = Image.asset('assets/icones/PROFIL/noun-heart-100.png');
  Widget? favoris;
  String information;
  String tel;
  String addresse;
  String prestation;
  String ContenuDeLaPrestation;

  MycomposantBtnSheet(
      {required this.title,
      required this.subtitle,
      required this.information,
      required this.tel,
      required this.addresse,
      required this.prestation,
      required this.ContenuDeLaPrestation,
       this.favoris});

  // btn sheet
  btnSheet(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
   final double screenWidth = MediaQuery.of(context).size.width;
    double fontSizeText1 =
        screenWidth * 0.054; 
    return showModalBottomSheet(
      isScrollControlled: true,
    
        backgroundColor: const Color(0xFF7C6BD7),
        context: context,
        builder: (BuildContext context) {
          return Container(
            // width: 375,
            height:screenHeight*.7,
            decoration: BoxDecoration(
                color: const Color(0xFF7C6BD7),
                border: Border.all(color: const Color(0xFF7C6BD7)),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Flexible(
                        flex: 10,
                        child: Container(
                            width: 43,
                            height: 37,
                           
                            child: InkWell(
                              child: favoris,
                              onTap: () {},
                              
                            )
                            ),
                      ),
                      // const SizedBox(
                      //   width: 90,
                      // ),
                      Flexible(
                        flex: 90,
                        child: Center(
                          child: Container(
                            width: 200,
                           
                            child: Text(
                              title,
                              textAlign: TextAlign.center,
                              style:  TextStyle(
                                  color:const Color(0xFF9599B3),
                                  // fontSize: 30,
                                  fontSize: fontSizeText1,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                // fin de la premiere ligne
                // debut du sous titre
                SizedBox(
                  height: 20,
                ),
                Column(
                  children: [
                    Text(
                      subtitle,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width*.625,
                      height:MediaQuery.of(context).size.height*.07 ,
                      child: ElevatedButton(
                          onPressed: () {
                            final linkFb = Uri.parse(
                                'https://www.facebook.com/ComplexeHotelierJMALes9Provinces');
                          },
                          child: const Text(
                            "Page facebook",
                            style: TextStyle(
                                color: Color(0xFF7C6BD7),
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          )),
                    )
                  ],

                  // fin du deuxieme bloc

                  // debut du troisieme bloc
                ),
                // fin du deuxieme bloc

                // debut du troisieme bloc
                SizedBox(height: 30,),
                Container(
                  padding: const EdgeInsets.only(left: 15, top: 10),
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        information,
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        'tel: ' + tel,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Adresse : ' + addresse,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
                // fin du troisieme bloc
                const SizedBox(
                  height: 5,
                ),
                // debut du dernier bloc
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.only(left: 15, top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        prestation,
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        ContenuDeLaPrestation,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}
