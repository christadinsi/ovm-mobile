import 'package:flutter/material.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/categories.dart';
import 'package:page_transition/page_transition.dart';

class DrawerPrestataires extends StatelessWidget {
  final int check;

  const DrawerPrestataires({Key? key, required this.check}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    if (check == 6) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: const Color.fromARGB(255, 127, 129, 148),
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/price-tag.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Solde facture",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Salle des fêtes",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Désignations',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'KARE F',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Tel : 062 22 22 34',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Détails',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Paiement effectué',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            '1 100 000Fcfa',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Reste à payer',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Date : 05/08/2023',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            height: height / 60,
                          ),
                          const Text(
                            '0 Fcfa',
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ),
      );
    } else if (check == 5) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Colors.green,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/noun-money-1346.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Avance",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Salle des fêtes",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Désignations',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'KARE F',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Tel : 062 22 22 34',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Détails',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Paiement effectué',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            '1 400 000 Fcfa',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Reste à payer',
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Date : 05/08/2023',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            height: height / 60,
                          ),
                          const Text(
                            '1 100 000 Fcfa',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ),
      );
    } else if (check == 4) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Design.ColorMarie,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/noun-bills-45964.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Facture",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Salle des fêtes",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Désignations',
                            style: TextStyle(
                                color: Design.ColorMarie,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'KARE F',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Tel : 062 22 22 34',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Détails',
                            style: TextStyle(
                                color: Design.ColorMarie,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Location de la salle',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          const Text(
                            'Service traiteur',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            width: width / 1.4,
                            child: const Divider(
                              thickness: 1,
                              color: Design.colorModuleGrey,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Montant à payer',
                            style: TextStyle(
                                color: Design.ColorMarie,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          const Text(
                            'Date : 05/08/2023',
                            style: TextStyle(
                                color: Design.colorModuleGrey, fontSize: 18),
                          ),
                          SizedBox(
                            height: height / 60,
                          ),
                          const Text(
                            '2 500 000 Fcfa',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ))
            ],
          ),
        ),
      );
    } else if (check == 3) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Design.colorvert,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/noun-heart-5271870.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Contacts favoris",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Vos prestataires",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                  height: height / 1.7,
                  padding: const EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Image(
                              image: const AssetImage(
                                  'assets/icones/PRESTATAIRES/noun-ring-1514513.png'),
                              width: width / 5,
                              height: width / 5,
                            ),
                            Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  'Bijoutier',
                                  style: TextStyle(
                                      color: Design.colorvert, fontSize: 22),
                                ),
                                SizedBox(
                                  width: width / 1.8,
                                  child: const Divider(
                                    thickness: 1,
                                    color: Design.colorModuleGrey,
                                  ),
                                ),
                                const Text(
                                  'Bijouterie merveille',
                                  style: TextStyle(
                                      color: Design.colorModuleGrey,
                                      fontSize: 18),
                                ),
                                const Text(
                                  'Tel : 074 10 25 28',
                                  style: TextStyle(
                                      color: Design.colorModuleGrey,
                                      fontSize: 18),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      );
    } else if (check == 2) {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Design.colorMariee,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/PRESTATAIRES/noun-heart-100.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Mes favoris",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Prestataires sélectionnés",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              Container(
                height: height / 1.7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //Catégories
                    Column(
                      children: [
                        const Text(
                          "Catégories",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.ColorMarie),
                        ),
                        InkWell(
                          child: Container(
                            width: width / 4,
                            height: height / 6,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 25.0),
                            decoration: BoxDecoration(
                                //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2, color: Design.ColorMarie)),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: const AssetImage(
                                      "assets/icones/PRESTATAIRES/noun-tasks-1123013.png"),
                                  width: width / 6,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const CategoriesMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),
                      ],
                    ),

                    //Contacts
                    Column(
                      children: [
                        const Text(
                          "Contacts",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorvert),
                        ),
                        InkWell(
                          child: Container(
                            width: width / 4,
                            height: height / 6,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 25.0),
                            decoration: BoxDecoration(
                                //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2, color: Design.colorvert)),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: const AssetImage(
                                      "assets/icones/PRESTATAIRES/noun-team-skills-2078803.png"),
                                  width: width / 5,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    } else {
      return SizedBox(
        height: height / 1.1,
        child: Drawer(
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(90),
                  bottomRight: Radius.circular(90))),
          backgroundColor: Colors.white,
          width: width / 1.12,
          elevation: 1,
          child: Column(
            children: [
              Container(
                color: Design.ColorMarie,
                width: width / 1.1,
                height: height / 3.6,
                padding: EdgeInsets.only(left: width / 12),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/icones/TABLES/noun-butler-48550.png",
                      height: height / 15,
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    const Text(
                      "Prestataires",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 27,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text(
                      "Ils sont tous disponibles",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 20,
                          color: Colors.white38,
                          fontWeight: FontWeight.w200),
                    ),
                  ],
                ),
              ),
              SizedBox(
                // margin: const EdgeInsetsDirectional.only(top: 25, start: 25, end: 25, bottom: 5),

                height: height / 1.7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //Favoris
                    Column(
                      children: [
                        const Text(
                          "Favoris",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorModuleGrey),
                        ),
                        InkWell(
                          onTap: () {},
                          child: Container(
                            width: width / 4,
                            height: height / 6,
                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 25.0),
                            decoration: BoxDecoration(
                                //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2, color: Design.colorModuleGrey)),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: const AssetImage(
                                      "assets/icones/PRESTATAIRES/Plan de travail 1.png"),
                                  width: width / 6,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),

                    //Contacts
                    Column(
                      children: [
                        const Text(
                          "Contacts",
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.colorvert),
                        ),
                        InkWell(
                          child: Container(
                            width: width / 4,
                            height: height / 6,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 25.0),
                            decoration: BoxDecoration(
                                //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2, color: Design.colorvert)),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: const AssetImage(
                                      "assets/icones/PRESTATAIRES/noun-team-skills-2078803.png"),
                                  width: width / 5,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }
  }

  Future<void> _formulaire_trois_mois(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          icon: new Image.asset(
            "assets/icones/PROFIL/product_detail_62_airtelMoney.png",
            height: 100,
          ),
          // icon: AssetImage("assets/icones/PROFIL/product_detail_62_airtelMoney.png"),
          title: const Text("Mobile money",
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  color: Design.colorBleu),
              textAlign: TextAlign.center),
          content: Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            //height: 150,
            child: const Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //AppWidgets.inputText(labelText: "Desciption", controller: appartDescriptionController),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                  style: TextButton.styleFrom(
                    textStyle: Theme.of(context).textTheme.labelLarge,
                  ),
                  child: const Text('Retour'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Enregistrer'),
                    onPressed: () {}),
              ],
            ),
          ],
        );
      },
    );
  }
}
