import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/categories.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/acceuil_materiel.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/favoris.dart';

class PrestatairesMariee extends StatefulWidget {
  const PrestatairesMariee({super.key});

  @override
  State<PrestatairesMariee> createState() => _PrestatairesMarieeState();
}

class _PrestatairesMarieeState extends State<PrestatairesMariee> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 3;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      appBar: AppBar(
        leading: const BackButton(
          color: Design.ColorMarie,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: _height / 1.8,
                child: Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(top: 50),
                      height: _height / 1.8,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/marieeProfile39.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: AppWidgetsTitles.titre(
                            texte: "Prestataires",
                            colorText: (Design.ColorMarie),
                            size: 35),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 25),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: [0.15, 0.50],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          //tileMode: TileMode.mirror,
                          colors: <Color>[
                            Colors.white,
                            Colors.white70.withOpacity(0.0)
                          ],
                        ),
                      ),
                      child: const Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          "Trouvez tous vos prestataires",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  height: _height / 2.5,
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //Catégories
                          Column(
                            children: [
                              const Text(
                                "Catégories",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.ColorMarie),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.ColorMarie)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/PRESTATAIRES/noun-tasks-1123013.png"),
                                        width: _width / 6,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const CategoriesMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),

                          //Favoris
                          Column(
                            children: [
                              const Text(
                                "Favoris",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorModuleGrey),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: const FavorisMariee(),
                                      type: PageTransitionType.rightToLeft,
                                      //childCurrent: widget,
                                      duration:
                                          const Duration(milliseconds: 500),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2,
                                          color: Design.colorModuleGrey)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/PRESTATAIRES/Plan de travail 1.png"),
                                        width: _width / 6,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          //Contacts
                          Column(
                            children: [
                              const Text(
                                "Contacts",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.colorvert),
                              ),
                              InkWell(
                                child: Container(
                                  width: _width / 4,
                                  height: _height / 6,

                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 25.0),
                                  decoration: BoxDecoration(
                                      //color: Colors.black12,
                                      borderRadius: BorderRadius.circular(35),
                                      border: Border.all(
                                          width: 2, color: Design.colorvert)),
                                  // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                        image: const AssetImage(
                                            "assets/icones/PRESTATAIRES/noun-team-skills-2078803.png"),
                                        width: _width / 5,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  openDrawerWithSelectValue(3);
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                      AppWidgetsBoutons.boutonRetour(
                          colorbouton: Design.ColorMarie,
                          height: _height / 16,
                          width: _width / 1.6,
                          onPressed: () {
                            Navigator.pushNamed(context, '/AccueilMariee');
                          }),
                    ],
                  ))
            ],
          )
        ],
      ),
      drawer: DrawerPrestataires(check: drawPos),
    );
  }
}
