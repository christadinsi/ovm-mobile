import 'package:flutter/material.dart';
import 'package:prestataires/components/btnnavigator_sheet.dart';
import 'package:prestataires/components/widgets-boutons.dart';
// import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';

class MaitreCeremonie extends StatefulWidget {
  const MaitreCeremonie({super.key});

  @override
  State<MaitreCeremonie> createState() => _MaitreCeremonieState();
}

class _MaitreCeremonieState extends State<MaitreCeremonie> {
   final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }
  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    Widget materiel = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: _width * .28,
        height: _height * .17,
        decoration: BoxDecoration(
          color: const Color(0xFF9599B3),
          borderRadius: BorderRadius.circular(40),
          boxShadow: const [
            BoxShadow(
                blurRadius: 6,
                color: Color(0xFF00000029),
                spreadRadius: 3,
                blurStyle: BlurStyle.inner)
          ],
        ),
        child: InkWell(
          onTap: () {
            MycomposantBtnSheet btn = MycomposantBtnSheet(
                ContenuDeLaPrestation:
                    'Photo et videos ',
                title: 'KAT PROD Événementiel ',
                // favoris: Icons.favorite,
               favoris: Image.asset('assets/icones/PRESTATAIRES/noun-heart-100.png'),
                subtitle: 'N Images',
                information: 'Informations',
                tel: '066 01 37 37',
                addresse: 'Adresse : Libreville, GABON',
                prestation: 'Prestation');
            btn.btnSheet(context);
          },
          child: Container(
            padding: const EdgeInsets.all(20),
            width: 60,
            height: 70,
            child: Image.asset(
                "assets/icones/PRESTATAIRES/noun-microphone-116.png"),
          ),
        ),
      ),
    );

    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text(
          "Prestataires",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
        actions: [
          IconButton(onPressed: () {
              _key.currentState?.openDrawer();
          }, icon: const Icon(Icons.dehaze_sharp))
        ],
        leading: const BackButton(
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        forceMaterialTransparency: true,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Flexible(
                  flex: 40,
                  child: Stack(children: [
                    Container(
                      width: _width * 1.81,
                      height: _height * .42,
                      // color: Colors.amber,
                      child: Image.asset(
                        "assets/images/lamariee/marieProfile109.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      bottom: -20,
                      // left: _width*.09,
                      // right:_width*.09,
                      child: Container(
                        width: _width,
                        height: _height * .19,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          gradient: LinearGradient(
                            stops: const [0.15, 0.99],
                            begin: FractionalOffset.bottomCenter,
                            end: FractionalOffset.topCenter,
                            //tileMode: TileMode.mirror,
                            colors: <Color>[
                              Colors.white,
                              Colors.white70.withOpacity(0.0)
                            ],
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Maître de cérémonie",
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Color(0xFFD47FA6),
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 2, right: 1),
                              child: Container(
                                  // margin: EdgeInsets.only(left: _width*.03,right: _width*.02),
                                  child: Text(
                                "Il fera vivre votre mariage",
                                style: TextStyle(
                                    fontSize: 20,
                                    color:
                                        const Color(0xFF000000).withOpacity(.6),
                                    fontWeight: FontWeight.bold),
                              )),
                            ),
                          //  const SizedBox(
                          //     height: 4,
                          //   ),
                          ],
                        ),
                      ),
                    ),
                  ])),
              Flexible(
                  flex: 40,
                  child: ListView.builder(
                     padding: const EdgeInsets.only(top: 0), 
                    itemCount: 6, // Vous avez six lignes de salles
                    itemBuilder: (BuildContext context, int index) {
                      return 
                          Row(
                            mainAxisAlignment:
                                MainAxisAlignment.spaceAround,
                            children: List.generate(
                              3, // Trois colonnes par ligne
                              (int columnIndex) {
                                final materielNumber =
                                    index * 3 + columnIndex + 1;
                                return Column(
                                  children: [
                                    materiel,
                                    Text(
                                      "Mc $materielNumber",
                                      style: const TextStyle(
                                        color: Color(0xFF7C6BD7),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          );}
                  )),
             const   SizedBox(
                height: 10,
              ),
              Flexible(
                  flex: 10,
                  child: Container(
                    width: _width * 1.81,
                    height: _height * .42,
                    child: AppWidgetsBoutons.boutonRetour(
                        colorbouton: Design.ColorMarie,
                        height: _height / 16,
                        width: _width / 1.6,
                        onPressed: () {
                          Navigator.pushNamed(context, '/AccueilMariee');
                        }),
                  )),
            ],
          ),
        ],
      ),
      drawer: DrawerPrestataires(check: drawPos),

    );
  }
}
