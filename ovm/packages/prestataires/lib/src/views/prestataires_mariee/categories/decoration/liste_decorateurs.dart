import 'dart:io';

import 'package:flutter/material.dart';
import 'package:prestataires/components/btnnavigator_sheet.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';

class ListeDecorateurs extends StatefulWidget {
  const ListeDecorateurs({super.key});

  @override
  State<ListeDecorateurs> createState() => _ListeDesSallesState();
}

class _ListeDesSallesState extends State<ListeDecorateurs> {
   final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }
  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    Widget salle = Container(
      width: _width * .30,
      height: _height * .19,
      decoration: BoxDecoration(
        color: const Color(0xFF9599B3),
        borderRadius: BorderRadius.circular(40),
        boxShadow: const [
          BoxShadow(
              blurRadius: 6,
              color: Color(0xFF00000029),
              spreadRadius: 3,
              blurStyle: BlurStyle.inner)
        ],
      ),
      child: InkWell(
        onTap: () {
          MycomposantBtnSheet btn = MycomposantBtnSheet(
              ContenuDeLaPrestation:
                  'Décoration de mariage',
              title: 'Décoration 1',
              // favoris: Image.asset('name'),
              // favoris: Image.asset('assets/icones/PRESTATAIRES/noun-heart-100.png'),
              subtitle: 'Bk service',
              information: 'Informations',
              tel: '062 22 22 34',
              addresse: 'Libreville Gabon',
              prestation: 'Prestation');
          btn.btnSheet(context);
        },
        child: Container(
          padding: const EdgeInsets.all(20),
          width: 64,
          height: 74,
          child:
              Image.asset("assets/icones/PRESTATAIRES/onboarding_20_prestataire.png"),
        ),
      ),
    );

    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text(
          "Prestataires",
          style: TextStyle(fontSize: 20, color: Color(0xFF352641),fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(onPressed: () {
              _key.currentState?.openDrawer();
          }, icon: const Icon(Icons.dehaze_sharp,color: Colors.white,))
        ],
        leading: const BackButton(
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Flexible(
                  flex: 40,
                  child: Stack(children: [
                    Container(
                      width: _width * 1.81,
                      height: _height * .42,
                      // color: Colors.amber,
                      child: Image.asset(
                        "assets/images/lamariee/marieProfile43.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      bottom: -2,
                      // left: _width*.09,
                      // right:_width*.09,
                      child: Container(
                        width: _width,
                        height: _height * .12,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          gradient: LinearGradient(
                              stops: [0.15, 0.50],
                              begin: FractionalOffset.topCenter,
                              end: FractionalOffset.bottomCenter,
                              colors: [
                                const Color(0xFFFFFFFF).withOpacity(.2),
                                const Color(0xFFFFFFFF),
                              ]),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Décoration",
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Color(0xFFD47FA6),
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 2, right: 1),
                              child: Container(
                                  // margin: EdgeInsets.only(left: _width*.03,right: _width*.02),
                                  child: Text(
                                "Il va sublimer votre mariage ",
                                style: TextStyle(
                                    fontSize: 20,
                                    color:
                                        const Color(0xFF000000).withOpacity(.6),
                                    fontWeight: FontWeight.bold),
                              )),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ])),
              Flexible(
                  flex: 50,
                  child: Container(
                    width: _width * 1.81,
                    height: _height * .5,
                    // color: Colors.blue,
                    child: ListView(
                     padding: const EdgeInsets.only(top: 0),
                      children: [
                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 1",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 2",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 3",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),
                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 4",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 5",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 6",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 7",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 8",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 9",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 10",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 11",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 12",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 13",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 14",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 15",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 16",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 17",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Déco 18",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                      ],
                    ),
                  )),
              const SizedBox(
                height: 10,
              ),
              Flexible(
                  flex: 10,
                  child: Container(
                    width: _width * 1.81,
                    height: _height * .42,
                    child: AppWidgetsBoutons.boutonRetour(
                        colorbouton: Design.ColorMarie,
                        height: _height / 16,
                        width: _width / 1.6,
                        onPressed: () {
                          Navigator.pushNamed(context, '/AccueilMariee');
                        }),
                  )),
            ],
          ),
        ],
      ),
      drawer: DrawerPrestataires(check: drawPos),

    );
  }
}
