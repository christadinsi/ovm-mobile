import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/alliances/alliances.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/beaute/beaute.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/ceremonie/ceremonie.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/decoration/decoration.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/divers/divers.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/musique/musique.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/papeterie/papeterie.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/photo/photo.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/reception.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/transport/transport.dart';


class CategoriesMariee extends StatelessWidget {
  const CategoriesMariee({super.key});

  @override
  Widget build(BuildContext context) {

    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return  Scaffold(

      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,

        title: const Text(
          'Prestataires',
          style: TextStyle(fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey
          ),
        ),

      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        //height: _height,
        //width: _width,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,

          children: [
            AppWidgetsTitles.bar(),
            AppWidgetsTitles.titre(texte: "Consulter catégories", colorText: (Design.ColorMarie), size: 25),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              height: _height/1.4,
              child: SingleChildScrollView(
                child: Column(
                  children: [

                    //Reception & Beauté
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                      //Reception
                      InkWell(
                        child:
                        Container(
                          width: _width/3,
                          height: _height/4.5,

                          margin: const EdgeInsets.symmetric(vertical: 5.0),
                          padding: const EdgeInsets.symmetric(vertical: 15.0),
                          decoration: BoxDecoration(
                            //color: Colors.black12,
                              borderRadius: BorderRadius.circular(35),
                              border: Border.all(
                                  width: 2,
                                  color: Design.ColorMarie
                              )
                          ),
                          // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                          child:   Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Text("Réception",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.ColorMarie
                                ),),
                              Image(
                                image: const AssetImage("assets/icones/PRESTATAIRES/noun-party-3203411.png"),
                                width: _width/4,
                              ),

                            ],
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            PageTransition(
                              child: const ReceptionMariee(),
                              type: PageTransitionType.rightToLeft,
                              //childCurrent: widget,
                              duration: const Duration(milliseconds: 500),
                            ),
                          );

                        },
                      ),

                      //Beauté
                      InkWell(
                        child:
                        Container(
                          width: _width/3,
                          height: _height/4.5,

                          margin: const EdgeInsets.symmetric(vertical: 5.0),
                          padding: const EdgeInsets.symmetric(vertical: 15.0),
                          decoration: BoxDecoration(
                            //color: Colors.black12,
                              borderRadius: BorderRadius.circular(35),
                              border: Border.all(
                                  width: 2,
                                  color: Design.ColorMarie
                              )
                          ),
                          // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                          child:   Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Text("Beauté",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold,
                                    color: Design.ColorMarie
                                ),),
                              Image(
                                image: const AssetImage("assets/icones/PRESTATAIRES/noun-mirror-4675515.png"),
                                width: _width/5,
                              ),

                            ],
                          ),
                        ),
                        onTap: () {

                          Navigator.push(
                            context,
                            PageTransition(
                              child: const BeauteMariee(),
                              type: PageTransitionType.rightToLeft,
                              //childCurrent: widget,
                              duration: const Duration(milliseconds: 500),
                            ),
                          );
                        },
                      ),
                    ],),

                    //Deco & musik
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        //Décoration
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Décoration",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-wedding-arch-3481819.png"),
                                  width: _width/4,
                                  height: _width/4,
                                ),

                              ],
                            ),
                          ),
                          onTap: () {

                            Navigator.push(
                              context,
                              PageTransition(
                                child: const DecorationMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),

                        //Musique
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Musique",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-music-111.png"),
                                  width: _width/5,
                                  height: _width/5,
                                ),

                              ],
                            ),
                          ),
                          onTap: () {

                            Navigator.push(
                              context,
                              PageTransition(
                                child: const MusiqueMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );

                          },
                        ),
                      ],),

                    //Photo & Cérémonie
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        //Photo
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Photo",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-pictures-1412356.png"),
                                  width: _width/4,
                                  height: _width/4,
                                ),


                              ],
                            ),
                          ),
                          onTap: () {

                            Navigator.push(
                              context,
                              PageTransition(
                                child: const PhotoMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),

                        //Cérémonie
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Cérémonie",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-microphone-116.png"),
                                  width: _width/5,
                                ),

                              ],
                            ),
                          ),
                          onTap: () {

                            Navigator.push(
                              context,
                              PageTransition(
                                child: const CeremonieMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );

                          },
                        ),
                      ],),

                    //Papeterie & Alliances
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        //Papeterie
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Papeterie",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/onboarding_34_papeterie.png"),
                                  width: _width/4,
                                  height: _width/4,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const PapeterieMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),

                        //Alliances
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Alliances",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-ring-1514513.png"),
                                  width: _width/4,
                                  height: _width/4,
                                ),

                              ],
                            ),
                          ),
                          onTap: () {

                            Navigator.push(
                              context,
                              PageTransition(
                                child: const AlliancesMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),
                      ],),

                    //Transport & Divers
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [

                        //Transport
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Transport",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-honeymoon-6395.png"),
                                  width: _width/4,
                                  height: _width/4,
                                ),

                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const TransportMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),

                        //Divers
                        InkWell(
                          child:
                          Container(
                            width: _width/3,
                            height: _height/4.5,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            decoration: BoxDecoration(
                              //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2,
                                    color: Design.ColorMarie
                                )
                            ),
                            // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                            child:   Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text("Divers",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.bold,
                                      color: Design.ColorMarie
                                  ),),
                                Image(
                                  image: const AssetImage("assets/icones/PRESTATAIRES/noun-accessories-4178633.png"),
                                  width: _width/4,
                                  height: _width/4,
                                ),

                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const DiversMariee(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),
                      ],),

                  ],
                ),
              ),
            ),
            AppWidgetsBoutons.boutonRetour( colorbouton: Design.ColorMarie, height: _height/16, width:  _width/1.6, onPressed: () {Navigator.pushNamed(context, '/AccueilMariee');}),

          ],

        ),
      )

    );
  }
}
