// ignore_for_file: sized_box_for_whitespace, use_full_hex_values_for_flutter_colors, non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:prestataires/components/btnnavigator_sheet.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';

class InterPatissiers extends StatefulWidget {
  const InterPatissiers({super.key});

  @override
  State<InterPatissiers> createState() => _InterPatissiersState();
}

class _InterPatissiersState extends State<InterPatissiers> {
   final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text(
          "Prestataires",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
         actions: <Widget>[
          //IconButton
          IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            //color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          //IconButton
        ],
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: myHeigth / 2.1,
                child: Stack(
                  children: [
                    Container(
                      height: myHeigth / 2.4,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/marieProfile63.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 40),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: const [0.15, 0.45],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          colors: <Color>[
                            Colors.white,
                            Colors.white70.withOpacity(0.0)
                          ],
                        ),
                      ),
                      child: const Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Pâtissiers",
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Design.colorMariee,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Il réalisera le gâteau de mariage",
                              style:
                                  TextStyle(fontSize: 20, color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  height: myHeigth / 2,
                  margin: const EdgeInsets.only(left: 5, right: 5),
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                          flex: 40,
                          child: ListView.builder(
                              padding: const EdgeInsets.only(top: 0),
                              itemCount: 12, // Vous avez six lignes de salles
                              itemBuilder: (BuildContext context, int index) {
                                return Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: List.generate(
                                    3, // Trois colonnes par ligne
                                    (int columnIndex) {
                                      final PatissierNumber =
                                          index * 3 + columnIndex + 1;
                                      return Column(
                                        children: [
                                          Container(
                                            width: myWidth * .26,
                                            height: myHeigth * .16,
                                            decoration: BoxDecoration(
                                              color: const Color(0xFF9599B3),
                                              borderRadius:
                                                  BorderRadius.circular(45),
                                              boxShadow: const [
                                                BoxShadow(
                                                    blurRadius: 6,
                                                    color: Color(0xFF00000029),
                                                    spreadRadius: 3,
                                                    blurStyle: BlurStyle.inner)
                                              ],
                                            ),
                                            child: InkWell(
                                              onTap: () {
                                                MycomposantBtnSheet btn =
                                                    MycomposantBtnSheet(
                                                        ContenuDeLaPrestation:
                                                            'Gâteaux de mariage',
                                                        title: 'Pâtissier 1',
                                                        favoris: Image.asset(
                                                            'assets/icones/PRESTATAIRES/noun-heart-100.png'),
                                                        subtitle: 'Edith - j',
                                                        information:
                                                            'Informations',
                                                        tel: '065 88 63 35',
                                                        addresse:
                                                            'Libreville, GABON',
                                                        prestation:
                                                            'Prestation');
                                                btn.btnSheet(context);
                                              },
                                              child: Container(
                                                padding: const EdgeInsets.only(
                                                    bottom: 33,
                                                    left: 17,
                                                    right: 17),
                                                child: Image.asset(
                                                    "assets/icones/PRESTATAIRES/noun-chef-45377.png"),
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Pâtissier $PatissierNumber",
                                            style: const TextStyle(
                                              color: Color(0xFF7C6BD7),
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(
                                            height: myHeigth / 35,
                                          )
                                        ],
                                      );
                                    },
                                  ),
                                );
                              })),
                      SizedBox(
                        height: myHeigth / 35,
                      ),
                      AppWidgetsBoutons.boutonRetour(
                          colorbouton: Design.ColorMarie,
                          height: myHeigth / 16,
                          width: myWidth / 1.6,
                          onPressed: () {
                            Navigator.pushNamed(context, '/AccueilMariee');
                          }),
                    ],
                  )),
            ],
          )
        ],
      ),
      drawer: DrawerPrestataires(check: drawPos),
    );
  }
}
