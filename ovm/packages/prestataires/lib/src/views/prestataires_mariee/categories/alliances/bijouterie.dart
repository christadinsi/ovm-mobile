import 'dart:io';

import 'package:flutter/material.dart';
import 'package:prestataires/components/btnnavigator_sheet.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';

class Bijouteries extends StatefulWidget {
  const Bijouteries({super.key});

  @override
  State<Bijouteries> createState() => _ListeDesSallesState();
}

class _ListeDesSallesState extends State<Bijouteries> {
   final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    Widget salle = Container(
      width: _width * .30,
      height: _height * .19,
      decoration: BoxDecoration(
        color: const Color(0xFF9599B3),
        borderRadius: BorderRadius.circular(40),
        boxShadow: const [
          BoxShadow(
              blurRadius: 6,
              color: Color(0xFF00000029),
              spreadRadius: 3,
              blurStyle: BlurStyle.inner)
        ],
      ),
      child: InkWell(
        onTap: () {
          MycomposantBtnSheet btn = MycomposantBtnSheet(
              ContenuDeLaPrestation:
                  'Alliances de  mariage,bijoux.. ',
              title: 'Bijoutier 1',
              // favoris: Image.asset('name'),
              // favoris: Image.asset('assets/icones/PRESTATAIRES/noun-heart-100.png'),
              subtitle: 'Bijouterie d\'Ani',
              information: 'Informations',
              tel: '062 22 22 34',
              addresse: 'quatier class',
              prestation: 'Prestation');
          btn.btnSheet(context);
        },
        child: Container(
          padding: const EdgeInsets.all(20),
          width: 64,
          height: 74,
          child:
              Image.asset("assets/icones/PRESTATAIRES/onboarding_24_mariage.png"),
        ),
      ),
    );

    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text(
          "Prestataires",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
        actions: [
          IconButton(onPressed: () {
             _key.currentState?.openDrawer();
          }, icon: const Icon(Icons.dehaze_sharp,color: Color(0xFF7C6BD7),))
        ],
        leading: const BackButton(
          color: Color(0xFF7C6BD7),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Column(
            children: [
              Flexible(
                  flex: 40,
                  child: Stack(children: [
                    Container(
                      width: _width * 1.81,
                      height: _height * .42,
                      // color: Colors.amber,
                      child: Image.asset(
                        "assets/images/lamariee/marieProfile115.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      bottom: -2,
                      // left: _width*.09,
                      // right:_width*.09,
                      child: Container(
                        width: _width,
                        height: _height * .12,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          gradient: LinearGradient(
                              stops: [0.15, 0.50],
                              begin: FractionalOffset.topCenter,
                              end: FractionalOffset.bottomCenter,
                              colors: [
                                const Color(0xFFFFFFFF).withOpacity(.1),
                                const Color(0xFFFFFFFF),
                              ]),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Bijoutiers",
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Color(0xFFD47FA6),
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            // const SizedBox(
                            //   height: 10,
                            // ),
                            Padding(
                              padding: const EdgeInsets.only(left: 2, right: 1),
                              child: Container(
                                  // margin: EdgeInsets.only(left: _width*.03,right: _width*.02),
                                  child: Text(
                                "Trouvez votre artisant parfait ",
                                style: TextStyle(
                                    fontSize: 20,
                                    color:
                                        const Color(0xFF000000).withOpacity(.6),
                                    fontWeight: FontWeight.bold),
                              )),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ])),
              Flexible(
                  flex: 50,
                  child: Container(
                    width: _width * 1.81,
                    height: _height * .5,
                    // color: Colors.blue,
                    child: ListView(
                     padding: EdgeInsets.only(top: 0),
                      children: [
                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 1",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 2",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 3",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),
                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 4",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 5",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 6",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 7",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 8",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 9",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 10",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 11",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 12",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 13",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 14",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 15",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                        const SizedBox(
                          height: 10,
                        ),

                        // premier bloc des salles
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 16",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 17",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                salle,
                                const Text(
                                  "Bijoutier 18",
                                  style: TextStyle(
                                      color: Color(0xFF7C6BD7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ],
                        ),
                        // fin de la premiere ligne de bloc
                      ],
                    ),
                  )),
              SizedBox(
                height: 10,
              ),
              Flexible(
                  flex: 10,
                  child: Container(
                    width: _width * 1.81,
                    height: _height * .42,
                    child: AppWidgetsBoutons.boutonRetour(
                        colorbouton: Design.ColorMarie,
                        height: _height / 16,
                        width: _width / 1.6,
                        onPressed: () {
                          Navigator.pushNamed(context, '/AccueilMariee');
                        }),
                  )),
            ],
          ),
        ],
      ),
      drawer: DrawerPrestataires(check: drawPos),

    );
  }
}
