import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/traiteurs/service_traiteur.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/traiteurs/service_traiteur_chef.dart';
import 'package:page_transition/page_transition.dart';

class AcceuilServiceTraiteur extends StatelessWidget {
  const AcceuilServiceTraiteur({super.key});

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: _height / 2,
                child: Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 50),
                      height: _height / 2,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/Chafing_Dishes.jpg"),

                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom: 25),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: [0.15, 0.50],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          //tileMode: TileMode.mirror,
                          colors: <Color>[
                            Colors.white,
                            Colors.white70.withOpacity(0.0)
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              // Navigator.pushNamed(context, '/ReceptionMariee');
                              // print("BONJOUR");
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              color: Design.ColorMarie,
                              size: 24,
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          const Text(
                            "Prestataires",
                            style: TextStyle(
                                fontSize: 20, color: Design.ColorMarie),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.dehaze_sharp,
                              color: Design.ColorMarie,
                              size: 24,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 330.0, left: 50),
                      child: Text(
                        "Service traiteur",
                        style:
                            TextStyle(color: Design.colorMariee, fontSize: 35),
                      ),
                    )
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Text("Regalez vos invites",
                        style: TextStyle(fontSize: 18,
                          color: Colors.grey
                        ),

                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text("avec des plats inoubliable",  style: TextStyle(fontSize: 18,
                        color: Colors.grey))
                  ],
                ),
              ),
              //Chefs & Traiteurs
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //chef
                    Column(
                      children: [
                        const Text(
                          "Chefs",
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.ColorMarie),
                        ),
                        InkWell(
                          child: Container(
                            width: _width / 4,
                            height: _height / 7,

                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            // padding:
                            //     const EdgeInsets.symmetric(vertical: 25.0),
                            decoration: BoxDecoration(
                                //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2, color: Design.ColorMarie)),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: const AssetImage(
                                      "assets/icones/PRESTATAIRES/noun-chef-45377.png"),
                                  width: _width / 6.5,
                                  height: _width / 6.5,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              PageTransition(
                                child: const ServiceTraiteurChef(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),
                      ],
                    ),

                    //Traiteur
                    Column(
                      children: [
                        const Text(
                          "Traiteurs",
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              color: Design.ColorMarie),
                        ),
                        InkWell(
                          child: Container(
                            width: _width / 4,
                            height: _height / 7,
                            margin: const EdgeInsets.symmetric(vertical: 5.0),
                            decoration: BoxDecoration(
                                //color: Colors.black12,
                                borderRadius: BorderRadius.circular(35),
                                border: Border.all(
                                    width: 2, color: Design.ColorMarie)),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image(
                                  image: const AssetImage(
                                      "assets/icones/BUFFET/profile_95_buffet.png"),
                                  width: _width / 6.5,
                                  height: _width / 6.5,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            // Navigator.pushNamed(context, '/ServiceTraiteur');
                              Navigator.push(
                              context,
                              PageTransition(
                                child: const ServiceTraiteur(),
                                type: PageTransitionType.rightToLeft,
                                //childCurrent: widget,
                                duration: const Duration(milliseconds: 500),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: AppWidgetsBoutons.boutonRetour(
                  colorbouton: Design.ColorMarie,
                  height: _height / 16,
                  width: _width / 1.6,
                  onPressed: () {
                    Navigator.pushNamed(context, '/AccueilMariee');
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
