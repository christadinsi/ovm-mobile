import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/divers/gadgets/gadgets.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/divers/nettoyage/nettoyage.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';
import 'package:page_transition/page_transition.dart';

class DiversMariee extends StatefulWidget {
  const DiversMariee({super.key});

  @override
  State<DiversMariee> createState() => _DiversMarieeState();
}

class _DiversMarieeState extends State<DiversMariee> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 1;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,
        actions: <Widget>[
          //IconButton
          IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            //color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          //IconButton
        ],

        title: const Text(
          'Prestataires',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            AppWidgetsTitles.bar(),
            AppWidgetsTitles.titre(
                texte: "Divers", colorText: (Design.ColorMarie), size: 25),
            Image(
              image: const AssetImage(
                  "assets/icones/PRESTATAIRES/noun-accessories-4178633.png"),
              width: _width / 2.5,
            ),
            const Text(
              "Choisissez le partenaire idéal !",
              style: TextStyle(fontSize: 18, color: Colors.black54),
            ),
            Container(
              height: _height / 3,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //Gadgets & Nettoyage
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //Gadgets
                      Column(
                        children: [
                          const Text(
                            "Gadgets",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.ColorMarie),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.ColorMarie)),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-key-ring-2545152.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                                 Navigator.push(
                                 context,
                                PageTransition(
                                 child: const Gadget(),
                                 type: PageTransitionType.rightToLeft,
                                  duration: const Duration(milliseconds: 1000)
                                   )
                                  );
                            },
                          ),
                        ],
                      ),

                      //Nettoyage
                      Column(
                        children: [
                          const Text(
                            "Nettoyage",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.ColorMarie),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.ColorMarie)),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-cleaning-service-1978963.png"),
                                    width: _width / 6,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                            
                             Navigator.push(
                                 context,
                                PageTransition(
                                 child: const Nettoyage(),
                                 type: PageTransitionType.rightToLeft,
                                  duration: const Duration(milliseconds: 1000)
                                   )
                                  );

                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            AppWidgetsBoutons.boutonRetour(
                colorbouton: Design.ColorMarie,
                height: _height / 16,
                width: _width / 1.6,
                onPressed: () {
                  Navigator.pushNamed(context, '/AccueilMariee');
                }),
          ],
        ),
      ),
      drawer: DrawerPrestataires(check: drawPos),
    );
  }
}
