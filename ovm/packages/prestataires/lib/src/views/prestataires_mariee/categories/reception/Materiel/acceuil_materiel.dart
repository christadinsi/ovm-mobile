import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/location_materiels/location_materiel.dart/acceuil_location.dart';
import 'package:page_transition/page_transition.dart';

class AccueilMateriel extends StatelessWidget {
  const AccueilMateriel({super.key});

  @override
  Widget build(BuildContext context) {
    // var size = MediaQuery.of(context).size;
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Color(0xFF7C6BD7),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: AppWidgetsTitles.titre(
            texte: "Prestataires",
            colorText: (Design.colorModuleGrey),
            size: 20),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
                    double fontSize = width * 0.09999;
                    // double fontSize2 = width * 0.08;
                    double fontSize3 = width * 0.05;
         return  Container(
          width: width,
          padding: const EdgeInsets.symmetric(horizontal: 22),
          child: Column(
            // mainAxisSize: MainAxisSize.max,
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
        
            children: [
              const SizedBox(
                height: 25,
              ),
              AppWidgetsTitles.bar(),
              Container(
                width: double.maxFinite,
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(vertical: 18),
                child: AppWidgetsTitles.titre(
                    texte: "Matériel", colorText: Design.ColorMarie, size: fontSize),
              ),
              Container(
                padding: const EdgeInsets.only(top: 0, bottom: 20.0),
                child: Column(
                  children: [
                    Image(
                      image:
                          const AssetImage("assets/icones/PRESTATAIRES/chair-guest-wedding-party.png"),
                      width: width / 3,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 8,
              ),
               const Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Tentes, chaises, couverts, ',
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 20,
                            fontFamily: 'Poppins'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                   Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'porte-clés, tréteaux et chaffing dish...',
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 17,
                            fontFamily: 'Poppins'),
                        maxLines: 1,
                        softWrap: true ,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ],
              ),
               SizedBox(
                height: height/8,
              ),
              Container(
                width: double.maxFinite,
                child: Column(
                  children: [
                    const Text(
                      "Locations",
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Design.ColorMarie),
                    ),
                    InkWell(
                      child: Container(
                        width: width / 4,
                        height: height / 6,
        
                        margin: const EdgeInsets.symmetric(vertical: 5.0),
                        // padding: const EdgeInsets.symmetric(vertical: 25.0),
                        decoration: BoxDecoration(
                            //color: Colors.black12,
                            borderRadius: BorderRadius.circular(35),
                            border:
                                Border.all(width: 2, color: Design.ColorMarie)),
                        // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(
                              image: const AssetImage(
                                  "assets/icones/PRESTATAIRES/noun-sale-2632482.png"),
                              width: width / 6,
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                      
                         Navigator.push(
                                 context,
                                PageTransition(
                                 child: const AcceuilLoaction(),
                                 type: PageTransitionType.rightToLeft,
                                  duration: const Duration(milliseconds: 1000)
                                   )
                                  );
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              AppWidgetsBoutons.boutonRetour(
                  colorbouton: Design.ColorMarie,
                  height: height / 16,
                  width: width / 1.6,
                  onPressed: () {
                    Navigator.pushNamed(context, '/AccueilMariee');
                  }),
            ],
          ),
        );
        },
        
      ),
    );
  }
}
