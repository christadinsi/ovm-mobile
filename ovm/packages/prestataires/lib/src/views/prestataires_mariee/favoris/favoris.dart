import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/alliances_fav/bijouteries/bijouteries_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/beaute_fav/beaute_fav.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/ceremonie_fav/mc/mc_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/decoration_fav/decoration_fav.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/divers_fav/divers_fav.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/musique_fav/dj/djs_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/papeterie_fav/imprimeries/imprimeries_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/photo_fav/photo_fav.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/reception_fav/reception_fav.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/transport_fav/agences/agences_select.dart';


class FavorisMariee extends StatelessWidget {
  const FavorisMariee({super.key});

  @override
  Widget build(BuildContext context) {

    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return  Scaffold(

        appBar: AppBar(
          //automaticallyImplyLeading: false,
          leading: const BackButton(),
          iconTheme: const IconThemeData(color: Design.colorMariee),
          centerTitle: true,
          toolbarHeight: 30,

          title: const Text(
            'Favoris',
            style: TextStyle(fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Design.colorMariee
            ),
          ),

        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          //height: _height,
          //width: _width,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

            children: [
              Row(
                children: [
                  SizedBox(
                    width: _width/2.7,
                    child:  AppWidgetsTitles.bar(),
                  ),

                  Image(
                    image: const AssetImage("assets/icones/PRESTATAIRES/Plan de travail 1.png"),
                    width: _width/7,
                  ),


                ],
              ),
              AppWidgetsTitles.titre(texte: "Prestataires sélectionés", colorText: (Design.colorModuleGrey), size: 25),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                height: _height/1.4,
                child: SingleChildScrollView(
                  child: Column(
                    children: [

                      //Reception & Beauté
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          //Reception
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Réception",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-party-3203411.png"),
                                    width: _width/4,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const ReceptionMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );

                            },
                          ),

                          //Beauté
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Beauté",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-mirror-4675515.png"),
                                    width: _width/5,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const BeauteMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],),

                      //Deco & musik
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          //Décoration
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Décoration",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-wedding-arch-3481819.png"),
                                    width: _width/4,
                                    height: _width/4,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const DecorationMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),

                          //Musique
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Musique",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-music-111.png"),
                                    width: _width/5,
                                    height: _width/5,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const DjsSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );

                            },
                          ),
                        ],),

                      //Photo & Cérémonie
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          //Photo
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Photo",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-pictures-1412356.png"),
                                    width: _width/4,
                                    height: _width/4,
                                  ),


                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const PhotoMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),

                          //Cérémonie
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Cérémonie",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-microphone-116.png"),
                                    width: _width/5,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const McsSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );

                            },
                          ),
                        ],),

                      //Papeterie & Alliances
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          //Papeterie
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Papeterie",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/onboarding_34_papeterie.png"),
                                    width: _width/4,
                                    height: _width/4,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const ImprimeriesSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),

                          //Alliances
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text("Alliances",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-ring-1514513.png"),
                                    width: _width/4,
                                    height: _width/4,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const BijouteriesSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );

                              /*Navigator.push(
                                context,
                                PageTransition(
                                  child: const AlliancesMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );*/
                            },
                          ),
                        ],),

                      //Transport & Divers
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [

                          //Transport
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Transport",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-honeymoon-6395.png"),
                                    width: _width/4,
                                    height: _width/4,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {
                             /* Navigator.push(
                                context,
                                PageTransition(
                                  child: const TransportMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );*/

                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const AgencesSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );

                            },
                          ),

                          //Divers
                          InkWell(
                            child:
                            Container(
                              width: _width/3,
                              height: _height/4.5,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              padding: const EdgeInsets.symmetric(vertical: 15.0),
                              decoration: BoxDecoration(
                                //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2,
                                      color: Design.colorMariee
                                  )
                              ),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child:   Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  const Text("Divers",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.bold,
                                        color: Design.colorMariee
                                    ),),
                                  Image(
                                    image: const AssetImage("assets/icones/PRESTATAIRES/noun-accessories-4178633.png"),
                                    width: _width/4,
                                    height: _width/4,
                                  ),

                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const DiversMarieeFav(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],),

                    ],
                  ),
                ),
              ),
              AppWidgetsBoutons.boutonRetour( colorbouton: Design.colorMariee, height: _height/16, width:  _width/1.6, onPressed: () {Navigator.pushNamed(context, '/AccueilMariee');}),

            ],

          ),
        )

    );
  }
}
