import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/acceuil_materiel.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';

class DecorateurFavori extends StatefulWidget {
  const DecorateurFavori({super.key});

  @override
  State<DecorateurFavori> createState() => _DecorateurFavoriState();
}

class _DecorateurFavoriState extends State<DecorateurFavori> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 2;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,
        actions: <Widget>[
          //IconButton
          IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            //color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          //IconButton
        ],

        title: const Text(
          'Favoris',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            AppWidgetsTitles.bar(),
            AppWidgetsTitles.titre(
                texte: "Décorateurs selectionnés",
                colorText: (Design.colorMariee),
                size: 24),
            Image(
              image: const AssetImage(
                  "assets/icones/PRESTATAIRES/noun-female-avatar-18685.png"),
              width: _width / 2.5,
            ),
            const Text(
              "Une décoration selon vos desirs !",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, color: Colors.black54),
            ),
            Container(
              height: _height / 3,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //Salles
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //Deco 01
                      Column(
                        children: [
                          Image(
                            image: const AssetImage(
                                "assets/icones/PRESTATAIRES/Plandetravail2.png"),
                            width: _width / 5,
                            height: _width / 5,
                          ),
                          const Text(
                            "Déco 01",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/onboarding_20_prestataire.png"),
                                    width: _width / 5,
                                    height: _width / 5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),

                      //Deco 02
                      Column(
                        children: [
                          SizedBox(
                            width: _width / 5,
                            height: _width / 5,
                          ),
                          const Text(
                            "Déco 02",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/onboarding_20_prestataire.png"),
                                    width: _width / 5,
                                    height: _width / 5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),

                      // deco 03
                      Column(
                        children: [
                          SizedBox(
                            width: _width / 5,
                            height: _width / 5,
                          ),
                          const Text(
                            "Déco 03",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,
                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/onboarding_20_prestataire.png"),
                                    width: _width / 5,
                                    height: _width / 5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {},
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            AppWidgetsBoutons.boutonRetour(
                colorbouton: Design.colorMariee,
                height: _height / 16,
                width: _width / 1.6,
                onPressed: () {
                  Navigator.pushNamed(context, '/AccueilMariee');
                }),
          ],
        ),
      ),
      drawer: DrawerPrestataires(check: drawPos),
    );
  }
}
