import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/decoration_fav/decorateur_fav/decorateur_favori.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/decoration_fav/fleuriste_fav/fleuriste_fav.dart';


class DecorationMarieeFav extends StatefulWidget {
  const DecorationMarieeFav({super.key});

  @override
  State<DecorationMarieeFav> createState() => _DecorationMarieeFavState();
}

class _DecorationMarieeFavState extends State<DecorationMarieeFav> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 2;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,
        actions: <Widget>[
          //IconButton
          IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            //color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          //IconButton
        ],

        title: const Text(
          'Favoris',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            AppWidgetsTitles.bar(),
            AppWidgetsTitles.titre(
                texte: "Prestataire Décoration", colorText: (Design.colorMariee), size: 25),
            Image(
              image: AssetImage(
                  "assets/icones/PRESTATAIRES/noun-wedding-arch-3481819.png"),
              width: _width / 2.5,
            ),
            Text(
              "Choisissez le partenaire idéal !",
              style: TextStyle(fontSize: 18, color: Colors.black54),
            ),
            Container(
              height: _height / 3,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //Décorateurs & Fleuristes
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //Décorateurs
                      Column(
                        children: [
                          const Text(
                            "Décorateurs",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-female-avatar-18685.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              // Navigator.pushNamed(context, '/DecorateurFavori');
                                Navigator.push(
                                context,
                                PageTransition(
                                  child: const DecorateurFavori(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],
                      ),

                      //Fleuristes
                      Column(
                        children: [
                          const Text(
                            "Fleuristes",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              // padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-flower-bouquet-5022999.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                                 Navigator.push(
                                context,
                                PageTransition(
                                  child: const FleuristeFavori(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            AppWidgetsBoutons.boutonRetour(
                colorbouton: Design.colorMariee,
                height: _height / 16,
                width: _width / 1.6,
                onPressed: () {
                  Navigator.pushNamed(context, '/AccueilMariee');
                }),
          ],
        ),
      ),
      drawer: DrawerPrestataires(check: drawPos),
    );
  }
}
