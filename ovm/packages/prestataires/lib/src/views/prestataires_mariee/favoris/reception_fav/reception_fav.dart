import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/components/widgets-title.dart';
import 'package:prestataires/design/design.dart';
import 'package:page_transition/page_transition.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/Materiel/acceuil_materiel.dart';
import 'package:prestataires/src/views/prestataires_mariee/categories/reception/salles/liste_salle.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/reception_fav/locations/locations_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/reception_fav/patissiers/patissiers_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/reception_fav/salles/salles_select.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/reception_fav/traiteurs/traiteurs_select.dart';


class ReceptionMarieeFav extends StatefulWidget {
  const ReceptionMarieeFav({super.key});

  @override
  State<ReceptionMarieeFav> createState() => _ReceptionMarieeFavState();
}

class _ReceptionMarieeFavState extends State<ReceptionMarieeFav> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 2;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _key,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme: const IconThemeData(color: Design.colorModuleGrey),
        centerTitle: true,
        toolbarHeight: 30,
        actions: <Widget>[
          //IconButton
          IconButton(
            icon: const Icon(Icons.dehaze_sharp),
            //color: Colors.grey,
            tooltip: 'Setting Icon',
            onPressed: () {
              _key.currentState?.openDrawer();
            },
          ),
          //IconButton
        ],

        title: const Text(
          'Prestataires',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Design.colorModuleGrey),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            AppWidgetsTitles.bar(),
            AppWidgetsTitles.titre(
                texte: "Réception", colorText: (Design.colorMariee), size: 25),

            Image(
              image: const AssetImage(
                  "assets/icones/PRESTATAIRES/noun-party-3203411.png"),
              width: _width / 2.5,
            ),
            const Text(
              "Choisissez le partenaire idéal !",
              style: TextStyle(fontSize: 18, color: Colors.black54),
            ),
            Container(
              height: _height / 2.5,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //Salles & traiteurs
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //Salles
                      Column(
                        children: [
                          const Text(
                            "Salles",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-hall-4870131.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const SallesSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );

                            },
                          ),
                        ],
                      ),

                      //Traiteurs
                      Column(
                        children: [
                          const Text(
                            "Traiteurs",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),
                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-cook-14276.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const TraiteursSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),

                  //Patissier & Matériel
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      //Patissiers
                      Column(
                        children: [
                          const Text(
                            "Patissiers",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),

                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/noun-chef-45377.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const PatissiersSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],
                      ),

                      //Matériel
                      Column(
                        children: [
                          const Text(
                            "Matériel",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                color: Design.colorMariee),
                          ),
                          InkWell(
                            child: Container(
                              width: _width / 4,
                              height: _height / 7,

                              margin: const EdgeInsets.symmetric(vertical: 5.0),

                              decoration: BoxDecoration(
                                  //color: Colors.black12,
                                  borderRadius: BorderRadius.circular(35),
                                  border: Border.all(
                                      width: 2, color: Design.colorMariee)),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: const AssetImage(
                                        "assets/icones/PRESTATAIRES/chair-guest-wedding-party.png"),
                                    width: _width / 6.5,
                                    height: _width / 6.5,
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                PageTransition(
                                  child: const LocationsSelection(),
                                  type: PageTransitionType.rightToLeft,
                                  //childCurrent: widget,
                                  duration: const Duration(milliseconds: 500),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            AppWidgetsBoutons.boutonRetour(
                colorbouton: Design.colorMariee,
                height: _height / 16,
                width: _width / 1.6,
                onPressed: () {
                  Navigator.pushNamed(context, '/AccueilMariee');
                }),
          ],
        ),
      ),
      drawer: DrawerPrestataires(check: drawPos),
    );
  }
}
