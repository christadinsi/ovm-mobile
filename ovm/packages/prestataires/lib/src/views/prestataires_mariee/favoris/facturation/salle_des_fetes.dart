import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/design/design.dart';
import 'package:prestataires/src/views/prestataires_mariee/drawer_prestataire.dart';
import 'package:prestataires/src/views/prestataires_mariee/favoris/facturation/photo_facture.dart';
import 'package:page_transition/page_transition.dart';

class SalleDesFetes extends StatefulWidget {
  const SalleDesFetes({super.key});

  @override
  State<SalleDesFetes> createState() => _SalleDesFetesState();
}

class _SalleDesFetesState extends State<SalleDesFetes> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  int drawPos = 2;

  void openDrawerWithSelectValue(int selectedValue) {
    setState(() {
      drawPos = selectedValue;
    });
    _key.currentState?.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: const Text(
          "Favoris",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Colors.white,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      drawer: DrawerPrestataires(check: drawPos),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                color: Colors.white,
                height: myHeigth / 2,
                child: Stack(
                  children: [
                    Container(
                      height: myHeigth / 2,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "assets/images/lamariee/marieProfile47.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 100),
                      child: Align(
                        alignment: Alignment.center,
                        child: Image.asset(
                          "assets/icones/PRESTATAIRES/noun-heart-5271870.png",
                          width: myWidth / 3.6,
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 30),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          stops: const [0.10, 0.40],
                          begin: FractionalOffset.bottomCenter,
                          end: FractionalOffset.topCenter,
                          colors: <Color>[
                            Colors.white,
                            Colors.white70.withOpacity(0.0)
                          ],
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Text(
                              "Salle des fêtes 01",
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: myHeigth / 70,
                            ),
                            const Text(
                              "KARE F",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Design.colorMariee,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                height: myHeigth / 60,
              ),
              Container(
                  height: myHeigth / 2.2,
                  margin: const EdgeInsets.only(left: 5, right: 5),
                  color: Colors.white,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Center(
                        child: SizedBox(
                          width: myWidth / 1.3,
                          height: myHeigth / 16,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.grey)),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  PageTransition(
                                    child: const PhotoFacture(),
                                    type: PageTransitionType.rightToLeft,
                                    //childCurrent: widget,
                                    duration: const Duration(milliseconds: 500),
                                  ),
                                );
                              },
                              child: const Text(
                                "Photos",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              const AutoSizeText(
                                "Facture",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Design.ColorMarie,
                                    fontWeight: FontWeight.bold),
                                maxLines: 1,
                                minFontSize: 12,
                              ),
                              Builder(builder: (context) {
                                return InkWell(
                                  onTap: () {
                                    openDrawerWithSelectValue(4);
                                  },
                                  child: Card(
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                    elevation: 6,
                                    child: Container(
                                      width: myWidth / 4,
                                      height: myHeigth / 6,
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                              color: Design.ColorMarie,
                                              width: 3),
                                          bottom: BorderSide(
                                              color: Design.ColorMarie,
                                              width: 3),
                                          left: BorderSide(
                                              color: Design.ColorMarie,
                                              width: 3),
                                          right: BorderSide(
                                              color: Design.ColorMarie,
                                              width: 3),
                                        ),
                                        color: Design.colorSecondTexte,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(50),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(18),
                                        child: Center(
                                          child: Image.asset(
                                            "assets/icones/PRESTATAIRES/noun-bills-45964.png",
                                            width: myWidth / 1,
                                            height: myHeigth / 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                            ],
                          ),
                          Column(
                            children: [
                              const AutoSizeText(
                                "Avance",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold),
                                maxLines: 1,
                                minFontSize: 12,
                              ),
                              Builder(builder: (context) {
                                return InkWell(
                                  onTap: () {
                                    openDrawerWithSelectValue(5);
                                  },
                                  child: Card(
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                    elevation: 6,
                                    child: Container(
                                      width: myWidth / 4,
                                      height: myHeigth / 6,
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                              color: Colors.green, width: 3),
                                          bottom: BorderSide(
                                              color: Colors.green, width: 3),
                                          left: BorderSide(
                                              color: Colors.green, width: 3),
                                          right: BorderSide(
                                              color: Colors.green, width: 3),
                                        ),
                                        color: Design.colorSecondTexte,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(50),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(13),
                                        child: Center(
                                          child: Image.asset(
                                            "assets/icones/PRESTATAIRES/noun-money-1346.png",
                                            width: myWidth / 1,
                                            height: myHeigth / 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                            ],
                          ),
                          Column(
                            children: [
                              const AutoSizeText(
                                "Solde",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                                maxLines: 1,
                                minFontSize: 12,
                              ),
                              Builder(builder: (context) {
                                return InkWell(
                                  onTap: () {
                                    openDrawerWithSelectValue(6);
                                  },
                                  child: Card(
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50)),
                                    elevation: 6,
                                    child: Container(
                                      width: myWidth / 4,
                                      height: myHeigth / 6,
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                              color: Colors.grey, width: 3),
                                          bottom: BorderSide(
                                              color: Colors.grey, width: 3),
                                          left: BorderSide(
                                              color: Colors.grey, width: 3),
                                          right: BorderSide(
                                              color: Colors.grey, width: 3),
                                        ),
                                        color: Design.colorSecondTexte,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(50),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(18),
                                        child: Center(
                                          child: Image.asset(
                                            "assets/icones/PRESTATAIRES/price-tag.png",
                                            width: myWidth / 2,
                                            height: myHeigth / 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              })
                            ],
                          )
                        ],
                      ),
                      AppWidgetsBoutons.boutonRetour(
                          colorbouton: Design.colorMariee,
                          height: myHeigth / 16,
                          width: myWidth / 1.6,
                          onPressed: () {
                            Navigator.pushNamed(context, '/AccueilMariee');
                          }),
                    ],
                  )),
            ],
          )
        ],
      ),
    );
  }
}
