import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:prestataires/components/widgets-boutons.dart';
import 'package:prestataires/design/design.dart';

class PhotoFacture extends StatefulWidget {
  const PhotoFacture({super.key});

  @override
  State<PhotoFacture> createState() => _PhotoFactureState();
}

class _PhotoFactureState extends State<PhotoFacture> {
  @override
  Widget build(BuildContext context) {
    final double myWidth = MediaQuery.of(context).size.width;
    final double myHeigth = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Photos - facture",
          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
        ),
        leading: const BackButton(
          color: Colors.grey,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image(
            image: const AssetImage(
                "assets/icones/PRESTATAIRES/noun-invoice-5029214.png"),
            width: myWidth / 2.5,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Text(
                "Salle des fêtes",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: myHeigth / 40,
              ),
              const Text(
                "KARE F",
                style: TextStyle(
                    fontSize: 25,
                    color: Design.colorMariee,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  const AutoSizeText(
                    "Facture",
                    style: TextStyle(
                        fontSize: 18,
                        color: Design.ColorMarie,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {},
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              bottom: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              left: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                              right: BorderSide(
                                  color: Design.ColorMarie, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/PRESTATAIRES/noun-bills-45964.png",
                                width: myWidth / 1,
                                height: myHeigth / 1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              Column(
                children: [
                  const AutoSizeText(
                    "Avance",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.green,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {},
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.green, width: 3),
                              bottom: BorderSide(color: Colors.green, width: 3),
                              left: BorderSide(color: Colors.green, width: 3),
                              right: BorderSide(color: Colors.green, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(13),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/PRESTATAIRES/noun-money-1346.png",
                                width: myWidth / 1,
                                height: myHeigth / 1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
              Column(
                children: [
                  const AutoSizeText(
                    "Solde",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                    minFontSize: 12,
                  ),
                  Builder(builder: (context) {
                    return InkWell(
                      onTap: () {},
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        elevation: 6,
                        child: Container(
                          width: myWidth / 4,
                          height: myHeigth / 6,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey, width: 3),
                              bottom: BorderSide(color: Colors.grey, width: 3),
                              left: BorderSide(color: Colors.grey, width: 3),
                              right: BorderSide(color: Colors.grey, width: 3),
                            ),
                            color: Design.colorSecondTexte,
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(18),
                            child: Center(
                              child: Image.asset(
                                "assets/icones/PRESTATAIRES/price-tag.png",
                                width: myWidth / 2,
                                height: myHeigth / 1,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  })
                ],
              )
            ],
          ),
          AppWidgetsBoutons.boutonRetour(
              colorbouton: Design.colorMariee,
              height: myHeigth / 16,
              width: myWidth / 1.6,
              onPressed: () {
                Navigator.pushNamed(context, '/AccueilMariee');
              }),
        ],
      ),
    );
  }
}
